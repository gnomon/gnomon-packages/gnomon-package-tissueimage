=======
Credits
=======

Development Lead
----------------

.. {# pkglts, doc.authors

* Guillaume Cerutti <guillaume.cerutti@inria.fr>
* Tristan Cabel <tristan.cabel@inria.fr>
* Arthur Luciani <arthur.luciani@inria.fr>
* Karamoko Samassa <karamoko.samassa@inria.fr>

.. #}

Contributors
------------

.. {# pkglts, doc.contributors

* GACON Florian <florian.gacon@inria.fr>


.. #}
