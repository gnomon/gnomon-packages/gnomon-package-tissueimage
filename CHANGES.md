# ChangeLog

## version 1.0.0 - 2024-03-27
* gnomon 1.0.0
* Move from dtkImage to vtkImageData
* progress bar + message
* wall property plugins for cell images
* improve voxelsize display in metadata

## version 0.5.0 - 2023-07-07
* libgnomon 0.81.0
* nuclei quantification
* fix for point cloud visualization

## version 0.4.0 - 2023-04-12
* libgnomon 0.80.0
* shared visu
* connect parameters and refresh
* manual cell tracking plugin

## version 0.3.2 - 2023-02-03
* libgnomon 0.72.0
* cell picking
* plugin metadata
* memory optimization

## version 0.3.1 2022/09/15
* libgnomon 0.71.0
* single channel image visu with histogram
* channel names edition plugin
* fixes on registration and quantification
* descriptive images for reader plugins

## version 0.3.1 2022/07/19
* libgnomon 0.70.1
* bugfixes
* better visu plugins
* update dependencies

## version 0.2.0 2022/03/15
* python 3.9
* libgnomon 0.60
