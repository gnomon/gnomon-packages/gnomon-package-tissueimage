# Gnomon plugin package : `Tissue Image`

This package contains Python plugins for the [Gnomon computational platform](https://gnomon.gitlabpages.inria.fr/gnomon/) 
to represent and manipulate 3D images (Multi-channel intensity images and Segmented cell images) and point clouds.

This package contains a large variety of plugins to visualize and manipulates these forms including: 
image filtering, segmentation, cell image quantification, image registration and cell tracking.

The plugins are largely reliant on [timagetk](https://mosaic.gitlabpages.inria.fr/timagetk/) 
and [ctrl](https://anaconda.org/MOSAIC/ctrl) for the cell tracking.

## Installation

This package is published on the [gnomon anaconda channel](https://anaconda.org/gnomon/gnomon_package_tissueimage).
To install it a **conda client** is needed such as [miniconda](https://docs.anaconda.com/miniconda/).

To install use the following command in your **gnomon** environment:

```shell
gnomon-utils package install gnomon_package_tissueimage
```

Alternatively use:
```shell
conda install -c conda-forge -c gnomon -c mosaic -c morpheme -c dtk-forge6 gnomon_package_tissueimage
```