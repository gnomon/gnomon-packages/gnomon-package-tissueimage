#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

short_descr = "Gnomon python plugins to represent and manipulate 3D images"
readme = open('README.md').read()

# find packages
pkgs = find_packages('src')

setup_kwds = dict(
    name='gnomon_package_tissueimage',
    version="1.0.0",
    description=short_descr,
    long_description=readme,
    author="gnomon-dev",
    author_email="amdt-gnomon-dev@inria.fr",
    url='',
    license='LGPL-3.0-or-later',
    zip_safe=False,

    packages=pkgs,

    package_dir={'': 'src'},
    package_data={
        "": [
            "*.png",
            "*/*.png",
            "*/*/*.png"
        ]
    },

    entry_points={
        'imageVtkVisualization': [
            'gnomonImageVtkVisualizationSurface = gnomon_package_tissueimage.visualization.imageVtkVisualization.gnomonImageVtkVisualizationSurface',
            'imageChannelHistogram = gnomon_package_tissueimage.visualization.imageVtkVisualization.imageChannelHistogram',
            'imageSuperimposedTimeVolumes = gnomon_package_tissueimage.visualization.imageVtkVisualization.imageSuperimposedTimeVolumes'
        ],
        'pointCloudVtkVisualization': [
            'pointCloudVtkVisualization = gnomon_package_tissueimage.visualization.pointCloudVtkVisualization.pointCloudVtkVisualization'
        ],
        'cellImageVtkVisualization': [
            'cellImageVtkVisualizationMarchingCubes = gnomon_package_tissueimage.visualization.cellImageVtkVisualization.cellImageVtkVisualizationMarchingCubes',
            'cellWallsMarchingCubes = gnomon_package_tissueimage.visualization.cellImageVtkVisualization.cellWallsMarchingCubes'
        ],
        'cellImageData': [
            'gnomonCellImageDataTissueImage = gnomon_package_tissueimage.form.cellImageData.gnomonCellImageDataTissueImage'
        ],
        'pointCloudData': [
            'gnomonPointCloudDataPandas = gnomon_package_tissueimage.form.pointCloudData.gnomonPointCloudDataPandas'
        ],
        #'cellGraphData': [
        #    'gnomonCellGraphDataPropertyGraph = gnomon_package_tissueimage.form.cellGraphData.gnomonCellGraphDataPropertyGraph'
        #],
        'imageData': [
            'gnomonImageDataMultiChannelImage = gnomon_package_tissueimage.form.imageData.gnomonImageDataMultiChannelImage'
        ],
        'imageReader': [
            'imageReaderTimagetk = gnomon_package_tissueimage.io.imageReader.imageReaderTimagetk'
        ],
        'cellImageReader': [
            'cellImageReaderTimagetk = gnomon_package_tissueimage.io.cellImageReader.cellImageReaderTimagetk'
        ],
        'pointCloudWriter': [
            'pointCloudWriterDataFrame = gnomon_package_tissueimage.io.pointCloudWriter.pointCloudWriterDataFrame'
        ],
        'imageWriter': [
            'gnomonImageWriter = gnomon_package_tissueimage.io.imageWriter.gnomonImageWriter'
        ],
        'cellImageWriter': [
            'cellImageWriterTissueImage = gnomon_package_tissueimage.io.cellImageWriter.cellImageWriterTissueImage'
        ],
        'pointCloudReader': [
            'gnomonPointCloudReaderDataFrame = gnomon_package_tissueimage.io.pointCloudReader.gnomonPointCloudReaderDataFrame'
        ],
        'imageFusion': [
            'principalDirectionsAutoFusion = gnomon_package_tissueimage.algorithm.imageFusion.principalDirectionsAutoFusion',
            'gnomonImageFusion = gnomon_package_tissueimage.algorithm.imageFusion.gnomonImageFusion'
        ],
        'pointCloudQuantification': [
            'nucleiSignalQuantificationTimagetk = gnomon_package_tissueimage.algorithm.pointCloudQuantification.nucleiSignalQuantificationTimagetk'
        ],
        'imageRegistration': [
            'registrationTimagetk = gnomon_package_tissueimage.algorithm.imageRegistration.registrationTimagetk',
            'backwardRigidRegistration = gnomon_package_tissueimage.algorithm.imageRegistration.backwardRigidRegistration'
        ],
        'pointCloudFromImage': [
            'nucleiDetectionTimagetk = gnomon_package_tissueimage.algorithm.pointCloudFromImage.nucleiDetectionTimagetk',
            'nucleiDetectionFromSegmentation = gnomon_package_tissueimage.algorithm.pointCloudFromImage.nucleiDetectionFromSegmentation',
            'seedDetectionTimagetk = gnomon_package_tissueimage.algorithm.pointCloudFromImage.seedDetectionTmagetk',
            'cellCentersTimagetk = gnomon_package_tissueimage.algorithm.pointCloudFromImage.cellCentersTimagetk'
        ],
        'cellImageQuantification': [
            'cellFeaturesTissueImage = gnomon_package_tissueimage.algorithm.cellImageQuantification.cellFeaturesTissueImage',
            'surfaceCellCurvature = gnomon_package_tissueimage.algorithm.cellImageQuantification.surfaceCellCurvature',
            'cellVolumetricGrowth = gnomon_package_tissueimage.algorithm.cellImageQuantification.cellVolumetricGrowth',
            'signalQuantificationImageSignal = gnomon_package_tissueimage.algorithm.cellImageQuantification.signalQuantificationImageSignal',
            'wallFeaturesTissueImage = gnomon_package_tissueimage.algorithm.cellImageQuantification.wallFeaturesTissueImage',
            'wallSignalQuantificationImage = gnomon_package_tissueimage.algorithm.cellImageQuantification.wallSignalQuantificationImage',
            'wallToCellAveraging = gnomon_package_tissueimage.algorithm.cellImageQuantification.wallToCellAveraging',
        ],
        'cellImageTracking': [
            'nudgeLineageCtrl = gnomon_package_tissueimage.algorithm.cellImageTracking.nudgeLineageCtrl',
            'manualCellTracking = gnomon_package_tissueimage.algorithm.cellImageTracking.manualCellTracking',
        ],
        'cellGraphFromImage': [
            'gnomonCellGraphFromImageTissueAnalysis = gnomon_package_tissueimage.algorithm.cellGraphFromImage.gnomonCellGraphFromImageTissueAnalysis'
        ],
        'cellImageFromImage': [
            'seededWatershedSegmentationTimagetk = gnomon_package_tissueimage.algorithm.cellImageFromImage.seededWatershedSegmentationTimagetk',
            'autoSeededWatershedSegmentation = gnomon_package_tissueimage.algorithm.cellImageFromImage.autoSeededWatershedSegmentation',
            'seedImageDetectionTimagetk = gnomon_package_tissueimage.algorithm.cellImageFromImage.seedImageDetectionTimagetk'
        ],
        'imageFilter': [
            'linearFilterTimagetk = gnomon_package_tissueimage.algorithm.imageFilter.linearFilterTimagetk',
            'gaussianSmoothingScipy = gnomon_package_tissueimage.algorithm.imageFilter.gaussianSmoothingScipy',
            'morphoContrastTimagetk = gnomon_package_tissueimage.algorithm.imageFilter.morphoContrastTimagetk',
            'globalContrastStretch = gnomon_package_tissueimage.algorithm.imageFilter.globalContrastStretch',
            'boundaryEdgeEnhancement = gnomon_package_tissueimage.algorithm.imageFilter.boundaryEdgeEnhancement',
            'imageAxisRotation = gnomon_package_tissueimage.algorithm.imageFilter.imageAxisRotation',
            'equalizeAdaptHist = gnomon_package_tissueimage.algorithm.imageFilter.equalizeAdaptHist',
            'channelNamesEdit = gnomon_package_tissueimage.algorithm.imageFilter.channelNamesEdit',
            'imageCrop = gnomon_package_tissueimage.algorithm.imageFilter.imageCrop',
        ],
        'cellImageFilter': [
            'morphoCellFilter = gnomon_package_tissueimage.algorithm.cellImageFilter.morphoCellFilter',
            'cellBinaryPropertyOperation = gnomon_package_tissueimage.algorithm.cellImageFilter.cellBinaryPropertyOperation',
            'cellPropertyMajorityFilter = gnomon_package_tissueimage.algorithm.cellImageFilter.cellPropertyMajorityFilter',
            'cellPropertyNeighborPropagation = gnomon_package_tissueimage.algorithm.cellImageFilter.cellPropertyNeighborPropagation',
            'cellImageCrop = gnomon_package_tissueimage.algorithm.cellImageFilter.cellImageCrop',
            'cellProximityCrop = gnomon_package_tissueimage.algorithm.cellImageFilter.cellProximityCrop',
        ],
        'imageConstructor': [
            'nucleiImageConstructor = gnomon_package_tissueimage.constructor.imageConstructor.nucleiImageConstructor'
        ]
    }
    ,
    keywords='',

    test_suite='nose.collector',
)

setup(**setup_kwds)
