import gnomon.core
from gnomon_package_tissueimage.utils import vtk_img_to_sp_img
from gnomon.utils import algorithmPlugin
from gnomon.utils import load_plugin_group
from gnomon.core import gnomonAbstractCellGraphFromImage
from gnomon.core import gnomonCellGraph

from timagetk.tasks.tissue_graph_from_image import tissue_graph_from_image

load_plugin_group("cellGraphData")

@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Cell Graph from Intensity Image")
class gnomonCellGraphFromImageTissueAnalysis(gnomonAbstractCellGraphFromImage):

    def __init__(self):
        super(gnomonCellGraphFromImageTissueAnalysis, self).__init__()
        self.image = None
        self.use_margins = True
        self.min_contact_area = None
        self.background_label = 1
        self.properties = ['barycenter','volume','L1']

        self.graph = gnomonCellGraph()

    def __del__(self):
        del self.image
        del self.graph

    def setImage(self, image):
        self.image = image

    def setUseMargins(self,use_margins):
        self.use_margins = use_margins

    def setMinContactArea(self, min_val):
        self.min_contact_area = min_val

    def setBackgroundLabel(self, label):
        self.background_label = label

    def setPredefinedProperties(self, names):
        self.properties = names

    def computedGraph(self):
        return self.graph

    def run(self):
        img = vtk_img_to_sp_img(self.image)

        properties_to_compute = self.properties
        if not 'barycenter' in properties_to_compute:
            properties_to_compute = ['barycenter'] + properties_to_compute

        graph = tissue_graph_from_image(img,
                   background=self.background_label,
                   features=properties_to_compute,
                   exclude_marginal_cells=True-self.use_margins,
                   wall_area_threshold=self.min_contact_area)
        # self.graph_data = gnomon.core.cellgraphdata_pluginFactory().create("gnomonCellGraphDataPropertyGraph").from_property_graph(graph)

        positions = graph.cell_property_dict('barycenter')
        for k, dim in enumerate(['x','y','z']):
            graph.cell_property_dict('barycenter_'+dim)
            for l in positions.keys():
                graph.cell_property_dict('barycenter_'+dim)[l] = positions[l][k]

        self.graph_data = gnomon.core.cellGraphData_pluginFactory().create("gnomonCellGraphDataPropertyGraph")
        self.graph_data.set_property_graph(graph)
        self.graph.setData(self.graph_data)
