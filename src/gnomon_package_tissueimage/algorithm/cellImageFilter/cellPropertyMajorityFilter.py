import logging
from copy import deepcopy

import numpy as np

from dtkcore import d_inliststring, d_int

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import cellImageInput, cellImageOutput
from gnomon.core import gnomonAbstractCellImageFilter

from timagetk import TissueImage3D
from timagetk_geometry.features.cells import cell_categorical_feature_majority_filtering


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Cell Majority Filter")
@cellImageInput("tissue", data_plugin="gnomonCellImageDataTissueImage")
@cellImageOutput("out_tissue", data_plugin="gnomonCellImageDataTissueImage")
class cellPropertyMajorityFilter(gnomonAbstractCellImageFilter):
    """Apply a majority filter on a categorical cell property.

    The method assigns to each cell the most represented value among itself and
    its neighbors.
    """

    def __init__(self):
        super().__init__()

        self.tissue = {}
        self.out_tissue = {}

        self._parameters = {
            'property_name': d_inliststring("Property", "", [""], "Categorical cell property on which to apply the filtering"),
            'neighborhood': d_int("Neighborhood", 1, 1, 10, "Topological distance up to which to consider cells neighbors"),
            'layer': d_inliststring("Layer", '', [''], "Layer on which to restrict the filtering"),
            'iterations': d_int("Iterations", 1, 1, 10, "Number of times the filtering will be repeated")
        }

    def __del__(self):
        pass

    def refreshParameters(self):
        if len(self.tissue)>0:
            tissue = list(self.tissue.values())[0]

            property_name = self['property_name']
            feature_names = [f for f in tissue.cells.feature_names() if len(tissue.cells.feature(f)) > 0]
            feature_is_scalar = []
            for f in feature_names:
                cell_feature = np.array(list(tissue.cells.feature(f).values()))
                feature_is_scalar += [cell_feature.ndim == 1]
            feature_names = [f for f, b in zip(feature_names, feature_is_scalar) if b]
            if len(feature_names) == 0:
                feature_names = [""]
            property_name = property_name if property_name in feature_names else ""
            self._parameters['property_name'].setValues(feature_names)
            self._parameters['property_name'].setValue(property_name)

            layer = self['layer']
            layer_values = [""]
            has_layer = 'layer' in tissue.cells.feature_names() and len(tissue.cells.feature('layer')) > 0
            if has_layer:
                layer_values += [str(l) for l in np.sort(np.unique(list(tissue.cells.feature('layer').values())))]
            layer = layer if layer in layer_values else ""
            self._parameters['layer'].setValues(layer_values)
            self._parameters['layer'].setValue(layer)

    def run(self):
        self.out_tissue = {}
        self.set_max_progress(2*len(self.tissue))

        for time in self.tissue.keys():
            self.set_progress_message(f"T {time} : output init")
            tissue = self.tissue[time]

            out_tissue = TissueImage3D(tissue, not_a_label=0, background=1)
            out_tissue.cells = deepcopy(tissue.cells)
            out_tissue.cells.image = out_tissue
            out_tissue.walls = deepcopy(tissue.walls)
            out_tissue.walls.image = out_tissue
            self.increment_progress()

            if self['property_name'] in tissue.cells.feature_names():
                self.set_progress_message(f"T {time} : applying majority filter")
                layer_restriction = None if self['layer'] == "" else int(self['layer'])
                cell_categorical_feature_majority_filtering(
                    out_tissue,
                    self['property_name'],
                    neighborhood=self['neighborhood'],
                    iterations=self['iterations'],
                    layer_restriction=layer_restriction
                )
            self.increment_progress()

            self.out_tissue[time] = out_tissue
