import logging
from copy import deepcopy

import numpy as np

from dtkcore import d_inliststring, d_bool

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import cellImageInput, cellImageOutput
from gnomon.core import gnomonAbstractCellImageFilter

from timagetk import TissueImage3D
from timagetk_geometry.features.cells import cell_feature_neighbor_propagation

@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Neighbor Propagation")
@cellImageInput("tissue", data_plugin="gnomonCellImageDataTissueImage")
@cellImageOutput("out_tissue", data_plugin="gnomonCellImageDataTissueImage")
class cellPropertyNeighborPropagation(gnomonAbstractCellImageFilter):
    """Propagate a cell property iteratively to neighboring cells.

    """

    def __init__(self):
        super().__init__()

        self.tissue = {}
        self.out_tissue = {}

        self._parameters = {
            'property_name': d_inliststring("Property", "", [""], "Cell property on which to perform the propagation"),
            'weighting': d_inliststring("Weighting", 'wall_area', ['uniform', 'volume', 'wall_area'], "Weight function used in the propagation"),
            'categorical': d_bool("Categorical", False, "Whether to consider the property as cell categories"),
        }

    def __del__(self):
        pass

    def refreshParameters(self):
        if len(self.tissue)>0:
            tissue = list(self.tissue.values())[0]

            property_name = self['property_name']
            feature_names = [f for f in tissue.cells.feature_names() if len(tissue.cells.feature(f)) > 0]
            feature_is_scalar = []
            for f in feature_names:
                cell_feature = np.array(list(tissue.cells.feature(f).values()))
                feature_is_scalar += [cell_feature.ndim == 1]
            feature_names = [f for f, b in zip(feature_names, feature_is_scalar) if b]
            if len(feature_names) == 0:
                feature_names = [""]
            property_name = property_name if property_name in feature_names else ""
            self._parameters['property_name'].setValues(feature_names)
            self._parameters['property_name'].setValue(property_name)

    def run(self):
        self.out_tissue = {}
        self.set_max_progress(2*len(self.tissue))

        for time in self.tissue.keys():
            self.set_progress_message(f"T {time} : output init")
            tissue = self.tissue[time]

            out_tissue = TissueImage3D(tissue, not_a_label=0, background=1)
            out_tissue.cells = deepcopy(tissue.cells)
            out_tissue.cells.image = out_tissue
            out_tissue.walls = deepcopy(tissue.walls)
            out_tissue.walls.image = out_tissue
            self.increment_progress()

            if self['property_name'] in tissue.cells.feature_names():
                self.set_progress_message(f"T {time} : performing neighbor propagation")
                cell_feature_neighbor_propagation(
                    out_tissue,
                    self['property_name'],
                    weighting=self['weighting'],
                    categorical=self['categorical'],
                    update_feature=True
                )
            self.increment_progress()

            self.out_tissue[time] = out_tissue
