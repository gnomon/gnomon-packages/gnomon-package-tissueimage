import logging
from copy import deepcopy

import numpy as np

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import cellImageInput, cellImageOutput
from gnomon.core import gnomonAbstractCellImageFilter

from timagetk import TissueImage3D
from dtkcore import d_int, d_real

@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Cell Proximity Crop")
@cellImageInput("tissue", data_plugin="gnomonCellImageDataTissueImage")
@cellImageOutput("out_tissue", data_plugin="gnomonCellImageDataTissueImage")
class cellProximityCrop(gnomonAbstractCellImageFilter):
    """ Apply crop with a cell label and an extent
    """
    def __init__(self):
        super().__init__()

        self.tissue = {}
        self.out_tissue = {}
        self._parameters = {}
        self._parameters["cell_label"] = d_int("Cell Label", 2, 2, 10, "Cell label at the center of the crop")
        self._parameters["extent"] = d_real("Extent", 10., 0., 10., 2, "Crop extent")


    def __del__(self):
        pass
    
    def refreshParameters(self):
        if len(self.tissue)>0:
            tissue = list(self.tissue.values())[0]

            self._parameters["cell_label"].setMax(np.max(tissue.cell_ids()))

            self._parameters["extent"].setMin(np.max(tissue.voxelsize))
            self._parameters["extent"].setMax(np.max(tissue.get_extent()))


    def run(self):
        self.out_tissue = {}
        self.set_max_progress(3*len(self.tissue))
        
        for time in self.tissue.keys():
            self.set_progress_message(f"T {time} : output init")
            tissue = self.tissue[time]

            range_list = []
            for order, axis in enumerate("zyx"):
                cell_center = tissue.cells.feature(f"barycenter_{axis}")[self["cell_label"]]
                min_range = np.maximum(0, (cell_center - self["extent"]/2) / tissue.voxelsize[order])
                max_range = np.minimum(tissue.shape[order], (cell_center + self["extent"]/2) / tissue.voxelsize[order])
                range_list.append((int(min_range), int(max_range)))

            temp_array = tissue.get_array()[
                range_list[0][0]:range_list[0][1],
                range_list[1][0]:range_list[1][1],
                range_list[2][0]:range_list[2][1]
            ]

            out_tissue = TissueImage3D(temp_array, voxelsize=tissue.voxelsize, not_a_label=0, background=1)
            self.increment_progress()

            self.set_progress_message(f"T {time} : updating features")
            out_tissue.cells = deepcopy(tissue.cells)
            out_tissue.cells.image = out_tissue
            cell_feature_names = [f for f in tissue.cells.feature_names() if len(tissue.cells.feature(f)) > 0]
            for feature_name in cell_feature_names:
                feature_dict = {c: tissue.cells.feature(feature_name)[c] for c in out_tissue.cell_ids()}
                out_tissue.cells.set_feature(feature_name, feature_dict)
            
            out_tissue.walls = deepcopy(tissue.walls)
            out_tissue.walls.image = out_tissue
            wall_feature_names = [f for f in tissue.walls.feature_names() if len(tissue.walls.feature(f)) > 0]
            for feature_name in wall_feature_names:
                feature_dict = {c: tissue.walls.feature(feature_name)[c] for c in out_tissue.wall_ids()}
                out_tissue.walls.set_feature(feature_name, feature_dict)
            self.increment_progress()

            self.set_progress_message(f"T {time} : updating cell barycenters")
            out_tissue.cells.set_feature('barycenter', {})
            positions = out_tissue.cells.barycenter()
            for k, dim in enumerate(['x','y','z']):
                out_tissue.cells.set_feature('barycenter_'+dim, {l:p[k] for l,p in positions.items()})
            self.increment_progress()

            self.out_tissue[time] = out_tissue
