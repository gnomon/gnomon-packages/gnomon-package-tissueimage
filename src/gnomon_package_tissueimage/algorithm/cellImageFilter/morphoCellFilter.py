from dtkcore import d_inliststring
from dtkcore import d_int

from gnomon.utils import algorithmPlugin
from gnomon.utils import load_plugin_group
from gnomon.utils.decorators import cellImageInput
from gnomon.utils.decorators import cellImageOutput
from gnomon.core import gnomonAbstractCellImageFilter

from timagetk.algorithms.morphology import label_filtering
from timagetk.components.tissue_image import TissueImage3D

load_plugin_group("cellImageData")

@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Miscellaneous Cell Image filters")
@cellImageInput("in_tissue", data_plugin="gnomonCellImageDataTissueImage")
@cellImageOutput("out_tissue", data_plugin="gnomonCellImageDataTissueImage")
class morphoCellFilter(gnomonAbstractCellImageFilter):

    def __init__(self):
        super(morphoCellFilter, self).__init__()
        self.in_tissue = {}
        self.out_tissue = {}

        self._parameters = {}
        self._parameters['method'] = d_inliststring("Morphological operation to apply", "erosion", ["erosion", "dilation", "opening", "closing"])
        self._parameters['radius'] = d_int("Radius of the structuring element", 1, 1, 50)
        self._parameters['iterations'] = d_int("Number of iteration of the morphological operation", 1, 1, 5)

    def run(self):
        self.out_tissue = {}
        self.set_max_progress(1*len(self.in_tissue))

        for time in self.in_tissue.keys():
            self.set_progress_message(f"T {time} : label filtering")
            img = self.in_tissue[time]  # This is a SpatialImage!
            filtered_img = label_filtering(img,
                                           method=self['method'],
                                           radius=self['radius'],
                                           iterations=self['iterations'])
            self.increment_progress()

            tissue = TissueImage3D(filtered_img, background=1, not_a_label=0)
            self.out_tissue[time] = tissue
