import numpy as np
import scipy.ndimage as nd

from dtkcore import d_bool
from dtkcore import d_inliststring
from dtkcore import d_int
from dtkcore import d_real

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import imageInput, pointCloudInput, cellImageOutput
from gnomon.core import gnomonAbstractCellImageFromImage

from timagetk import SpatialImage, LabelledImage, TissueImage3D
from timagetk.components.labelled_image import relabel_from_mapping
from timagetk.algorithms.regionalext import regional_extrema
from timagetk.algorithms.connexe import connected_components
from timagetk.algorithms.watershed import watershed


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Auto Watershed")
@imageInput('img', data_plugin='gnomonImageDataMultiChannelImage')
@pointCloudInput('seed_points', data_plugin='gnomonPointCloudDataPandas')
@cellImageOutput('tissue', data_plugin="gnomonCellImageDataTissueImage")
class autoSeededWatershedSegmentation(gnomonAbstractCellImageFromImage):
    """Perform an auto-seeded watershed segmentation of a 3D membrane image.

    """

    def __init__(self):
        super().__init__()

        self._parameters = {}
        self._parameters['membrane_channel'] = d_inliststring("Membrane channel", "", [""], "Membrane marker channel on which to perform the segmentation")

        self._parameters['gaussian_sigma'] = d_real("Gaussian sigma", 0.75, 0., 50., 2, "Standard deviation of the Gaussian kernel used for the detection of seeds")
        self._parameters['seg_gaussian_sigma'] = d_real("Segmentation Gaussian sigma", 0.5, 0., 50., 2, "Standard deviation of the Gaussian kernel used for the watershed")
        self._parameters['h_min'] = d_int("H-min", 2, 0, 255, "High threshold of the H-transform for the detection of seeds")
        self._parameters['max_volume'] = d_int("Max volume", 1000, 0, 100000, "Volume threshold for the suppression of oversized cells (µm3)")

        self._parameters['padding'] = d_bool("Padding", False, "Whether to pad the upper part of the image image with zeros before segmentation")
        self._parameters['orientation'] = d_inliststring('Orientation', "up", ["up", "down"], "Whether to upper part corresponds to the top or the bottom of the image stack")
        self._parameters['seeds_only'] = d_bool("Seeds", False, "Whether to return only the seeds of the watershed instead of the full segmentation")

        self._parameter_groups = {}
        for parameter_name in ['padding', 'orientation']:
            self._parameter_groups[parameter_name] = 'padding'
        for parameter_name in ['seeds_only']:
            self._parameter_groups[parameter_name] = 'output'

        self.img = {}
        self.seed_points = {}
        self.tissue = {}

    def refreshParameters(self):
        if len(self.img) > 0:
            image = list(self.img.values())[0]
            if image is not None:
                if len(image) == 1:
                    if 'membrane_channel' in self._parameters.keys():
                        del self._parameters['membrane_channel']
                else:
                    if 'membrane_channel' not in self._parameters:
                        self._parameters['membrane_channel'].setValues([""])
                    membrane_channel = self['membrane_channel']
                    self._parameters['membrane_channel'].setValues(list(image.keys()))
                    if membrane_channel in image.keys():
                        self._parameters['membrane_channel'].setValue(membrane_channel)
                    else:
                        self._parameters['membrane_channel'].setValue(list(image.keys())[0])

                image = list(image.values())[0]
                if image.dtype == np.uint8:
                    self._parameters['h_min'].setMax(255)
                    self._parameters['h_min'].setValue(2)
                elif image.dtype == np.uint16:
                    self._parameters['h_min'].setMax(65535)
                    self._parameters['h_min'].setValue(200)

    def run(self):
        self.set_max_progress((5-self['seeds_only'])*len(self.img))
        self.tissue = {}

        for time in self.img.keys():

            in_image = self.img[time]

            if 'membrane_channel' in self._parameters.keys():
                img = in_image[self['membrane_channel']]
            else:
                img = list(in_image.values())[0]

            img_array = img.get_array()

            if self['padding']:
                self.set_progress_message(f"T {time} : padding")
                pad_width = img.shape[0] // 4
                if self['orientation'] == 'up':
                    img_array = np.concatenate([
                        img_array,
                        np.zeros((pad_width, img.shape[1], img.shape[2]), dtype=img.dtype)
                    ], axis=0)
                elif self['orientation'] == 'down':
                    img_array = np.concatenate([
                        np.zeros((pad_width, img.shape[1], img.shape[2]), dtype=img.dtype),
                        img_array
                    ], axis=0)

            self.set_progress_message(f"T {time} : smoothing")
            smooth_array = nd.gaussian_filter(img_array, sigma=self['gaussian_sigma']/np.array(img.voxelsize))
            smooth_img = SpatialImage(smooth_array, voxelsize=img.voxelsize)
            self.increment_progress()

            self.set_progress_message(f"T {time} : generating seed image")
            ext_img = regional_extrema(smooth_img, height=self['h_min'], method='minima')
            seed_img = connected_components(ext_img, connectivity=18, low_threshold=1, high_threshold=self['h_min'])
            self.increment_progress()

            if not self['seeds_only']:
                self.set_progress_message(f"T {time} : watershed segmentation")
                seg_smooth_array = nd.gaussian_filter(img_array, sigma=self['seg_gaussian_sigma']/np.array(img.voxelsize))
                seg_smooth_img = SpatialImage(seg_smooth_array, voxelsize=img.voxelsize)
                seg_img = watershed(seg_smooth_img, seed_img)
                self.increment_progress()
            else:
                seg_img = seed_img

            if self['padding']:
                self.set_progress_message(f"T {time} : padding")
                if self['orientation'] == 'up':
                    seg_img_array = seg_img.get_array()[:-pad_width]
                elif self['orientation'] == 'down':
                    seg_img_array = seg_img.get_array()[pad_width:]
                seg_img = LabelledImage(seg_img_array, voxelsize=img.voxelsize, not_a_label=0)
                print(img.shape, seg_img.shape)

            tissue = TissueImage3D(seg_img, not_a_label=0, background=1)
            if self['max_volume'] > 0:
                self.set_progress_message(f"T {time} : big cells culling")
                volumes = tissue.cells.volume(real=True)
                volumes = {cid: vol for cid, vol in volumes.items() if cid not in [0, 1]}
                big_cells = [cid for cid, vol in volumes.items() if vol >= self['max_volume']]
                if len(big_cells) > 0:
                    seg_img = relabel_from_mapping(seg_img, {cid: 1 for cid in big_cells}, clear_unmapped=False)
                    tissue = TissueImage3D(seg_img, not_a_label=0, background=1)
            self.increment_progress()

            self.set_progress_message(f"T {time} : computing barycenters")
            positions = tissue.cells.barycenter()
            for k, dim in enumerate(['x','y','z']):
                tissue.cells.set_feature('barycenter_'+dim, {l:p[k] for l, p in positions.items()})
            self.increment_progress()

            self.tissue[time] = tissue

