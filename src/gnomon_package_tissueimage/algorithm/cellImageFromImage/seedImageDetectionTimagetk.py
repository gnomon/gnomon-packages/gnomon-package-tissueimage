import numpy as np
import scipy.ndimage as nd

from dtkcore import d_bool
from dtkcore import d_inliststring
from dtkcore import d_int
from dtkcore import d_real

from gnomon.utils import algorithmPlugin
from gnomon.utils import load_plugin_group
from gnomon.utils.decorators import cellImageOutput
from gnomon.utils.decorators import imageInput
from gnomon.utils.decorators import pointCloudInput
from gnomon.core import gnomonAbstractCellImageFromImage

from timagetk import TissueImage3D
from timagetk.tasks.segmentation import watershed_preprocessing
from timagetk.tasks.segmentation import seeded_watershed
from timagetk.tasks.segmentation import watershed_postprocessing

load_plugin_group("cellImageData")


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Seed detection")
@imageInput('img', data_plugin='gnomonImageDataMultiChannelImage')
@cellImageOutput('seeds', data_plugin="gnomonCellImageDataTissueImage")
class seedImageDetectionTimagetk(gnomonAbstractCellImageFromImage):
    """Perform a watershed seed detection on a membrane-marked tissue 3D image.

    Seeds are detected for each cell in the tissue using a local intensity minimum
    detector called H-Transform.

    """

    def __init__(self):
        super().__init__()

        self._parameters = {}
        self._parameters['membrane_channel'] = d_inliststring("Membrane channel", "", [""], "Membrane marker channel on which to perform the segmentation")

        self._parameters['equalize'] = d_bool("Equalize", True, "Whether to perform *adaptive histogram equalization* to image prior to any other step.")
        self._parameters['gaussian_sigma'] = d_real("Gaussian sigma", 0.5, 0., 50., 2, "Standard deviation of the Gaussian kernel used for the detection of seeds")

        self._parameters['h_min'] = d_int("H-min", 2, 0, 255, "High threshold of the H-transform for the detection of seeds")

        self._parameter_groups = {}
        for parameter_name in ['equalize']:
            self._parameter_groups[parameter_name] = 'preprocessing'
        for parameter_name in ['gaussian_sigma', 'h_min']:
            self._parameter_groups[parameter_name] = 'seed_detection'
        
        self.img = {}
        self.seeds = {}


    def refreshParameters(self):
        if len(self.img) > 0:
            image = list(self.img.values())[0]
            if image is not None:
                if len(image) == 1:
                    if 'membrane_channel' in self._parameters.keys():
                        del self._parameters['membrane_channel']
                else:
                    if 'membrane_channel' not in self._parameters:
                        self._parameters['membrane_channel'].setValues([""])
                    membrane_channel = self['membrane_channel']
                    self._parameters['membrane_channel'].setValues(list(image.keys()))
                    if membrane_channel in image.keys():
                        self._parameters['membrane_channel'].setValue(membrane_channel)
                    else:
                        self._parameters['membrane_channel'].setValue(list(image.keys())[0])

                image = list(image.values())[0]
                if image.dtype == np.uint8:
                    self._parameters['h_min'].setMax(255)
                    self._parameters['h_min'].setValue(2)
                elif image.dtype == np.uint16:
                    self._parameters['h_min'].setMax(65535)
                    self._parameters['h_min'].setValue(200)

    def run(self):
        self.tissue = {}
        self.set_max_progress(3*len(self.img))

        for time in self.img.keys():
            in_image = self.img[time]

            if 'membrane_channel' in self._parameters.keys():
                img = in_image[self['membrane_channel']]
            else:
                img = list(in_image.values())[0]

            self.set_progress_message(f"T {time} : watershed preprocessing")
            smooth_img, _ = watershed_preprocessing(img, sigma=self['gaussian_sigma'], equalize_hist=self['equalize'])
            self.increment_progress()

            self.set_progress_message(f"T {time} : generating seeds")
            seg_img, seed_img, _ = seeded_watershed(smooth_img, h_min=self['h_min'])
            self.increment_progress()

            self.set_progress_message(f"T {time} : conversion to TissueImage3D")
            seeds = TissueImage3D(seed_img, not_a_label=0, background=1)
            self.increment_progress()
            self.seeds[time] = seeds
