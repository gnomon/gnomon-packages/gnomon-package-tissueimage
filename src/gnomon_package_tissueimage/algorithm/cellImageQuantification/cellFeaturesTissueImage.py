import logging
from copy import deepcopy

import numpy as np
import pandas as pd

from dtkcore import d_inliststringlist

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import cellImageInput
from gnomon.utils.decorators import cellImageOutput
from gnomon.utils.decorators import dataFrameOutput
from gnomon.utils.decorators import imageInput
from gnomon.core import gnomonAbstractCellImageQuantification

from timagetk import TissueImage3D
from timagetk.components.tissue_image import cell_layers_from_image
from timagetk.features.pandas_tools import cells_to_dataframe

tissue_property_names = ['barycenter', 'inertia_axis', 'principal_direction_norms', 'shape_anisotropy', 'area', 'volume']
other_property_names = ['surface_area']
topological_property_names = ['neighbors', 'number_of_neighbors', 'layer']


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Cell Features Quantification")
@cellImageInput("tissue", data_plugin="gnomonCellImageDataTissueImage")
@cellImageOutput("out_tissue", data_plugin="gnomonCellImageDataTissueImage")
@dataFrameOutput('df', data_plugin="gnomonDataFrameDataPandas")
class cellFeaturesTissueImage(gnomonAbstractCellImageQuantification):
    """
    Compute geometrical or topological properties of the cells.

    The method computes one or several properties on each cell in the
    image. The computable properties are:
     * The volume of each cell in microm^3
     * The layer (1 or 2) to which the cell belongs
     * The number of neigboring cells in the tissue.

    """

    def __init__(self):
        super().__init__()

        self.tissue = {}
        self.img_dict = {}

        self.out_tissue = {}
        self.df = {}

        self._parameters = {}
        self._parameters['properties'] = d_inliststringlist("properties", ["volume"], tissue_property_names + other_property_names + topological_property_names, "Geometrical or topological properties to compute on each cell")

    def __del__(self):
        pass

    def run(self):
        self.out_tissue = {}
        self.df = {}
        self.set_max_progress((1 + len(self["properties"]))*len(self.tissue))

        for time in self.tissue.keys():
            tissue = self.tissue[time]

            self.set_progress_message(f"T {time} : initializing output")
            out_tissue = TissueImage3D(tissue, not_a_label=0, background=1)
            out_tissue.cells = deepcopy(tissue.cells)
            out_tissue.cells.image = out_tissue
            out_tissue.walls = deepcopy(tissue.walls)
            out_tissue.walls.image = out_tissue
            self.increment_progress()

            for property_name in self['properties']:
                self.set_progress_message(f"T {time} : computing {property_name}")
                if property_name in tissue_property_names:
                    getattr(out_tissue.cells, property_name)()
                elif property_name == 'layer':
                    cell_layers = {c:4 for c in out_tissue.cell_ids()}
                    neighbors = out_tissue.neighbors()
                    layer_cells = neighbors[1]
                    cell_layers.update({c:1 for c in layer_cells})
                    for l in [2,3]:
                        layer_cells = [c for c in out_tissue.cell_ids()
                                         if any([cell_layers[n]==l-1 for n in neighbors[c] if n!=1])
                                         and cell_layers[c]>l-1]
                        cell_layers.update({c:l for c in layer_cells})
                    out_tissue.cells.set_feature('layer', cell_layers)
                elif property_name == 'surface_area':
                    wall_areas = out_tissue.walls.area()
                    cell_surface_areas = {
                        c: wall_areas.get((1, c), np.nan) for c in out_tissue.cell_ids()
                    }
                    out_tissue.cells.set_feature('surface_area', cell_surface_areas)
                else:
                    out_tissue.cells.set_feature(property_name, getattr(out_tissue.cells, property_name)())
                self.increment_progress()

            self.out_tissue[time] = out_tissue

            self.df[time] = cells_to_dataframe(out_tissue.cells, cell_ids=out_tissue.cell_ids())
