import logging
from copy import deepcopy

import numpy as np
import pandas as pd

from dtkcore import d_bool

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import cellImageInput
from gnomon.utils.decorators import cellImageOutput
from gnomon.utils.decorators import dataFrameOutput
from gnomon.utils.decorators import imageInput
from gnomon.core import gnomonAbstractCellImageQuantification

from timagetk import TissueImage3D
from timagetk.components.tissue_image import cell_layers_from_image
from timagetk.features.pandas_tools import cells_to_dataframe


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Cell Volumetric Growth")
@cellImageInput("tissue", data_plugin="gnomonCellImageDataTissueImage")
@cellImageOutput("out_tissue", data_plugin="gnomonCellImageDataTissueImage")
@dataFrameOutput('df', data_plugin="gnomonDataFrameDataPandas")
class cellVolumetricGrowth(gnomonAbstractCellImageQuantification):
    """
    Compute volumetric growth rates on lineaged cells.

    """

    def __init__(self):
        super().__init__()

        self.tissue = {}
        self.img_dict = {}

        self.out_tissue = {}
        self.df = {}

        self._parameters = {}
        self._parameters['log_growth'] = d_bool("log", False, "Compute the log volumetric growth")

    def __del__(self):
        pass

    def run(self):
        self.out_tissue = {}
        self.df = {}

        self.set_max_progress(2*len(self.tissue) + 2*len(self.tissue) - 1)

        sorted_times = np.sort(list(self.tissue.keys()))

        assert all(['ancestor' in self.tissue[time].cells.feature_names() for time in sorted_times])

        for time in sorted_times:
            tissue = self.tissue[time]
            self.set_progress_message(f"T {time} : initializing output")
            out_tissue = TissueImage3D(tissue, not_a_label=0, background=1)
            out_tissue.cells = deepcopy(tissue.cells)
            out_tissue.cells.image = out_tissue
            out_tissue.walls = deepcopy(tissue.walls)
            out_tissue.walls.image = out_tissue

            out_tissue.cells.set_feature('volume', out_tissue.cells.volume())
            self.out_tissue[time] = out_tissue
            self.increment_progress()

        volumetric_growths = {
            sorted_times[-1]: {l: np.nan for l in self.tissue[sorted_times[-1]].cell_ids()}
        }
        for (mother_time, daughter_time) in zip(sorted_times[:-1], sorted_times[1:]):
            mother_tissue = self.out_tissue[mother_time]
            daughter_tissue = self.out_tissue[daughter_time]

            self.set_progress_message(f"T {mother_time} : computing lineage volume")

            ancestor_dict = daughter_tissue.cells.feature('ancestor')

            mother_volumes = mother_tissue.cells.feature('volume')
            daughter_volumes = daughter_tissue.cells.feature('volume')

            lineage_volumes = {m_l: np.nan for m_l in mother_tissue.cell_ids()}
            for d_l, m_l in ancestor_dict.items():
                if d_l in daughter_volumes and m_l in lineage_volumes:
                    if np.isnan(lineage_volumes[m_l]):
                        lineage_volumes[m_l] = 0.
                    lineage_volumes[m_l] += daughter_volumes[d_l]
            self.increment_progress()

            self.set_progress_message(f"T {mother_time} : computing volumetric growth")
            mother_volumetric_growth = {}
            for m_l in mother_tissue.cell_ids():
                mother_volumetric_growth[m_l] = lineage_volumes[m_l]/mother_volumes[m_l]
            volumetric_growths[mother_time] = mother_volumetric_growth
            self.increment_progress()

        for time in self.tissue.keys():
            self.set_progress_message(f"T {time} : applying feature")
            out_tissue = self.out_tissue[time]
            out_tissue.cells.set_feature('volumetric_growth', volumetric_growths[time])
            if self['log_growth']:
                out_tissue.cells.set_feature('log_volumetric_growth', {
                    l: np.log10(g) for l,g in volumetric_growths[time].items()
                })
            self.increment_progress()

            self.df[time] = cells_to_dataframe(out_tissue.cells, cell_ids=out_tissue.cell_ids())
