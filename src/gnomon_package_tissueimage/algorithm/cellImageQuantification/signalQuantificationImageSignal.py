import logging
from copy import deepcopy

import numpy as np
import pandas as pd
import scipy.ndimage as nd

from dtkcore import d_inliststringlist
from dtkcore import d_real

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import cellImageInput
from gnomon.utils.decorators import cellImageOutput
from gnomon.utils.decorators import dataFrameOutput
from gnomon.utils.decorators import imageInput
from gnomon.core import gnomonAbstractCellImageQuantification

from timagetk.algorithms.signal_quantification import quantify_cell_signal_intensity
from timagetk import TissueImage3D, SpatialImage, MultiChannelImage
from timagetk.features.pandas_tools import cells_to_dataframe


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Image Intensity Quantification")
@cellImageInput("tissue", data_plugin="gnomonCellImageDataTissueImage")
@imageInput('img', data_plugin='gnomonImageDataMultiChannelImage')
@cellImageOutput("out_tissue", data_plugin="gnomonCellImageDataTissueImage")
@dataFrameOutput('df', data_plugin="gnomonDataFrameDataPandas")
class signalQuantificationImageSignal(gnomonAbstractCellImageQuantification):
    """Compute average image signal as a property of each cell

    The method estimates a value of signal for every cell label by averaging
    the image intensity within the region defined by the cell. The regions can
    be eroded to consider only inner signal.
    """

    def __init__(self):
        super().__init__()

        self.tissue = {}
        self.img = {}
        self.out_tissue = {}
        self.df = {}

        self._parameters = {}
        self._parameters['channels'] = d_inliststringlist("Channels", [""],[""], "Channels on which to quantify signal intensity inside cells")
        self._parameters['erosion_radius'] = d_real("Erosion radius", 1, 0, 5, 1, "Radius of the structuring element for the cell erosion")

    def __del__(self):
        pass

    def refreshParameters(self):
        if len(self.img)>0:
            img = list(self.img.values())[0]

            if len(img.channel_names) == 1:
                if 'channels' in self._parameters.keys():
                    del self._parameters['channels']
            else:
                if not 'channels' in self._parameters.keys():
                    self._parameters['channels'] = d_inliststringlist("channels", [""], [""], "Channels on which to quantify signal intensity inside cells")
                self._parameters['channels'].setValues(img.channel_names)
                self._parameters['channels'].setValue(img.channel_names)

    def run(self):
        self.set_max_progress(3*len(self.tissue))
        self.df = {}
        self.out_tissue = {}

        for time in self.tissue.keys():
            if not time in self.img.keys():
                logging.error("Impossible to quantify! No signal image provided")
                self.df = {}
                return

            tissue = self.tissue[time]

            self.set_progress_message(f"T {time} : initializing output")
            out_tissue = TissueImage3D(tissue, not_a_label=0, background=1)
            out_tissue.cells = deepcopy(tissue.cells)
            out_tissue.cells.image = out_tissue
            out_tissue.walls = deepcopy(tissue.walls)
            out_tissue.walls.image = out_tissue
            self.increment_progress()

            self.set_progress_message(f"T {time} : computing cell signal intensity")
            img = self.img[time]
            if 'channels' in self._parameters:
                cell_signals = {
                    channel: quantify_cell_signal_intensity(
                        img.get_channel(channel),
                        out_tissue,
                        erosion_radius=self['erosion_radius']
                    )
                    for channel in self['channels']
                }
            else:
                cell_signals = quantify_cell_signal_intensity(img, out_tissue, erosion_radius=self['erosion_radius'])
            self.increment_progress()

            self.set_progress_message(f"T {time} : applying feature")
            for channel in cell_signals:
                signal_name = channel if channel != "" else "image_signal"
                out_tissue.cells.set_feature(signal_name, cell_signals[channel])
            self.increment_progress()

            self.out_tissue[time] = out_tissue

            self.df[time] = cells_to_dataframe(out_tissue.cells, cell_ids=out_tissue.cell_ids())
