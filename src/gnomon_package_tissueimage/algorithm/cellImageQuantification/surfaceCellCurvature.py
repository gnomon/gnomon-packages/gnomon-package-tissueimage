import logging
from copy import deepcopy

import numpy as np
import pandas as pd

from dtkcore import d_inliststringlist
from dtkcore import d_real

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import cellImageInput
from gnomon.utils.decorators import cellImageOutput
from gnomon.utils.decorators import dataFrameOutput
from gnomon.utils.decorators import imageInput
from gnomon.core import gnomonAbstractCellImageQuantification

from timagetk import TissueImage3D, LabelledImage
from timagetk.features.array_tools import find_geometric_median
from timagetk.features.pandas_tools import cells_to_dataframe

from timagetk.algorithms.resample import isometric_resampling

from cellcomplex.property_topomesh.creation import triangle_topomesh
from cellcomplex.property_topomesh.analysis import compute_topomesh_property, compute_topomesh_vertex_property_from_faces


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Surface Curvature")
@cellImageInput("tissue", data_plugin="gnomonCellImageDataTissueImage")
@cellImageOutput("out_tissue", data_plugin="gnomonCellImageDataTissueImage")
@dataFrameOutput('df', data_plugin="gnomonDataFrameDataPandas")
class surfaceCellCurvature(gnomonAbstractCellImageQuantification):
    """
    Compute properties reflecting the local curvature of surfac cells.

    The method estimates the curvature tensor at the level of each surface
    cell based on a triangulation of cell surface centers. It allows to
    estimate:
     * The mean curvature
     * The gaussian curvature
     * The principal curvature tensor itself

    """

    def __init__(self):
        super().__init__()

        self.tissue = {}

        self.out_tissue = {}
        self.df = {}

        self._parameters = {}
        self._parameters['curvature'] = d_inliststringlist("Curvature",
                                                           ["mean_curvature", "gaussian_curvature"],
                                                           ["mean_curvature", "gaussian_curvature", "principal_curvature_tensor"],
                                                           "Type of curvature properties to compute on each surface cell")
        self._parameters['resampling_voxelsize'] = d_real("Resampling voxelsize", 1., 0.1, 5., 1, "Voxelsize to use for the resampling allowing to estimate the surface cell triangulation")

        self._parameter_groups = {}
        for parameter_name in ['resampling_voxelsize']:
            self._parameter_groups[parameter_name] = 'surface_triangulation'

    def __del__(self):
        pass

    def run(self):
        self.set_max_progress(7*len(self.tissue) + len(self.tissue)*len(self['curvature']))
        self.out_tissue = {}
        self.df = {}

        if len(self.tissue) == 0:
            logging.error("Impossible to quantify! No segmented image provided")
            return

        for time in self.tissue.keys():
            tissue = self.tissue[time]

            self.set_progress_message(f"T {time} : initializing output")
            out_tissue = TissueImage3D(tissue, not_a_label=0, background=1)
            out_tissue.cells = deepcopy(tissue.cells)
            out_tissue.cells.image = out_tissue
            out_tissue.walls = deepcopy(tissue.walls)
            out_tissue.walls.image = out_tissue
            self.increment_progress()

            self.set_progress_message(f"T {time} : resampling")
            resampling_voxelsize = self['resampling_voxelsize']

            seg_img = LabelledImage(out_tissue.get_array(), voxelsize=out_tissue.voxelsize, not_a_label=0)
            seg_img = isometric_resampling(seg_img, method=resampling_voxelsize, option='label')

            image_cell_junctions = seg_img.topological_elements(element_order=[0, 1, 2])
            self.increment_progress()

            print(" - Extracting L1 triangulation")
            self.set_progress_message(f"T {time} : extracting L1 triangulation")
            image_cell_vertices = image_cell_junctions[0]
            cell_tetras = np.sort(list(image_cell_vertices.keys()))
            l1_cell_triangles = cell_tetras[:, 1:][cell_tetras[:, 0] == 1]

            l1_cells = np.unique(l1_cell_triangles)

            image_cell_edges = image_cell_junctions[1]
            cell_junction_triangles = np.sort(list(image_cell_edges.keys()))
            valid_cell_junction_triangles = np.all(np.any([cell_junction_triangles == c for c in l1_cells], axis=0), axis=1)
            cell_junction_triangles = cell_junction_triangles[valid_cell_junction_triangles]

            l1_cell_triangles = np.unique(np.sort(list(l1_cell_triangles) + list(cell_junction_triangles)), axis=0)
            self.increment_progress()

            def cell_normal_estimation(c, seg_img=seg_img):
                cell_interface_coordinates = seg_img.surfel_coordinates((c, 1))
                if (len(cell_interface_coordinates)) == 1 and (len(list(cell_interface_coordinates.values())[0]) > 1):

                    cell_interface_points = np.array(list(cell_interface_coordinates.values())[0]) * np.array(seg_img.voxelsize)
                    l1_cell_interface_center = cell_interface_points[find_geometric_median(cell_interface_points)]
                    l1_cell_center = np.mean(np.transpose(np.where(seg_img == c)) * np.array(seg_img.voxelsize), axis=0)[::-1]

                    cell_interface_covariance = np.cov(cell_interface_points, rowvar=False)
                    cell_interface_eval, cell_interface_evec = np.linalg.eig(cell_interface_covariance)
                    l1_cell_normal = cell_interface_evec[:, np.argmin(np.abs(cell_interface_eval))]
                    normal_orientation = np.sign(np.dot(l1_cell_normal, l1_cell_interface_center - l1_cell_center))
                    normal_orientation += (normal_orientation == 0)
                    l1_cell_normal *= normal_orientation
                    return l1_cell_normal, l1_cell_interface_center, l1_cell_center
                else:
                    return None, None, None

            self.set_progress_message(f"T {time} : estimating cells normals and centers")
            l1_cell_normals_and_centers = [cell_normal_estimation(c) for c in l1_cells]

            l1_cell_normals = {}
            l1_cell_interface_centers = {}
            l1_cell_centers = {}
            for c, (l1_cell_normal, l1_cell_interface_center, l1_cell_center) in zip(l1_cells, l1_cell_normals_and_centers):
                if l1_cell_normal is not None:
                    l1_cell_normals[c] = l1_cell_normal
                    l1_cell_interface_centers[c] = l1_cell_interface_center
                    l1_cell_centers[c] = l1_cell_center
            self.increment_progress()

            self.set_progress_message(f"T {time} : cleaning cell traingles")
            valid_cell_triangles = np.all(np.any([l1_cell_triangles == c for c in l1_cell_normals.keys()], axis=0), axis=1)
            l1_cell_triangles = l1_cell_triangles[valid_cell_triangles]
            self.increment_progress()

            self.set_progress_message(f"T {time} : generating topomesh")
            triangulation_topomesh = triangle_topomesh(l1_cell_triangles, l1_cell_centers)
            triangulation_topomesh.update_wisp_property('normal', 0, l1_cell_normals)
            self.increment_progress()

            self.set_progress_message(f"T {time} : computing curvature")
            compute_topomesh_property(triangulation_topomesh, 'mean_curvature', 2)
            self.increment_progress()
            for property_name in self['curvature']:
                self.set_progress_message(f"T {time} : computing {property_name}")
                compute_topomesh_vertex_property_from_faces(triangulation_topomesh, property_name, neighborhood=3, adjacency_sigma=1.2)
                zero_value = np.zeros_like(triangulation_topomesh.wisp_property(property_name,0).values()[0])*np.nan
                if zero_value.ndim == 0:
                    zero_value = np.nan
                out_tissue.cells.set_feature(property_name, dict(zip(out_tissue.cell_ids(),
                                                                     [triangulation_topomesh.wisp_property(property_name,0)[c]
                                                                      if c in triangulation_topomesh.wisps(0)
                                                                      else zero_value
                                                                      for c in out_tissue.cell_ids()])))
                self.increment_progress()

            self.out_tissue[time] = out_tissue

            self.df[time] = cells_to_dataframe(out_tissue.cells, cell_ids=out_tissue.cell_ids())
