import logging
from copy import deepcopy

import numpy as np
import pandas as pd

from dtkcore import d_inliststringlist

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import cellImageInput, cellImageOutput, dataFrameOutput
from gnomon.core import gnomonAbstractCellImageQuantification

from timagetk import TissueImage3D
from timagetk.features.pandas_tools import cell_walls_to_dataframe

wall_feature_names = ['inertia_axis', 'area']

@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Wall Features Quantification")
@cellImageInput("tissue", data_plugin="gnomonCellImageDataTissueImage")
@cellImageOutput("out_tissue", data_plugin="gnomonCellImageDataTissueImage")
@dataFrameOutput('df', data_plugin="gnomonDataFrameDataPandas")
class wallFeaturesTissueImage(gnomonAbstractCellImageQuantification):
    """Compute geometrical properties of the cell walls.

    The method computes one or several properties on each cell interface
    (wall) in the image. The computable properties are:
     * The area of each wall in microm^2
     * The inertia axes of each wall

    """

    def __init__(self):
        super().__init__()

        self.tissue = {}

        self.out_tissue = {}
        self.df = {}

        self._parameters = {}
        self._parameters['properties'] = d_inliststringlist("properties", ["area"], wall_feature_names, "Geometrical properties to compute on each cell wall")

    def __del__(self):
        pass

    def run(self):
        self.set_max_progress(2*len(self.tissue) + len(self.tissue)*len(self["properties"]))
        self.out_tissue = {}
        self.df = {}

        for time in self.tissue.keys():
            tissue = self.tissue[time]

            self.set_progress_message(f"T {time} : initializing output")
            out_tissue = TissueImage3D(tissue, not_a_label=0, background=1)
            out_tissue.cells = deepcopy(tissue.cells)
            out_tissue.cells.image = out_tissue
            out_tissue.walls = deepcopy(tissue.walls)
            out_tissue.walls.image = out_tissue
            self.increment_progress()

            if not 'center_x' in out_tissue.walls.feature_names():
                self.set_progress_message(f"T {time} : computing wall centers")
                wall_centers = out_tissue.walls.geometric_median()
                out_tissue.walls.set_feature('center', wall_centers)
                for k, dim in enumerate(['x','y','z']):
                    out_tissue.walls.set_feature('center_'+dim, {w: p[k] for w, p in wall_centers.items()})
            self.increment_progress()

            for property_name in self['properties']:
                self.set_progress_message(f"T {time} : computing {property_name}")
                out_tissue.walls.set_feature(property_name, getattr(out_tissue.walls, property_name)())
                self.increment_progress()

            self.out_tissue[time] = out_tissue

            self.df[time] = cell_walls_to_dataframe(out_tissue.walls, cell_ids=out_tissue.cell_ids())
