import logging
from copy import deepcopy

import numpy as np
import pandas as pd
import scipy.ndimage as nd

from dtkcore import d_bool, d_real, d_inliststringlist

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import cellImageInput, imageInput, cellImageOutput, dataFrameOutput
from gnomon.core import gnomonAbstractCellImageQuantification

from timagetk.algorithms.signal_quantification import quantify_cell_interface_signal_intensity
from timagetk import TissueImage3D
from timagetk.features.pandas_tools import cell_walls_to_dataframe


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Wall Image Signal")
@cellImageInput("tissue", data_plugin="gnomonCellImageDataTissueImage")
@imageInput('img', data_plugin='gnomonImageDataMultiChannelImage')
@cellImageOutput("out_tissue", data_plugin="gnomonCellImageDataTissueImage")
@dataFrameOutput('df', data_plugin="gnomonDataFrameDataPandas")
class wallSignalQuantificationImage(gnomonAbstractCellImageQuantification):
    """Measure image signal intensity at the level of the cell walls.

    The method computes a value of signal for each cell wall by averaging the
    image intensity at a given distance of the cell-cell interface.
    """

    def __init__(self):
        super().__init__()

        self.tissue = {}
        self.img = {}
        self.out_tissue = {}
        self.df = {}

        self._parameters = {}
        self._parameters['channels'] = d_inliststringlist("Channels", [""],[""], "Channels on which to quantify signal intensity at the cell walls")
        self._parameters['first_layer'] = d_bool("L1 only", True, "Whether to quantify signals only on L1 cell interfaces or all tissue")
        self._parameters['wall_distance'] = d_real("Wall distance", 1, 0, 10, 1, "Distance up to which the image signal will be averaged")

    def __del__(self):
        pass

    def refreshParameters(self):
        if len(self.img)>0:
            img = list(self.img.values())[0]

            if len(img.channel_names) == 1:
                if 'channels' in self._parameters.keys():
                    del self._parameters['channels']
            else:
                if not 'channels' in self._parameters.keys():
                    self._parameters['channels'] = d_inliststringlist("channels", [""], [""], "Channels on which to quantify signal intensity at the cell walls")
                self._parameters['channels'].setValues(img.channel_names)
                self._parameters['channels'].setValue(img.channel_names)

    def run(self):
        self.set_max_progress(6*len(self.tissue))
        self.df = {}
        self.out_tissue = {}

        for time in self.tissue.keys():
            if not time in self.img.keys():
                logging.error("Impossible to quantify! No signal image provided")
                self.df = {}
                return

            tissue = self.tissue[time]

            self.set_progress_message(f"T {time} : initializing output")
            out_tissue = TissueImage3D(tissue, not_a_label=0, background=1)
            out_tissue.cells = deepcopy(tissue.cells)
            out_tissue.cells.image = out_tissue
            out_tissue.walls = deepcopy(tissue.walls)
            out_tissue.walls.image = out_tissue
            self.increment_progress()

            img = self.img[time]
            self.set_progress_message(f"T {time} : extracting first layer")
            if self['first_layer']:
                if 'layer' in out_tissue.cells.feature_names():
                    cell_layer = out_tissue.cells.feature('layer')
                    cell_ids = [c for c in out_tissue.cell_ids() if cell_layer[c]==1]
                else:
                    cell_ids = out_tissue.neighbors(1)[1]
            else:
                cell_ids = None
            self.increment_progress()

            self.set_progress_message(f"T {time} : quantifying cell interface signal intensity")
            cell_signals = quantify_cell_interface_signal_intensity(
                img,
                out_tissue,
                cell_ids=cell_ids,
                interface_distance=self['wall_distance']
            )

            for channel in cell_signals:
                signal_name = channel if channel != "" else "image_signal"
                if ('channels' not in self._parameters) or (signal_name in self['channels']):
                    out_tissue.walls.set_feature(signal_name, cell_signals[channel])
            self.increment_progress(3)

            self.set_progress_message(f"T {time} : computing cell interface centers")
            wall_centers = out_tissue.walls.geometric_median()
            out_tissue.walls.set_feature('center', wall_centers)
            for k, dim in enumerate('xyz'):
                out_tissue.walls.set_feature(f'center_{dim}', {w: p[k] for w, p in wall_centers.items()})
            self.increment_progress()

            self.out_tissue[time] = out_tissue

            self.df[time] = cell_walls_to_dataframe(out_tissue.walls, cell_ids=out_tissue.cell_ids())
