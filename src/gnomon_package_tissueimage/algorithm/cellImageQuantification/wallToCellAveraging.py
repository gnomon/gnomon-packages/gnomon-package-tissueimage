import logging
from copy import deepcopy

import numpy as np
import scipy.ndimage as nd
import pandas as pd

from dtkcore import d_inliststringlist

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import cellImageInput, cellImageOutput, dataFrameOutput
from gnomon.core import gnomonAbstractCellImageQuantification

from timagetk import TissueImage3D
from timagetk.features.pandas_tools import cells_to_dataframe

@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Wall To Cell Properties")
@cellImageInput("tissue", data_plugin="gnomonCellImageDataTissueImage")
@cellImageOutput("out_tissue", data_plugin="gnomonCellImageDataTissueImage")
@dataFrameOutput('df', data_plugin="gnomonDataFrameDataPandas")
class wallToCellAveraging(gnomonAbstractCellImageQuantification):
    """Compute cell properties by averaging wall properties

    The method computes for each cell a value of an existing wall property as
    the area-weighted average of the property values on its incident walls.

    """

    def __init__(self):
        super().__init__()

        self.tissue = {}

        self.out_tissue = {}
        self.df = {}

        self._parameters = {}
        self._parameters['properties'] = d_inliststringlist("properties", [], [], "Geometrical properties to compute on each cell wall")

    def __del__(self):
        pass

    def refreshParameters(self):
        if len(self.tissue) > 0:
            tissue = list(self.tissue.values())[0]
            wall_prop = [p for p in tissue.walls.feature_names() if tissue.walls.feature(p) != {} and p!='area']

            properties = self["properties"]
            self._parameters["properties"].setValues(wall_prop)
            properties = [p for p in properties if p in wall_prop]
            if len(properties) == 0:
                properties = wall_prop
            self._parameters["properties"].setValue(properties)

    def run(self):
        self.set_max_progress((2 + 3*len(self["properties"]))*len(self.tissue))
        self.out_tissue = {}
        self.df = {}

        for time in self.tissue.keys():
            tissue = self.tissue[time]

            self.set_progress_message(f"T {time} : initializing output")
            out_tissue = TissueImage3D(tissue, not_a_label=0, background=1)
            out_tissue.cells = deepcopy(tissue.cells)
            out_tissue.cells.image = out_tissue
            out_tissue.walls = deepcopy(tissue.walls)
            out_tissue.walls.image = out_tissue
            self.increment_progress()

            self.set_progress_message(f"T {time} : extracting cell labels and wall areas")
            cell_labels = out_tissue.cell_ids()
            wall_areas = out_tissue.walls.area()
            self.increment_progress()

            for property_name in self['properties']:
                self.set_progress_message(f"T {time} : computing property {property_name}")
                wall_property = out_tissue.walls.feature(property_name)
                wall_property_array = np.array([wall_property[w] for w in wall_property])
                wall_area_array = np.array([wall_areas[w] for w in wall_property])
                wall_labels = np.array([list(w) for w in wall_property])
                self.increment_progress()
                self.set_progress_message(f"T {time} : averaring {property_name}")
                if wall_property_array.ndim == 1:
                    cell_property_array = nd.sum((wall_area_array*wall_property_array)[:, np.newaxis], wall_labels, index=cell_labels)
                    cell_property_array /= nd.sum(wall_area_array[:, np.newaxis], wall_labels, index=cell_labels)
                elif wall_property_array.ndim == 2:
                    cell_property_array = np.transpose([
                        nd.sum((wall_area_array*wall_property_array[:, k])[:, np.newaxis], wall_labels, index=cell_labels)
                        for k in wall_property_array.shape[1]
                    ])
                    cell_property_array /= nd.sum(wall_area_array[:, np.newaxis], wall_labels, index=cell_labels)[:, np.newaxis]
                else:
                    cell_property_array = {}
                self.increment_progress()
                self.set_progress_message(f"T {time} : applying feature")
                feature_name = property_name if not property_name in out_tissue.cells.feature_names() else f"wall_{property_name}"
                out_tissue.cells.set_feature(feature_name, dict(zip(cell_labels, cell_property_array)))
                self.increment_progress()

            self.out_tissue[time] = out_tissue

            self.df[time] = cells_to_dataframe(out_tissue.cells, cell_ids=cell_labels)
