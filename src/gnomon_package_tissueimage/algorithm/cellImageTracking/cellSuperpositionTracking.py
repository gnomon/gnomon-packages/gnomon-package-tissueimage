# {# gnomon, plugin.imports
# do not modify, any code after the gnomon tag will be overwritten
import numpy as np

from dtkcore import d_bool, d_int, d_real, d_inliststring, d_inliststringlist

import gnomon.core

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import cellImageInput, imageInput, dataDictInput, cellImageOutput
# #}
# add your imports before the next gnomon tag
from timagetk.algorithms.blockmatching import blockmatching
from timagetk.components.trsf import Trsf
from timagetk.third_party.ctrl.tracking import CellTracking

# {# gnomon, plugin.class
# do not modify, any code after the gnomon tag will be overwritten
@algorithmPlugin(version='1.0.0', coreversion='1.0.0', name="Naive Cell Tracking")
@cellImageInput(attr='dict_img_seg', data_plugin='gnomonCellImageDataTissueImage')
@imageInput(attr='dict_img', data_plugin='gnomonImageDataMultiChannelImage')
@dataDictInput(attr='dict_transform', data_plugin="gnomonNumpyDataDictData")
@cellImageOutput(attr='tracked_img_seg', data_plugin='gnomonCellImageDataTissueImage')
class cellSuperpositionTracking(gnomon.core.gnomonAbstractCellImageTracking):
    """Compute cell lineage "naively" by deformation and superposition.

    """

    def __init__(self):
        super().__init__()

        self._parameters = {}

        self.dict_img_seg = {}
        self.dict_img = {}
        self.dict_transform = {}
        self.tracked_img_seg = {}

    def run(self):
        self.tracked_img_seg = {}
        times = list(self.dict_img_seg.keys())
        self.set_max_progress(5*(len(times)-1) + 1)

        # - Setting ancestors for the first timestamps
        self.set_progress_message(f"Initializing ancestors")
        seg_0 = self.dict_img_seg[times[0]]
        lineage_dict = {cell_id: cell_id for cell_id in seg_0.cells.ids()}
        seg_0.cells.set_feature('ancestor', lineage_dict)
        self.tracked_img_seg[times[0]] = seg_0
        self.increment_progress()

        for ix, time in enumerate(times[:-1]):
            # - Define the mother (time t) and daughter (time t+1) images
            mother_seg = self.dict_img_seg[time]
            daughter_seg = self.dict_img_seg[times[ix+1]]

            if times[ix+1] in self.dict_img:
                daughter_img = self.dict_img[times[ix+1]]
            if time in self.dict_img:
                mother_img = self.dict_img[time]

            trsf_def = None
            if time in self.dict_transform and "transform" in self.dict_transform[time]:
                trsf_array = self.dict_transform[time]["transform"]
                print(trsf_array, trsf_array.dtype)
                if trsf_array.ndim==2 and trsf_array.shape[0]==4 and trsf_array.shape[1]==4:
                    trsf_def = Trsf(trsf_array)

            if trsf_def is None:
                # Estimate a registration transformation between the mother and daughter image
                self.set_progress_message(f"T {time} : estimating transformation")
                trsf_affine = blockmatching(mother_img, daughter_img, method='affine', channel=mother_img.channel_names[0])
                trsf_def = blockmatching(mother_img, daughter_img, method='vectorfield', init_trsf=trsf_affine, channel=mother_img.channel_names[0])
            self.increment_progress()

            # Initialize a cell tracking object
            self.set_progress_message(f"T {time} : initializing cell tracking")
            track = CellTracking(mother_seg=mother_seg, daughter_seg=daughter_seg, trsf=trsf_def, trsf_direction='forward')
            self.increment_progress()

            # Estimate a lineage based on cell superposition
            self.set_progress_message(f"T {time} : estimating cell lineage")
            lineage_dict = track.naive_lineage()
            self.increment_progress()

            # Get the lineage dict
            self.set_progress_message(f"T {time} : extracting lineage dict")
            lineage_dict = lineage_dict._invert()
            lineage_dict.update({dlab: mother_seg.not_a_label for dlab in daughter_seg.labels() if dlab not in lineage_dict})
            self.increment_progress()

            # Update the property of the daughter segmented image
            self.set_progress_message(f"T {time} : updating properties")
            daughter_seg.cells.set_feature('ancestor', lineage_dict)
            self.increment_progress()

            self.tracked_img_seg[times[ix+1]] = daughter_seg

