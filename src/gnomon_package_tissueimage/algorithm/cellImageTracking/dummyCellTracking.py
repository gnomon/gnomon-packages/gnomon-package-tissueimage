from pprint import pprint

from gnomon.core import gnomonAbstractCellImageTracking
from gnomon.utils.gnomonPlugin import algorithmPlugin
from gnomon.utils.decorators import cellImageInput, imageInput, cellImageOutput


@algorithmPlugin(version="1.0.0", coreversion="1.0.0")
@cellImageInput("tissue_input", data_plugin="gnomonCellImageDataTissueImage")
@imageInput('img', data_plugin='gnomonImageDataMultiChannelImage')
@cellImageOutput("tissue_output", data_plugin="gnomonCellImageDataTissueImage")
class dummyCellTracking(gnomonAbstractCellImageTracking):

    def __init__(self):
        super().__init__()

        self.tissue_input = {}
        self.img = {}
        self.tissue_output = {}

        self._parameters = {}

    def run(self):
        self.tissue_output = {}
        # pprint(self.tissue_input)
        for time in self.tissue_input.keys():
            self.tissue_output[time] = self.tissue_input[time]
