import logging
from copy import deepcopy

import numpy as np

from dtkcore import d_bool, d_int, d_real, d_inliststring, d_inliststringlist

import gnomon.core

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import cellImageInput, imageInput, dataDictInput, cellImageOutput

from timagetk import TissueImage3D


@algorithmPlugin(version='1.0.0', coreversion='1.0.0', name="Manual Tracking")
@cellImageInput(attr='img_seg', data_plugin='gnomonCellImageDataTissueImage')
@imageInput(attr='img_dict', data_plugin='gnomonImageDataMultiChannelImage')
@dataDictInput(attr='input_lineage_trsf', data_plugin="gnomonNumpyDataDictData")
@cellImageOutput(attr='img_seg_with_lineage', data_plugin='gnomonCellImageDataTissueImage')
class manualCellTracking(gnomon.core.gnomonAbstractCellImageTracking):
    """Perform a cell tracking using only a manual input.
    """

    def __init__(self):
        super().__init__()

        self._parameters = {}

        self.img_dict = {}
        self.img_seg = {}
        self.input_lineage_trsf = {}

        self.img_seg_with_lineage = {}

    def refreshParameters(self):
        pass

    def run(self):
        self.set_max_progress(1*(len(self.img_seg)-1)+1)
        self.img_seg_with_lineage = {}
        #self.lineage_tree = {}

        self.set_progress_message(f"Initializing")
        ancestor_dicts = {}
        sorted_times = np.sort(list(self.img_seg.keys()))
        ancestor_dicts[sorted_times[0]] = {l: l for l in self.img_seg[sorted_times[0]].labels()}

        first_seg = TissueImage3D(self.img_seg[sorted_times[0]], not_a_label=0, background=1)
        first_seg.cells = deepcopy(self.img_seg[sorted_times[0]].cells)
        self.img_seg_with_lineage[sorted_times[0]] = first_seg
        self.increment_progress()

        for (mother_time, daughter_time) in zip(sorted_times[:-1], sorted_times[1:]):
            self.set_progress_message(f"T {mother_time} : updating lineage")
            init_lineage_dict = {}
            if mother_time in self.input_lineage_trsf and "manual_lineage" in self.input_lineage_trsf[mother_time]:
                lineage_str = self.input_lineage_trsf[mother_time]["manual_lineage"]
                mother_cells, daughter_cells = eval(lineage_str)
                if len(mother_cells) == len(daughter_cells):
                    init_lineage_dict = {}
                    for m_c, d_c in zip(mother_cells, daughter_cells):
                        if isinstance(d_c, list):
                            init_lineage_dict[m_c] = d_c
                        else:
                            init_lineage_dict[m_c] = [d_c]
            mother_seg = self.img_seg[mother_time]

            daughter_seg = TissueImage3D(self.img_seg[daughter_time], not_a_label=0, background=1)
            daughter_seg.cells = deepcopy(self.img_seg[daughter_time].cells)

            ancestor_dict = {d_l: m_l for m_l, d_ls in init_lineage_dict.items() for d_l in d_ls}
            ancestor_dict.update({d_l: mother_seg.not_a_label for d_l in daughter_seg.labels() if d_l not in ancestor_dict})
            ancestor_dicts[daughter_time] = ancestor_dict

            self.img_seg_with_lineage[daughter_time] = daughter_seg

            for time in ancestor_dicts:
                self.img_seg_with_lineage[time].cells.set_feature('ancestor', ancestor_dicts[time])
            self.increment_progress()
