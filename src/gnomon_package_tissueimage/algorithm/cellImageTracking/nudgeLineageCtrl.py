import logging
from copy import deepcopy

import numpy as np

from dtkcore import d_bool, d_int, d_real, d_inliststring, d_inliststringlist

import gnomon.core

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import cellImageInput, imageInput, dataDictInput, cellImageOutput

from timagetk import LabelledImage, TissueImage3D
from timagetk.components.trsf import Trsf

from timagetk.third_party.ctrl.lineage import Lineage
from timagetk.third_party.ctrl.tracking import IterativeCellTracking

# import treex as tx


@algorithmPlugin(version='1.0.0', coreversion='1.0.0', name="Cell Tracking (ctrl)")
@cellImageInput(attr='img_seg', data_plugin='gnomonCellImageDataTissueImage')
@imageInput(attr='img_dict', data_plugin='gnomonImageDataMultiChannelImage')
@dataDictInput(attr='input_lineage_trsf', data_plugin="gnomonNumpyDataDictData")
@cellImageOutput(attr='img_seg_with_lineage', data_plugin='gnomonCellImageDataTissueImage')
class nudgeLineageCtrl(gnomon.core.gnomonAbstractCellImageTracking):
    """Perform a cell tracking on a segmented tissue image.

    Use a naive method to estimate the lineage between cells of two or more
    consecutive segmented tissue images.
    """

    def __init__(self):
        super().__init__()

        self._parameters = {}
        self._parameters['registration_channel'] = d_inliststring("Registration Channel", "", [""], "Which channel to use to compute image transformations")
        self._parameters['downsampling_factor'] = d_int("Downsampling", 2, 1, 8, "Factor to downsample images for cell overlap computation")

        self._parameters['registration_pyramid_lowest'] = d_int("Pyramid lowest", 2, 0, 4, "Lowest level at which to compute deformation.")
        self._parameters['registration_pyramid_highest'] = d_int("Pyramid highest", 5, 1, 5, "Highest level at which to compute deformation.")
        #self._parameters['registration_block_size'] = d_int("Block size", 5, 1, 20, "Block size used to compute blockmatching deformation.")
        #self._parameters['registration_elastic_sigma'] = d_real("Elastic sigma", 1, 0, 3, 1, "Sigma for elastic regularization in vector field computation")

        self._parameters['anchors_method'] = d_inliststring("Method", "centroids", ["centroids", "vertices"], "Method for the generation of anchors from manual lineage cells")
        self._parameters['anchors_pyramid_lowest'] = d_int("Pyramid lowest", 2, 0, 4, "Lowest level at which to compute deformation.")
        self._parameters['anchors_pyramid_highest'] = d_int("Pyramid highest", 2, 1, 5, "Highest level at which to compute deformation.")
        #self._parameters['anchors_block_size'] = d_int("Block size", 5, 1, 20, "Block size used to compute blockmatching deformation.")
        #self._parameters['anchors_elastic_sigma'] = d_real("Elastic sigma", 1, 0, 3, 1, "Sigma for elastic regularization in vector field computation")

        self._parameters['max_topo_dist'] = d_int("Topological distance",  3, 1, 5, "Topological distance up to which anchors will be generated")

        self._parameter_groups = {}
        for parameter_name in ["registration_pyramid_lowest", "registration_pyramid_highest"]: #+ ["registration_block_size", "registration_elastic_sigma"]:
            self._parameter_groups[parameter_name] = 'registration'
        for parameter_name in ["anchors_method", "anchors_pyramid_lowest", "anchors_pyramid_highest"]: #+ ["anchors_block_size", "anchors_elastic_sigma"]:
            self._parameter_groups[parameter_name] = 'anchors'
        for parameter_name in ["max_topo_dist"]:
            self._parameter_groups[parameter_name] = 'nudge'

        self.img_dict = {}
        self.img_seg = {}
        self.input_lineage_trsf = {}

        self.tracking = None

        self.img_seg_with_lineage = {}
        self.lineage_tree = {}

    def refreshParameters(self):
        if len(self.img_dict) > 0:
            images = list(self.img_dict.values())[0]
            if len(images) == 1:
                if 'registration_channel'  in self._parameters.keys():
                    del self._parameters['registration_channel']
            else:
                if 'registration_channel' not in self._parameters.keys():
                    self._parameters['registration_channel'] = d_inliststring("Registration Channel", "", [""], "Which channel to use to compute image transformations")
                    channel_name = images.channel_names[0]
                else:
                    channel_name = self['registration_channel']
                self._parameters['registration_channel'].setValues(images.channel_names)
                self._parameters['registration_channel'].setValue(channel_name)

    def run(self):
        self.img_seg_with_lineage = {}
        #self.lineage_tree = {}

        if (len(self.img_dict) < 2) or (len(self.img_dict) != len(self.img_seg)) or (np.any([t not in self.img_seg.keys() for t in self.img_dict.keys()])):
            logging.error(f"Input images are not correct, they should be sequences of at least 2 time steps with the same indices")
        else:
            if 'registration_channel' in self._parameters.keys():
                registration_imgs = {t: img.get_channel(self['registration_channel']) for t, img in self.img_dict.items()}
            else:
                registration_imgs = {t: img.get_channel(img.channel_names[0]) for t, img in self.img_dict.items()}

            if np.any([self.img_seg[t].shape != registration_imgs[t].shape for t in self.img_dict.keys()]):
                logging.error(f"Images should have the same shape!")
            else:
                self.set_max_progress(7*(len(self.img_seg)-1) + 2)
                self.set_progress_message(f"Initializing")
                ancestor_dicts = {}
                cost_dicts = {}
                preservation_dicts = {}
                sorted_times = np.sort(list(self.img_dict.keys()))
                ancestor_dicts[sorted_times[0]] = {l: l for l in self.img_seg[sorted_times[0]].labels()}

                first_seg = TissueImage3D(self.img_seg[sorted_times[0]], not_a_label=0, background=1)
                first_seg.cells = deepcopy(self.img_seg[sorted_times[0]].cells)
                self.img_seg_with_lineage[sorted_times[0]] = first_seg
                self.increment_progress()

                for (mother_time, daughter_time) in zip(sorted_times[:-1], sorted_times[1:]):
                    init_trsf = None
                    if mother_time in self.input_lineage_trsf and "transform" in self.input_lineage_trsf[mother_time]:
                        self.set_progress_message(f"T {mother_time} : applying transformation")
                        trsf_array = self.input_lineage_trsf[mother_time]["transform"]
                        print(trsf_array, trsf_array.dtype)
                        if trsf_array.ndim==2 and trsf_array.shape[0]==4 and trsf_array.shape[1]==4:
                            init_trsf = Trsf(trsf_array)
                    self.increment_progress()

                    init_lineage_dict = {}
                    if mother_time in self.input_lineage_trsf and "manual_lineage" in self.input_lineage_trsf[mother_time]:
                        self.set_progress_message(f"T {mother_time} : updating lineage from manual input")
                        lineage_str = self.input_lineage_trsf[mother_time]["manual_lineage"]
                        mother_cells, daughter_cells = eval(lineage_str)
                        if len(mother_cells) == len(daughter_cells):
                            init_lineage_dict = {}
                            for m_c, d_c in zip(mother_cells, daughter_cells):
                                if isinstance(d_c, list):
                                    init_lineage_dict[m_c] = d_c
                                else:
                                    init_lineage_dict[m_c] = [d_c]
                    self.increment_progress()

                    mother_img = registration_imgs[mother_time]
                    mother_seg = self.img_seg[mother_time]

                    daughter_img = registration_imgs[daughter_time]
                    daughter_seg = TissueImage3D(self.img_seg[daughter_time], not_a_label=0, background=1)
                    daughter_seg.cells = deepcopy(self.img_seg[daughter_time].cells)

                    nudge = (self.tracking is not None) and (len(init_lineage_dict) > 0)
                    if not nudge:
                        if len(init_lineage_dict) < 4:
                            logging.error(f"There should be at least 4 cell manual lineages, but there are only {len(init_lineage_dict)}!")
                            init_lineage_dict = None
                        else:
                            self.set_progress_message(f"T {mother_time} : computing initial lineage")
                            init_lineage_dict = Lineage(init_lineage_dict)
                        self.set_progress_message(f"T {mother_time} : iterative cell tracking")
                        self.tracking = IterativeCellTracking(mother_img=mother_img,
                                                              daughter_img=daughter_img,
                                                              mother_seg=mother_seg,
                                                              daughter_seg=daughter_seg,
                                                              init_trsf=init_trsf,
                                                              init_lineage_dict=init_lineage_dict,
                                                              trsf_direction='forward',
                                                              ds=self['downsampling_factor'])

                    self.tracking.max_topo_dist = self["max_topo_dist"]
                    self.tracking.anchors_method = self["anchors_method"]
                    self.tracking.reg_highest_level = self["registration_pyramid_highest"]
                    self.tracking.reg_lowest_level = self["registration_pyramid_lowest"]
                    self.tracking.pts_highest_level = self["anchors_pyramid_highest"]
                    self.tracking.pts_lowest_level = self["anchors_pyramid_lowest"]
                    self.increment_progress()

                    if nudge:
                        self.set_progress_message(f"T {mother_time} : applying nudge")
                        self.tracking.init_lineage_dict = {}
                        # TODO: modify -bl-size n_vx n_vx n_vx to 7 or 9?
                        self.tracking.make_nudge(init_lineage_dict, blk_params='-elastic-sigma 2 2 2 -max-iter 6')
                    else:
                        self.set_progress_message(f"T {mother_time} : using naive lineage")
                        self.tracking.naive_lineage()
                    self.increment_progress()

                    self.set_progress_message(f"T {mother_time} : extracting ancestors")
                    lineage_dict = self.tracking.lineage_dict
                    ancestor_dict = {d_l: int(m_l) for m_l, d_ls in lineage_dict.items() for d_l in d_ls}
                    ancestor_dict.update({d_l: int(mother_seg.not_a_label) for d_l in daughter_seg.labels() if d_l not in ancestor_dict})
                    self.increment_progress()

                    ancestor_dicts[daughter_time] = ancestor_dict
                    self.set_progress_message(f"T {mother_time} : evaluating lineage cost")
                    if mother_time == sorted_times[0]:
                        cost_dict = {m_l: float(self.tracking.lineage_cost['mother'][m_l])
                                          if m_l in self.tracking.lineage_cost['mother']
                                          else np.nan
                                     for m_l in mother_seg.labels()}
                        cost_dicts[mother_time] = cost_dict
                    cost_dict = {d_l: float(self.tracking.lineage_cost['daughter'][d_l])
                                      if d_l in self.tracking.lineage_cost['daughter']
                                      else np.nan
                                 for d_l in daughter_seg.labels()}
                    cost_dicts[daughter_time] = cost_dict
                    self.increment_progress()

                    self.set_progress_message(f"T {mother_time} : evaluating lineage quality")
                    self.tracking.evaluate_lineage()
                    preservation_dict = {m_l: self.tracking.preservation_score[m_l]
                                              if m_l in self.tracking.preservation_score
                                              else np.nan
                                         for m_l in mother_seg.labels()}
                    preservation_dicts[mother_time] = preservation_dict
                    if daughter_time == sorted_times[-1]:
                        preservation_dict = {d_l: float(self.tracking.preservation_score[m_l])
                                                  if m_l in self.tracking.preservation_score
                                                  else np.nan
                                             for d_l, m_l in ancestor_dict.items()}
                        preservation_dicts[daughter_time] = preservation_dict

                    self.img_seg_with_lineage[daughter_time] = daughter_seg
                    self.increment_progress()

                self.set_progress_message(f"updating features in output")
                for time in ancestor_dicts:
                    self.img_seg_with_lineage[time].cells.set_feature('ancestor', ancestor_dicts[time])
                    self.img_seg_with_lineage[time].cells.set_feature('lineage_quality', preservation_dicts[time])
                    self.img_seg_with_lineage[time].cells.set_feature('lineage_cost', cost_dicts[time])
                self.increment_progress()

                # lineage_tree = tx.Tree()
                # lineage_tree.add_attribute_to_id('label',0)
                # lineage_tree.add_attribute_to_id('time',0)
                # lineage_label_nodes = {}
                # for i_time, time in enumerate(sorted_times):
                #     p_img = deepcopy(self.img_seg[time])
                #     lineage_dict = {l:lineage_dicts[time][l] if l in lineage_dicts[time] else 0 for l in p_img.labels}
                #     p_img.update_image_property('mother_label', lineage_dict)
                #
                #     self.img_seg_with_lineage[time] = p_img
                #
                #     if i_time == 0:
                #         lineage_subtree = tx.Tree()
                #         lineage_subtree.add_attribute_to_id('label', 0)
                #         lineage_subtree.add_attribute_to_id('time', time)
                #         lineage_tree.add_subtree_to_id(lineage_subtree)
                #         child_id = lineage_tree.my_children[-1].my_id
                #         lineage_label_nodes[(time,0)] = child_id
                #         for l in p_img.labels:
                #             lineage_subtree = tx.Tree()
                #             lineage_subtree.add_attribute_to_id('label', l)
                #             lineage_subtree.add_attribute_to_id('time', time)
                #             lineage_tree.add_subtree_to_id(lineage_subtree)
                #             child_id = lineage_tree.my_children[-1].my_id
                #             lineage_label_nodes[(time,l)] = child_id
                #     else:
                #         mother_id = lineage_label_nodes[(sorted_times[i_time-1],0)]
                #         lineage_subtree = tx.Tree()
                #         lineage_subtree.add_attribute_to_id('label', 0)
                #         lineage_subtree.add_attribute_to_id('time', time)
                #         lineage_tree.add_subtree_to_id(lineage_subtree,node_id=mother_id)
                #         child_id = lineage_tree.subtree(mother_id).my_children[-1].my_id
                #         lineage_label_nodes[(time,0)] = child_id
                #         for l in p_img.labels:
                #             mother_id = lineage_label_nodes[(sorted_times[i_time-1],lineage_dict[l])]
                #             lineage_subtree = tx.Tree()
                #             lineage_subtree.add_attribute_to_id('label', l)
                #             lineage_subtree.add_attribute_to_id('time', time)
                #             lineage_tree.add_subtree_to_id(lineage_subtree,node_id=mother_id)
                #             child_id = lineage_tree.subtree(mother_id).my_children[-1].my_id
                #             lineage_label_nodes[(time,l)] = child_id
                # self.lineage_tree[0] = lineage_tree
