import numpy as np
import scipy.ndimage as nd
from dtkcore import d_inliststring
from dtkcore import d_int
from dtkcore import d_real
from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import imageInput
from gnomon.utils.decorators import imageOutput
from gnomon.core import gnomonAbstractImageFilter
from timagetk import MultiChannelImage, SpatialImage

@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Threshold Edge Enhancement")
@imageInput("img_", data_plugin="gnomonImageDataMultiChannelImage")
@imageOutput("filtered_images", data_plugin="gnomonImageDataMultiChannelImage")
class boundaryEdgeEnhancement(gnomonAbstractImageFilter):
    """Improve the edge signal of an image by adding an artificial boundary

    The algorithm uses the image intensity, filtered by a Gaussian kernel of
    standard deviation `gaussian_sigma` to estimate the contour of the
    object of interest by thresholding (using the `threshold` parameter). The
    contour is then added to the original image after a Gaussian smoothing of
    `boundary_sigma` multiplied by a given `edge_intensity`.
    """
    def __init__(self):
        super().__init__()
        self.img_ = {}

        self.filtered_images = {}

        self._parameters = {}
        self._parameters['channel'] = d_inliststring("Channel", "", [""], "Channel on which to extract the tissue boundary")
        self._parameters['gaussian_sigma'] = d_real("Gaussian Sigma", 2., 0., 50., 1, "Standard deviation of the Gaussian kernel used to smooth signal")
        self._parameters['threshold'] = d_int("Threshold", 40,0,255, "Intensity threshold used to detect the tissue boundary")
        self._parameters['boundary_sigma'] = d_real("Boundary sigma", 0.5, 0., 5., 1, "Standard deviation of the Gaussian kernel used to compute the boundary")
        self._parameters['edge_intensity'] = d_int("Edge intensity", 255, 0, 255, "Intensity factor for the added boundary edge")

        self._parameter_groups = {}
        for parameter_name in ['gaussian_sigma', 'threshold']:
            self._parameter_groups[parameter_name] = 'boundary_detection'

    def refreshParameters(self):
        if len(self.img_)>0:
            img_ = list(self.img_.values())[0]
            if len(img_) == 1 and 'channel' in self._parameters:
                del self._parameters['channel']
            else:
                if 'channel' not in self._parameters.keys():
                    self._parameters['channel'] = d_inliststring("Channel", "", [""], "Channel on which to extract the tissue boundary")
                self._parameters['channel'].setValues(list(img_.keys()))
                self._parameters['channel'].setValue(list(img_.keys())[0])

            img = img_[list(img_.keys())[0]]

            if img.dtype == np.uint8:
                self._parameters['threshold'].setMax(255)
                self._parameters['threshold'].setValue(20)
            elif img.dtype == np.uint16:
                self._parameters['threshold'].setMax(65535)
                self._parameters['threshold'].setValue(1000)
            else:
                self._parameters['threshold'].setMax(img.max())
                self._parameters['threshold'].setValue(int(img.max() / 10.))

    def run(self):
        self.set_max_progress(3*sum(len(img.channel_names) for img in self.img_.values()))
        for time in self.img_.keys():

            in_image = self.img_[time]
            temp_filtered_images = []

            for channel in in_image.channel_names:
                self.set_progress_message(f"T {time} - channel {channel} : creating mask")
                img = in_image[channel]

                voxelsize = np.array(img.voxelsize)
                size = np.array(img.shape)

                mask_img = (nd.gaussian_filter(img,self['gaussian_sigma']/voxelsize) > self['threshold'])
                mask_img = nd.binary_fill_holes(mask_img)
                mask_img = mask_img.astype(float)
                self.increment_progress()

                self.set_progress_message(f"T {time} - channel {channel} : finding the edge")
                edge_img = np.linalg.norm([nd.gaussian_filter1d(mask_img,self['boundary_sigma']/voxelsize[k],order=1,axis=k) for k in range(3)],axis=0)
                edge_img = self['edge_intensity']*edge_img
                self.increment_progress()

                self.set_progress_message(f"T {time} - channel {channel} : increasing edge intensity")
                max_intensity = 65535 if img.dtype==np.uint16 else 255
                enhanced_img = np.minimum(img.get_array().astype(float) + edge_img, max_intensity).astype(img.dtype)
                enhanced_img = SpatialImage(enhanced_img,voxelsize=img.voxelsize)

                temp_filtered_images.append(enhanced_img)
                self.increment_progress()

            self.filtered_images[time] = MultiChannelImage(temp_filtered_images, channel_names=in_image.channel_names)