import numpy as np
import scipy.ndimage as nd
from dtkcore import d_string
from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import imageInput
from gnomon.utils.decorators import imageOutput
from gnomon.core import gnomonAbstractImageFilter
from timagetk import MultiChannelImage, SpatialImage


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Channel Names Edit")
@imageInput("img_", data_plugin="gnomonImageDataMultiChannelImage")
@imageOutput("filtered_images", data_plugin="gnomonImageDataMultiChannelImage")
class channelNamesEdit(gnomonAbstractImageFilter):
    """Change the channel names of the image."""

    def __init__(self):
        super().__init__()
        self.img_ = {}

        self.filtered_images = {}

        self._parameters = {}

        self._parameter_groups = {}

    def refreshParameters(self):
        if len(self.img_) > 0:
            img_ = list(self.img_.values())[0]
            for channel in img_.keys():
                if channel not in self._parameters:
                    self._parameters[channel] = d_string(f"{channel} ==>", channel, f"Edit channel {channel} name")
            for channel in self._parameters.keys():
                if channel not in img_.keys():
                    del self._parameters[channel]

    def run(self):
        self.set_max_progress(1*sum(len(img.channel_names) for img in self.img_.values()))
        for time in self.img_.keys():

            in_image = self.img_[time]
            temp_filtered_images = []
            channel_names = []
            for channel in in_image.keys():
                self.set_progress_message(f"T {time} - channel {channel} : changing name to {self[channel]}")
                print(f"{channel} ==> {self[channel]}")
                channel_names.append(self[channel])
                temp_filtered_images.append(SpatialImage(in_image[channel]))
                self.increment_progress()

            self.filtered_images[time] = MultiChannelImage(temp_filtered_images, channel_names=channel_names)
