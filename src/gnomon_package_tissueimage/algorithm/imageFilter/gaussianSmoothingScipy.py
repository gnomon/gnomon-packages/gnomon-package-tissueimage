from dtkcore import d_real, d_inliststringlist

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import imageInput, imageOutput
from gnomon.core import gnomonAbstractImageFilter

import numpy as np
import scipy.ndimage as nd

from timagetk import SpatialImage, MultiChannelImage


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Scipy Gaussian Smoothing")
@imageInput("img", data_plugin="gnomonImageDataMultiChannelImage")
@imageOutput("filtered_img", data_plugin="gnomonImageDataMultiChannelImage")
class gaussianSmoothingScipy(gnomonAbstractImageFilter):
    """Smooth an image using the Gaussian filter from SciPy.

    The algorithm smooths a 3D image by applying an isotropic Gaussian filter
    for which the standard deviation can be set. Only the selected channels
    will appear in the filtered image.
    """

    def __init__(self):
        super().__init__()

        self.img = {}
        self.filtered_img = {}

        self._parameters = {}
        self._parameters['channels'] = d_inliststringlist("Channels", [""], [""], "Channels on which to compute the algorithm")
        self._parameters['gaussian_sigma'] = d_real("Gaussian sigma",  1., 0, 10., 2, "Standard deviation of the Gaussian kernel")
        
        self._parameter_groups = {}

    def refreshParameters(self):
        img = list(self.img.values())[0]

        if len(img) == 1:
            if 'channels' in self._parameters.keys():
                del self._parameters['channels']
        else:
            if not 'channels' in self._parameters.keys():
                self._parameters['channels'] = d_inliststringlist("Channels", [""], [""], "Channels on which to compute the algorithm")
            self._parameters['channels'].setValues(list(img.keys()))
            self._parameters['channels'].setValue(list(img.keys()))

    def run(self):
        if len(self.img) > 0:
            if 'channels' in self._parameters:
                channel_names = self['channels']
            else:
                channel_names = list(self.img.values())[0].channel_names
            self.set_max_progress(1 * len(channel_names) * len(self.img))

        print("-----------------")
        print(channel_names)
        print("-----------------")

        self.filtered_img = {}
        for time in self.img:
            filtered_img = {}
            for channnel_name in self.img[time]:
                if channnel_name in channel_names:
                    self.set_progress_message(f"T {time} - Smoothing channel {channnel_name}")
                    img = self.img[time][channnel_name]

                    sigma = self['gaussian_sigma']/np.array(img.voxelsize)
                    filtered_img[channnel_name] = SpatialImage(
                        nd.gaussian_filter(img.get_array(), sigma=sigma),
                        voxelsize=img.voxelsize
                    )
                    self.increment_progress()
            self.filtered_img[time] = MultiChannelImage(filtered_img)
