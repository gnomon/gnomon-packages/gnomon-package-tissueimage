from dtkcore import d_inliststring
from dtkcore import d_inliststringlist
from dtkcore import d_real

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import imageInput
from gnomon.utils.decorators import imageOutput
from gnomon.core import gnomonAbstractImageFilter

from timagetk.algorithms.exposure import global_contrast_stretch
from timagetk import MultiChannelImage

@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Global Contrast Stretch")
@imageInput('images', data_plugin="gnomonImageDataMultiChannelImage")
@imageOutput('filtered_images', data_plugin="gnomonImageDataMultiChannelImage")
class globalContrastStretch(gnomonAbstractImageFilter):

    def __init__(self):
        super().__init__()

        self.images = {}
        self.filtered_images = {}

        self._parameters = {}
        self._parameters['channels'] = d_inliststringlist("Channels", [""], [""], "Channels on which to compute the algorithm")
        self._parameters['pc_min'] = d_real("Min percentile", 2, 0, 255, 2, "Lower percentile use to define the lower range of the input image for contrast stretching")
        self._parameters['pc_max'] = d_real("Max percentile", 99, 0, 255, 2, "Upper percentile use to define the lower range of the input image for contrast stretching")

        self._parameter_groups = {}

        self.__doc__ = global_contrast_stretch.__doc__

    def refreshParameters(self):
        if len(self.images)>0:
            images = list(self.images.values())[0]
            if len(images) == 1 and 'channels' in self._parameters:
                del self._parameters['channels']
            else:
                if 'channels' not in self._parameters.keys():
                    self._parameters['channels'] = d_inliststringlist("Channels", [""], [""], "Channels on which to compute the algorithm")
                self._parameters['channels'].setValues(images.channel_names)
                self._parameters['channels'].setValue(images.channel_names)

    def run(self):
        self.set_max_progress(1*sum(
            len([
                channel for channel in img.channel_names if 'channels' not in self._parameters.keys() or channel in self['channels']
            ]) for img in self.images.values()
        ))
        self.filtered_images = {}

        for time in self.images.keys():
            in_image = self.images[time]
            self.filtered_images[time] = {}
            temp_filtered_images = []
            temp_channel_names = []
            for channel in in_image.channel_names:
                if 'channels' not in self._parameters.keys() or channel in self['channels']:
                    self.set_progress_message(f"T {time} - channel {channel}")
                    img = in_image[channel]
                    temp_channel_names += [channel]
                    filtered_img = global_contrast_stretch(img, pc_min=self['pc_min'], pc_max=self['pc_max'])
                    temp_filtered_images.append(filtered_img)
                    self.increment_progress()

            if len(temp_channel_names) == 1:
                temp_channel_names = [""]
            self.filtered_images[time] = MultiChannelImage(temp_filtered_images, channel_names=temp_channel_names)
