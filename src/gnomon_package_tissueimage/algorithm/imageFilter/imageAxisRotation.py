# coding=utf-8

import numpy as np
from dtkcore import d_inliststring
from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import imageInput
from gnomon.utils.decorators import imageOutput
from gnomon.core import gnomonAbstractImageFilter
from timagetk import MultiChannelImage, SpatialImage


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Axis Rotation")
@imageInput("img_", data_plugin="gnomonImageDataMultiChannelImage")
@imageOutput("filtered_images", data_plugin="gnomonImageDataMultiChannelImage")
class imageAxisRotation(gnomonAbstractImageFilter):
    """Rotate the image by a given angle around the X, Y or Z axis.

    The algorithm simply rotates all the channels of the image around one of
    the image axes (X, Y or Z axis) by an angle of 90°, -90° or 180°.
    """

    def __init__(self):
        super().__init__()

        self.img_ = {}
        self.filtered_images = {}

        self._parameters = {}
        self._parameters['image_axis'] = d_inliststring("image_axis", "z", ["x", "y" ,"z"], "Image axis around which to rotate")
        self._parameters['rotation_angle'] = d_inliststring("rotation_angle", "90°", ["90°", "-90°", "180°"], "Angle of rotation around image axis")

        self._parameter_groups = {}

        self.axis_index = {'x':2,'y':1,'z':0}

    def run(self):
        self.set_max_progress(1*sum(
            len(img.channel_names) for img in self.img_.values()
        ))
        for time in self.img_.keys():

            in_image = self.img_[time]
            self.filtered_images[time] = {}
            temp_filtered_images = []

            for channel in in_image.channel_names:
                self.set_progress_message(f"T {time} - channel {channel}")
                img = in_image[channel]
                voxelsize = np.array(img.voxelsize)
                axis = self.axis_index[self['image_axis']]
                if self['rotation_angle'] == "180°":
                    slices = tuple([slice(None, None, -1 if a != axis else 1) for a in range(3)])
                    rotated_img = img.get_array()[slices]
                    rotated_voxelsize = img.voxelsize
                elif "90°" in self['rotation_angle']:
                    sign = -1 if '-' in self['rotation_angle'] else 1
                    transposition = tuple([[a2 for a2 in range(3) if a2 not in [axis, a]][0] if a != axis else a for a in range(3)])
                    transposed_axes = [a for a in range(3) if a != axis]
                    slices = tuple([slice(None, None, (-1 * sign if a == np.min(transposed_axes) else sign) if a != axis else 1) for a in range(3)])
                    rotated_img = np.transpose(img.get_array(), transposition)[slices]
                    rotated_voxelsize = tuple(list(voxelsize[list(transposition)]))
                temp_filtered_images.append(SpatialImage(rotated_img, voxelsize=rotated_voxelsize))
                self.increment_progress()
            self.filtered_images[time] = MultiChannelImage(temp_filtered_images, channel_names=in_image.channel_names)
