import logging
from copy import deepcopy

import numpy as np

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import imageInput, imageOutput
from gnomon.core import gnomonAbstractImageFilter

from dtkcore import d_range_real, array_real_2

from timagetk import SpatialImage, MultiChannelImage


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Crop Image")
@imageInput("img", data_plugin="gnomonImageDataMultiChannelImage")
@imageOutput("out_img", data_plugin="gnomonImageDataMultiChannelImage")
class imageCrop(gnomonAbstractImageFilter):
    """ Apply a manual crop to an intensity image.
    """
    def __init__(self):
        super().__init__()

        self.img = {}
        self.out_img = {}
        self._parameters = {}
        self._parameters["crop_range_x"] = d_range_real("X Crop range", array_real_2([0, 100]), 0, 100, "Crop range for x_min, x_max")
        self._parameters["crop_range_y"] = d_range_real("Y Crop range", array_real_2([0, 100]), 0, 100, "Crop range for y_min, y_max")
        self._parameters["crop_range_z"] = d_range_real("Z Crop range", array_real_2([0, 100]), 0, 100, "Crop range for z_min, z_max")

    def __del__(self):
        pass
    
    def refreshParameters(self):
        if len(self.img)>0:
            img = list(self.img.values())[0]
            for i_ax, axis in enumerate('zyx'):
                range_min = 0
                range_max = img.shape[i_ax]*img.voxelsize[i_ax]

                self._parameters[f"crop_range_{axis}"].setMin(range_min)
                self._parameters[f"crop_range_{axis}"].setMax(range_max)
                self._parameters[f"crop_range_{axis}"].setValueMin(range_min)
                self._parameters[f"crop_range_{axis}"].setValueMax(range_max)

    def run(self):
        self.out_img = {}

        if len(self.img) > 0:
            channel_names = list(self.img.values())[0].channel_names
            self.set_max_progress(len(channel_names)*len(self.img))

            for time in self.img.keys():
                img = self.img[time]
                channel_names = img.channel_names

                cropped_img = {}
                for channel_name in channel_names:
                    self.set_progress_message(f"T {time} : cropping channel {channel_name}")
                    channel_img = self.img[time].get_channel(channel_name)

                    range_list = []
                    for i_ax, axis in enumerate("zyx"):
                        range_list.append([
                            int(np.round(r / img.voxelsize[i_ax]))
                            for r in self[f"crop_range_{axis}"]
                        ])

                    cropped_array = channel_img.get_array()[
                        range_list[0][0]:range_list[0][1],
                        range_list[1][0]:range_list[1][1],
                        range_list[2][0]:range_list[2][1]
                    ]

                    cropped_img[channel_name] = SpatialImage(cropped_array, voxelsize=img.voxelsize, not_a_label=0, background=1)
                    self.increment_progress()

                self.out_img[time] = MultiChannelImage(cropped_img)
