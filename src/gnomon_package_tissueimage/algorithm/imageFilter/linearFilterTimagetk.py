from dtkcore import d_inliststring
from dtkcore import d_inliststringlist
from dtkcore import d_real
from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import imageInput
from gnomon.utils.decorators import imageOutput
from gnomon.core import gnomonAbstractImageFilter
from timagetk.algorithms.linearfilter import linearfilter
from timagetk import MultiChannelImage
from timagetk.third_party.vt_parser import FILTERING_METHODS

@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Gaussian Filter")
@imageInput("images", data_plugin="gnomonImageDataMultiChannelImage")
@imageOutput("filtered_images", data_plugin="gnomonImageDataMultiChannelImage")
class linearFilterTimagetk(gnomonAbstractImageFilter):
    """Compute a linear filtering of an image.

    The algorithm performs a filtering of the selcted channels of a 3D image by
    an isotropic Gaussian kernel of standard deviation equal to the value of
    the gaussian_sigma parameter. Depending on the method parameter, different
    orders of Gaussian derivatives will be used.
    """

    def __init__(self):
        super().__init__()

        self.images = {}
        self.filtered_images = {}

        self._parameters = {}
        self._parameters['method'] = d_inliststring("Method", "smoothing", FILTERING_METHODS, "Type of linear filter to apply to each channel")
        self._parameters['channels'] = d_inliststringlist("Channels", [""], [""], "Channels on which to compute the algorithm")
        self._parameters['gaussian_sigma'] = d_real("Gaussian sigma",  1., 0, 10., 2, "Standard deviation of the Gaussian kernel")
        
        self._parameter_groups = {}

    def refreshParameters(self):
        image = list(self.images.values())[0]

        if len(image) == 1:
            if 'channels' in self._parameters.keys():
                del self._parameters['channels']
        else:
            if not 'channels' in self._parameters.keys():
                self._parameters['channels'] = d_inliststringlist("Channels", [""], [""], "Channels on which to compute the algorithm")
            self._parameters['channels'].setValues(list(image.keys()))
            self._parameters['channels'].setValue(list(image.keys()))

    def run(self):
        self.set_max_progress(1*sum(
            len(self['channels'] if 'channels' in self._parameters else img.channel_names) for img in self.images.values()
        ))
        self.filtered_images = {}

        for time in self.images.keys():
            if 'channels' in self._parameters:
                channel_names = self['channels']
            else:
                channel_names = self.images[time].channel_names

            temp_filtered_images = []
            for channel in channel_names:
                self.set_progress_message(f"T {time} - channel {channel}")
                img = self.images[time][channel]
                filtered_img = linearfilter(img, method=self['method'], sigma=self['gaussian_sigma'])
                temp_filtered_images.append(filtered_img)
                self.increment_progress()

            if len(channel_names) == 1:
                channel_names = [""]
            self.filtered_images[time] = MultiChannelImage(temp_filtered_images, channel_names=channel_names)
