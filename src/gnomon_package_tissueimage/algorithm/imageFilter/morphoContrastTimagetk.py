from dtkcore import d_inliststringlist
from dtkcore import d_int
from dtkcore import d_real
from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import imageInput
from gnomon.utils.decorators import imageOutput
from gnomon.core import gnomonAbstractImageFilter
from timagetk.algorithms.morphology import morphology_oc_alternate_sequential_filter
from timagetk import MultiChannelImage, SpatialImage

@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Morphological Filter")
@imageInput("images", data_plugin="gnomonImageDataMultiChannelImage")
@imageOutput("filtered_images", data_plugin="gnomonImageDataMultiChannelImage")
class morphoContrastTimagetk(gnomonAbstractImageFilter):
    """
    Performs an alternate sequantial filtering of opening and closing.
    """
    def __init__(self):
        super().__init__()
        self.images = {}
        self.filtered_images = {}

        self._parameters = {}
        self._parameters['channels'] = d_inliststringlist("Channels", [""], [""], "Channels on which to compute the algorithm")
        self._parameters['radius'] = d_real("Radius", 1., 0, 10., 2, "Radius of the structural element for the morphological filter")
        self._parameters['iterations'] = d_int("Iterations", 2, 0, 10, "Number of iterations of the morphological filter")

        self._parameter_groups = {}

        self.__doc__ = morphology_oc_alternate_sequential_filter.__doc__

    def __del__(self):
        del self.filtered_images

    def refreshParameters(self):
        image = list(self.images.values())[0]

        if len(image) == 1:
            if 'channels' in self._parameters.keys():
                del self._parameters['channels']
        else:
            if not 'channels' in self._parameters.keys():
                self._parameters['channels'] = d_inliststringlist("Channels", [""], [""], "Channels on which to compute the algorithm")
            self._parameters['channels'].setValues(list(image.keys()))
            self._parameters['channels'].setValue(list(image.keys()))

    def run(self):
        self.set_max_progress(1*sum(
            len([
                channel for channel in img.channel_names if 'channels' not in self._parameters.keys() or channel in self['channels']
            ]) for img in self.images.values()
        ))
        self.filtered_images = {}

        for time in self.images.keys():
            in_image = self.images[time]
            temp_filtered_images = []
            temp_channel_names = []
            for channel in in_image.channel_names:
                if 'channels' not in self._parameters.keys() or channel in self['channels']:
                    self.set_progress_message(f"T {time} - channel {channel}")
                    img = in_image[channel]
                    temp_channel_names += [channel]
                    filtered_img = morphology_oc_alternate_sequential_filter(img, max_radius=self['radius'], iterations=self['iterations'])
                    filtered_img = SpatialImage(filtered_img, voxelsize=img.voxelsize)
                    temp_filtered_images.append(filtered_img)
                    self.increment_progress()
            
            self.filtered_images[time] = MultiChannelImage(temp_filtered_images, channel_names=temp_channel_names)
