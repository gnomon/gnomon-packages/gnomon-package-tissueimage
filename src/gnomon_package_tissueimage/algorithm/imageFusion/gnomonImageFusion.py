import logging
from copy import deepcopy

import numpy as np
from gnomon_package_tissueimage.utils import vtk_img_to_sp_img
from dtkcore import d_inliststring
from dtkcore import d_int
from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import imageOutput
from gnomon.core import gnomonAbstractImageFusion
from timagetk.algorithms.pointmatching import pointmatching
from timagetk.tasks.fusion import iterative_fusion


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Image Fusion")
@imageOutput('fused_img_dict', data_plugin="gnomonImageDataMultiChannelImage")
class gnomonImageFusion(gnomonAbstractImageFusion):

    def __init__(self):
        super(gnomonImageFusion, self).__init__()
        self.image_series = []
        self.input_landmarks = []

        self.fused_img_dict = {}

        self._parameters = {}
        self._parameters['channel'] = d_inliststring("", [""], "Signal channel on which to perform the image fusion")
        self._parameters['nb_iterations'] = d_int("Number of iterations for the registration on the reference image", 0, 0, 5)
        self._parameters['n_job'] = d_int("Number of jobs", 1, 1, 5)

        self.__doc__ = iterative_fusion.__doc__

    def addImage(self, image_series):
        self.image_series.append(image_series)

        channels = None
        for image_series in self.image_series:
            if image_series is not None:
                image = list(image_series.values())[0]
                if channels is None:
                    channels = set(image.channels())
                else:
                    channels = channels.intersection(set(image.channels()))
            #print image_series, channels
        if (channels is None) or (len(channels) == 1):
            if 'channel' in self._parameters.keys():
                pass #    del self._parameters['channel']
        else:
            #print channels
            if 'channel' not in self._parameters.keys():
                self._parameters['channel'].setValues([""])
            current_channel = self['channel']
            self._parameters['channel'].setValues(list(channels))
            if current_channel not in channels:
                self._parameters['channel'].setValue(list(channels)[0])

    def imageSeriesToSpatialImageDicts(self, time):
        img_dicts = []
        for image_series in self.image_series:
            img_dict = {}
            if time in image_series.keys():
                image = image_series[time]
                for channel in image.channels():
                    img_dict[channel] = vtk_img_to_sp_img(image.image(channel))
            img_dicts.append(img_dict)
        return img_dicts

    def removeImages(self):
        self.image_series = []

    def addLandmarks(self, landmarks):
        self.input_landmarks.append(landmarks)

    def removeLandmarks(self):
        self.input_landmarks = []

    def run(self):
        self.set_max_progress(2*len(self.image_series[O]))
        self.fused_img_dict = {}

        if len(self.image_series) == 0:
            logging.error("--> Please add at least one image.")
            return

        for time in self.image_series[0].keys():
            self.set_progress_message(f"T {time} : converting image to SpatialImage")
            img_dicts = self.imageSeriesToSpatialImageDicts(time)
            if 'channel' in self._parameters.keys():
                imgs = [deepcopy(img_dict[self['channel']]) for img_dict in img_dicts]
            else:
                imgs = [deepcopy(list(img_dict.values())[0]) for img_dict in img_dicts]
            self.increment_progress()

            if len(self.input_landmarks) > 0 and np.array(self.input_landmarks).shape[1] != 0:
                if len(self.input_landmarks) == len(self.image_series):
                    self.set_progress_message(f"T {time} : applying transformation")
                    man_trsfs = []
                    for i  in range(1, len(self.input_landmarks)):
                        alignment_trsf = pointmatching(self.input_landmarks[i], self.input_landmarks[0])
                        man_trsfs.append(alignment_trsf)
                    self.set_progress_message(f"T {time} : fusing")
                    fused_img = iterative_fusion(imgs, n_iter=self['nb_iterations'], init_trsfs=man_trsfs, n_job=self['n_job'])

                else:
                    logging.error("--> You provided" + str(len(self.input_landmarks)) + "landmarks for " + str(len(imgs)) + " images")
            else:
                self.set_progress_message(f"T {time} : fusing")
                fused_img = iterative_fusion(imgs, n_iter=self['nb_iterations'], n_job=self['n_job'])
            self.increment_progress()

            self.fused_img_dict[time] = {"":fused_img}
