import logging
from copy import deepcopy
from time import time as current_time

import numpy as np
import scipy.ndimage as nd
from gnomon_package_tissueimage.utils import vtk_img_to_sp_img
from dtkcore import d_inliststring
from dtkcore import d_int
from dtkcore import d_real
from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import imageOutput
from gnomon.core import gnomonAbstractImageFusion
from timagetk.algorithms.averaging import images_averaging
from timagetk.algorithms.blockmatching import blockmatching
from timagetk.algorithms.resample import isometric_resampling
from timagetk.algorithms.trsf import apply_trsf
from timagetk.algorithms.trsf import create_trsf
from timagetk.algorithms.trsf import inv_trsf
from timagetk.algorithms.trsf import trsfs_averaging

logging.getLogger().setLevel(logging.INFO)


def average_distance(img1, img2):
    img21 = np.absolute(img2 / img2.mean() - img1 / img1.mean()).astype('uint8')
    img21 = nd.filters.median_filter(img21 + 10, size=2) - 10
    return img21.mean()


def principal_directions(img, threshold=10):
    '''
    Returns the first two principal eigen vectors of an input image
    Fixed threshold of 10 for a 8 bit image
    It supposes an isotropic sampling of the image.
    '''
    x, y, z = np.where(img > threshold)  ## appropriate thresholding
    x = x - np.mean(x)
    y = y - np.mean(y)
    z = z - np.mean(z)
    coords = np.vstack([x, y, z])
    cov = np.cov(coords)
    evals, evecs = np.linalg.eig(cov)
    sort_indices = np.argsort(evals)[::-1]
    return list(evecs[:, sort_indices][0:2])


def superposing_trsf(pv_flo, pv_ref, img):
    '''
    - pv_flo and pv-ref are lists of the first two principal vectors of the floating and the reference images respectively
    - img is the floating image
    '''
    # compute the rotation matrix R$
    pframe_ref = np.array([pv_ref[0], pv_ref[1], np.cross(pv_ref[0], pv_ref[1])])
    pframe_flo = np.array([pv_flo[0], pv_flo[1], np.cross(pv_flo[0], pv_flo[1])])
    F = np.transpose(pframe_ref)
    G = np.transpose(pframe_flo)
    R = np.matmul(G, np.linalg.inv(F))
    # blockmatching rotates around the origin,
    # so a rotation around the middle of the image contains an additional translation t
    s = img.shape
    res = img.voxelsize
    com = np.array(res) * np.array(s) / 2
    i = np.identity(3)
    t = np.dot((i - R), np.array(com))
    R = np.transpose(R)

    T = np.identity(4)
    T[0:3, 0:3] = R
    T = np.transpose(T)
    T[0:3, 3] = t
    trsf = create_trsf('identity')

    return trsf


def reconstruction_auto(input_imgs, registration_method='deformable_registration', averaging_method='robust_mean', max_image_distance=0.5, intensity_threshold=10, pyramid_highest=2, pyramid_lowest=2,
                        elastic_sigma=5, fluid_sigma=5, max_iterations=1):
    """

    :param input_imgs:
    :param pyramid_highest: pyramid-highest-level, default is 3: it corresponds to 32x32x32 for an original 256x256x256 image
    :param pyramid_lowest: pyramid-lowest-level, default is 0: original dimension
    :param elastic_sigma: sigma for elastic regularization of the transformation
    :param fluid_sigma: sigma for fluid regularization (field interpolation and regularization for pairings)
    :param max_image_distance:
    :param max_iterations:
    :return:
    """

    if registration_method.find("_registration") == -1:
        registration_method += "_registration"
    assert registration_method in ['rigid_registration', 'deformable_registration']

    assert averaging_method in ['mean', 'robust_mean', 'maximum']

    if pyramid_highest < pyramid_lowest:
        pyramid_highest = pyramid_lowest

    start = current_time()

    rigid_blockmatching_params = ""
    rigid_blockmatching_params += " -estimator wlts"
    rigid_blockmatching_params += " -floating-low-threshold 0"
    rigid_blockmatching_params += " -reference-low-threshold 0"
    rigid_blockmatching_params += " -pyramid-highest-level 5"
    rigid_blockmatching_params += " -pyramid-lowest-level " + str(pyramid_lowest)
    rigid_blockmatching_params += " -lts-fraction 0.55"

    ###################################################
    #   Average model computation ---- MAR (Multi Angle Reconstruction)
    ###################################################

    logging.info("---------------")
    logging.info("INITIALISATION (time : " + str((current_time() - start) / 60) + " min)")
    logging.info("---------------")

    # Resampling the reference image to isotropic resolution
    # ------------------------------------------------------
    # it will be the initial reference image for the average model
    logging.info("Resampling ref image to isotropic resolution")

    # img_ref = resample_isotropic(input_imgs[0], voxelsize=input_imgs[0].voxelsize[0])
    img_ref = isometric_resampling(input_imgs[0], method='min', option='gray')

    # Registration of the floating images on resampled ref
    # -----------------------------------------------------
    logging.info("Rigid registration of images on resampled ref")

    rigid_imgs = []
    rigid_trsfs = []

    # A. Find optimised rigid transformations
    for i in range(0, len(input_imgs)):

        start_function = current_time()

        img_flo = input_imgs[i]
        result_trsf, result_img = blockmatching(img_flo, img_ref, param_str_2=rigid_blockmatching_params)
        dist = average_distance(img_ref, result_img)

        if dist > max_image_distance:
            logging.info("Rigid transform failed, (d=" + str(dist) + ") check main object directions to initialize...")

            img_flo = input_imgs[i]
            img_flo_iso = isometric_resampling(input_imgs[i], voxelsize=img_ref.voxelsize[0])

            pv_ref = principal_directions(img_ref, threshold=intensity_threshold)
            logging.debug("pv_ref=" + str(pv_ref))

            pv_flo = principal_directions(img_flo_iso, threshold=intensity_threshold)
            logging.debug("pv_flo=" + str(pv_flo))

            logging.info("Computing the initial transformation matrix...")
            # resolve the indeterminacy in orientations : check a total of 4 possibilities (2 for each axis)
            list_pv = [pv_flo, pv_flo * np.array([[-1], [1]]), pv_flo * np.array([[1], [-1]]), pv_flo * np.array([[-1], [-1]])]

            direction_distances = []
            direction_transforms = []
            direction_images = []

            for i in range(0, 4):
                logging.info('-----------------')

                trsf_init = superposing_trsf(list_pv[i], pv_ref, img_flo)
                trsf_result, tfd_img = blockmatching(img_flo, img_ref, init_result_trsf=trsf_init, param_str_2=rigid_blockmatching_params)

                direction_transforms += [trsf_result]
                direction_images += [tfd_img]
                direction_distances.append(average_distance(img_ref, tfd_img))

            # choose the best orientation
            logging.info(str(direction_distances))
            dist = min(direction_distances)
            imin = direction_distances.index(dist)

            result_trsf = direction_transforms[imin]
            result_img = direction_images[imin]

            if dist > max_image_distance:
                result_trsf, result_img = blockmatching(img_flo, img_ref, init_result_trsf=result_trsf, param_str_2=rigid_blockmatching_params)
                dist = average_distance(img_ref, result_img)
                if dist > max_image_distance:
                    logging.info("WARNING!!! The registration is not proper, please check the input image")

        rigid_imgs += [result_img]
        rigid_trsfs += [result_trsf]

        logging.info("Registration quality distance = " + str(dist))
        logging.info("Auto-registration done.(time : " + str((current_time() - start_function) / 60) + " min)")

    # B. Compute the fusion of rigid registered images
    logging.info("Compute the fusion of rigid registered images")

    rigid_fused = images_averaging(rigid_imgs, method=averaging_method)
    # del rigid_fused.metadata['timagetk']

    if registration_method == 'rigid_registration':
        return rigid_fused

    elif registration_method == 'deformable_registration':

        deformable_blockmatching_params = " -estimator wlts"
        deformable_blockmatching_params += " -py-gf"
        deformable_blockmatching_params += " -floating-low-threshold 2"
        deformable_blockmatching_params += " -reference-low-threshold 2"
        deformable_blockmatching_params += " -pyramid-highest-level " + str(pyramid_highest)
        deformable_blockmatching_params += " -pyramid-lowest-level " + str(pyramid_lowest)
        deformable_blockmatching_params += " -elastic-sigma " + str(elastic_sigma)
        deformable_blockmatching_params += " -fluid-sigma " + str(fluid_sigma)

        it = 0

        while it <= max_iterations:
            logging.info("---------------")
            logging.info("Iteration " + str(it) + " (time : " + str((current_time() - start) / 60) + " min)")
            logging.info("---------------")
            logging.info("Registration of images on the ref")

            deformable_imgs = []
            deformable_trsfs = []

            for i in range(0, len(input_imgs)):
                img_flo = rigid_imgs[i]

                result_trsf, result_img = blockmatching(img_flo, img_ref, param_str_1=" -trsf-type vectorfield", param_str_2=deformable_blockmatching_params)

                deformable_imgs += [result_img]
                deformable_trsfs += [result_trsf]

            logging.info("Fusing images")
            deformable_fused = images_averaging(deformable_imgs, method=averaging_method)

            logging.info("Compute mean image")

            # Compute mean transformation
            mean_trsf = trsfs_averaging(deformable_trsfs)

            # Apply inverse mean transformation to the fused image
            mean_trsf_inv = inv_trsf(mean_trsf)
            mean_img = apply_trsf(deformable_fused, mean_trsf_inv, template_img=img_ref)

            img_ref = mean_img
            it += 1

        logging.info("---------------")
        logging.info("FINALIZATION time : " + str((current_time() - start) / 60) + " min)")
        logging.info("---------------")

        # del deformable_fused.metadata['timagetk']
        # del mean_img.metadata['timagetk']

        return mean_img


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Principal Direction Auto-Fusion")
@imageOutput('fused_img_dict', data_plugin="gnomonImageDataMultiChannelImage")
class principalDirectionsAutoFusion(gnomonAbstractImageFusion):
    """Short summary principalDirectionsAutoFusion.

    Test long descritpion.

    Parameters
    ----------


    Attributes
    ----------
    _in_imgs_series : type
        Description of attribute `_in_imgs_series`.
    input_landmarks : type
        Description of attribute `input_landmarks`.
    _out_imgs_serie : type
        Description of attribute `_out_imgs_serie`.
    _parameters : type
        Description of attribute `_parameters`.

    """
    def __init__(self):
        super(principalDirectionsAutoFusion, self).__init__()
        self.image_series = []
        self.input_landmarks = []

        self.fused_img_dict = {}

        self._parameters = {}
        self._parameters['channel'] = d_inliststring("Signal channel on which to perform the image fusion", "", [""])
        self._parameters['nb_iterations'] = d_int("Number of iterations for the deformable registration on the reference image", 1, 0, 10)
        self._parameters['max_image_distance'] = d_real("Relative image distance threshold above which to re-estimate rigid transform from image principal directions",
                                                                            0.45, 0, 1, 2)
        self._parameters['registration_method'] = d_inliststring("Type of registration", 'deformable',['rigid','deformable'])
        self._parameters['averaging_method'] = d_inliststring("Type of averaging", 'robust_mean',['mean','robust_mean','maximum'])
        self._parameters['intensity_threshold'] = d_int("Threshold on signal intensity for the determination of object principal directions", 10,0,255)
        self._parameters['pyramid_lowest'] = d_int("Maximal block dimension considered for the registration (0 means voxel dimension)", 2,0,5)


    def addImage(self, image_series):
        self.image_series.append(image_series)

        channels = None
        for image_series in self.image_series:
            if image_series is not None:
                image = image_series.current().asImage()
                if channels is None:
                    channels = set(image.channels())
                else:
                    channels = channels.intersection(set(image.channels()))
            #print image_series, channels
        if (channels is None) or (len(channels) == 1):
            if 'channel' in self._parameters.keys():
                pass #    del self._parameters['channel']
        else:
            #print channels
            if 'channel' not in self._parameters.keys():
                self._parameters['channel'].setValues([""])
            current_channel = self['channel']
            self._parameters['channel'].setValues(list(channels))
            if current_channel not in channels:
                self._parameters['channel'].setValue(list(channels)[0])

    def imageSeriesToSpatialImageDicts(self,time):
        img_dicts = []
        for image_series in self.image_series:
            img_dict = {}
            if time in image_series.times():
                image = image_series.at(time).asImage()
                for channel in image.channels():
                    img_dict[channel] = vtk_img_to_sp_img(image.image(channel))
            img_dicts.append(img_dict)
        return img_dicts

    def removeImages(self):
        self.image_series = []

    def addLandmarks(self, landmarks):
        self.input_landmarks.append(landmarks)

    def removeLandmarks(self):
        self.input_landmarks = []


    def run(self):
        self.set_max_progress(2*len(self.image_series[O].times()))
        self.fused_img_dict = {}

        if len(self.image_series) == 0:
            logging.error("--> Please add at least one image.")
            return

        for time in self.image_series[0].times():
            self.set_progress_message(f"T {time} : converting image to SpatialImage")
            img_dicts = self.imageSeriesToSpatialImageDicts(time)
            if 'channel' in self._parameters.keys():
                imgs = [deepcopy(img_dict[self['channel']]) for img_dict in img_dicts]
            else:
                imgs = [deepcopy(img_dict.values()[0]) for img_dict in img_dicts]
            self.increment_progress()

            self.set_progress_message(f"T {time} : auto fusion")
            fused_img = reconstruction_auto(imgs,
                                            registration_method=self['registration_method'],
                                            averaging_method=self['averaging_method'],
                                            max_image_distance=self['max_image_distance'],
                                            intensity_threshold=self['intensity_threshold'],
                                            pyramid_lowest=self['pyramid_lowest'],
                                            max_iterations=self['nb_iterations'])
            self.increment_progress()

            self.fused_img_dict[time] = {"":fused_img}
