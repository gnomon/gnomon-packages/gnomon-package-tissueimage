import logging
from copy import deepcopy

import numpy as np

from dtkcore import d_inliststring, d_int

import gnomon.core

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import imageInput
from gnomon.utils.decorators import imageOutput, dataDictOutput
from gnomon.core import gnomonAbstractImageRegistration

from timagetk import MultiChannelImage
from timagetk.algorithms.blockmatching import blockmatching
from timagetk.algorithms.trsf import apply_trsf


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Backward Registration")
@imageInput("_in_img", data_plugin="gnomonImageDataMultiChannelImage")
@dataDictOutput("_transform_dict", data_plugin="gnomonNumpyDataDictData")
@imageOutput("_out_img", data_plugin="gnomonImageDataMultiChannelImage")
class backwardRigidRegistration(gnomonAbstractImageRegistration):
    """Register an image time series on the first time point using rigid transforms.

    """

    def __init__(self):
        super().__init__()

        self._in_img = {}
        self._out_img = {}
        self._transform_dict = {}

        self._parameters = {}
        self._parameters['channel'] = d_inliststring("Channel", "", [""], "Signal channel on which to perform the temporal registration")
        self._parameters['pyramid_lowest'] = d_int("Pyramid lowest", 2, 0, 4, "Lowest level at which to compute deformation.")
        self._parameters['pyramid_highest'] = d_int("Pyramid highest", 5, 1, 5, "Highest level at which to compute deformation.")

    def refreshParameters(self):
        if len(self._in_img)>0:
            images = list(self._in_img.values())[0]
            if len(images) == 1:
                if 'channel'  in self._parameters.keys():
                    del self._parameters['channel']
            else:
                if 'channel' not in self._parameters.keys():
                    self._parameters['channel'] = d_inliststring("Channel", [""], [""], "Channels on which to compute the algorithm")
                    channel_name = images.channel_names[0]
                else:
                    channel_name = self['channel']
                self._parameters['channel'].setValues(images.channel_names)
                self._parameters['channel'].setValue(channel_name)

    def run(self):
        self._out_img = {}

        times = sorted(list(self._in_img.keys()))
        self.set_max_progress(len(times))

        self.set_progress_message(f"Preparing images for registration")
        if 'channel' in self._parameters.keys():
            images = {t: deepcopy(img.get_channel(self['channel'])) for t, img in self._in_img.items()}
        else:
            images = {t: deepcopy(img.get_channel(img.channel_names[0])) for t, img in self._in_img.items()}

        # first image which is the reference and is not transformed
        self._out_img[times[0]] = deepcopy(self._in_img[times[0]])
        self._transform_dict[times[0]] = {}
        self._transform_dict[times[0]]["transform"] = np.eye(4)

        # the first image is used as a reference
        transformed_images = {times[0]: images[times[0]]}
        self.increment_progress()

        for previous_time, next_time in zip(times[:-1], times[1:]):
            self.set_progress_message(f"Registering t={next_time} on (registered) t={previous_time}")
            reference_img = transformed_images[previous_time]
            floating_img = images[next_time]

            rigid_trsf = blockmatching(
                floating_image=floating_img,
                reference_image=reference_img,
                method='rigid',
                pyramid_lowest_level=self['pyramid_lowest'],
                pyramid_highest_level=self['pyramid_highest']
            )

            transformed_images[next_time] = apply_trsf(floating_img, rigid_trsf, template_img=reference_img)

            out_img_dict = {}
            for channel in self._in_img[previous_time].keys():
                out_img_dict[channel] = apply_trsf(
                    self._in_img[next_time].get_channel(channel),
                    rigid_trsf,
                    template_img=self._in_img[previous_time].get_channel(channel)
                )
            self._out_img[next_time] = MultiChannelImage(out_img_dict)

            self._transform_dict[next_time] = {}
            self._transform_dict[next_time]["transform"] = rigid_trsf.get_array().astype(float)
            self.increment_progress()
