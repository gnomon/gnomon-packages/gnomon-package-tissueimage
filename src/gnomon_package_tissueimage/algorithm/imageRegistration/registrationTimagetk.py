import logging
from copy import deepcopy
from pprint import pprint

import numpy as np

from dtkcore import d_inliststring, d_real, d_int, d_string

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import dataDictOutput, imageInput, dataDictInput
from gnomon.utils.decorators import imageOutput
from gnomon.core import gnomonAbstractImageRegistration

from timagetk.algorithms.blockmatching import blockmatching
from timagetk.algorithms.trsf import apply_trsf, Trsf
from timagetk.tasks.registration import consecutive_registration
from timagetk import MultiChannelImage, SpatialImage


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Registration")
@imageInput("_in_img", data_plugin="gnomonImageDataMultiChannelImage")
@dataDictInput("_init_transform_dict", data_plugin="gnomonNumpyDataDictData")
@dataDictOutput("_out_img_transform_dict", data_plugin="gnomonNumpyDataDictData")
@imageOutput("_out_img", data_plugin="gnomonImageDataMultiChannelImage")
class registrationTimagetk(gnomonAbstractImageRegistration):

    def __init__(self):
        super().__init__()

        self._in_img = {}
        self._init_transform_dict = {}
        self._out_img = {}
        self._out_img_transform_dict = {}

        self._parameters = {}
        self._parameters['channel'] = d_inliststring("Channel", "", [""], "Signal channel on which to perform the temporal registration")
        self._parameters['method'] = d_inliststring("Method",  "rigid", ["rigid", "affine", "vectorfield"], "Method for the image registration : rigid, affine or deformable (vectorfield)")
        self._parameters['estimator'] = d_inliststring("Estimator", 'wlts', ['wlts', 'lts', 'wls', 'ls'], "Transformation estimator")
        self._parameters['pyramid_lowest'] = d_int("Pyramid lowest", 2, 0, 4, "Lowest level at which to compute deformation.")
        self._parameters['pyramid_highest'] = d_int("Pyramid highest", 5, 1, 5, "Highest level at which to compute deformation.")
        self._parameters['elastic_sigma'] = d_real("Elastic sigma", 1, 0, 3, 1, "Transformation regularization, sigma for elastic regularization (only for vector field). "
                                                   "This is equivalent to a gaussian filtering of the final deformation field.")
        self._parameters['fluid_sigma'] = d_real("Fluid sigma", 1, 0, 3, 1, "Sigma for fluid regularization ie field interpolation and regularization for pairings (only for vector field). "
                                                   "This is equivalent to a gaussian filtering of the incremental deformation field.")
        # TODO : restore when d_string control is fixed
        # self._parameters['extra_args'] = d_string("Extra CLI args", "", "Additional CLI arguments to pass to the VT blockmatching command.")

        self._parameter_groups = {}
        for parameter_name in ["estimator", "pyramid_lowest", "pyramid_highest", "elastic_sigma", "fluid_sigma"]:
            self._parameter_groups[parameter_name] = 'advanced'

        self.__doc__ = consecutive_registration.__doc__

    def refreshParameters(self):
        if len(self._in_img)>0:
            images = list(self._in_img.values())[0]
            if len(images) == 1:
                if 'channel'  in self._parameters.keys():
                    del self._parameters['channel']
            else:
                if 'channel' not in self._parameters.keys():
                    self._parameters['channel'] = d_inliststring("Channel", [""], [""], "Channels on which to compute the algorithm")
                    channel_name = images.channel_names[0]
                else:
                    channel_name = self['channel']
                self._parameters['channel'].setValues(images.channel_names)
                self._parameters['channel'].setValue(channel_name)

    def run(self):
        self._out_img = {}

        imgs = self._in_img
        timestamps = list(imgs.keys())
        timestamps.sort()
        self.set_max_progress(3*(len(timestamps) - 1))

        logging.info("nb imgs in imgs: " + str(len(imgs)))
        if 'channel' in self._parameters.keys():
            registration_imgs = {t: deepcopy(img.get_channel(self['channel'])) for t, img in imgs.items()}
        else:
            registration_imgs = {t: deepcopy(img.get_channel(img.channel_names[0])) for t, img in imgs.items()}


        # the last image is used as a reference
        reference_imgs = registration_imgs  # just an alias

        for t_float, t_ref in zip(timestamps[:-1][::-1], timestamps[1:][::-1]):
            if t_float in self._init_transform_dict:
                self.set_progress_message(f"T {t_float} : applying initial transformation")
                logging.info("Applying initial transformation")
                dict_trsf = self._init_transform_dict[t_float]["transform"]
                if isinstance(dict_trsf, str):
                    dict_trsf = eval(dict_trsf)
                init_trsf = Trsf(dict_trsf)
            else:
                init_trsf = None
            self.increment_progress()

            self.set_progress_message(f"T {t_float} : blockmatching")
            transform = blockmatching(floating_image=registration_imgs[t_float],
                                      reference_image=reference_imgs[t_ref],
                                      method=self['method'],
                                      pyramid_lowest_level=self['pyramid_lowest'],
                                      pyramid_highest_level=self['pyramid_highest'],
                                      elastic_sigma=self['elastic_sigma'],
                                      fluid_sigma=self['fluid_sigma'],
                                      # params=self['extra_args'],
                                      estimator=self['estimator'],
                                      init_trsf=init_trsf)
            self.increment_progress()

            self.set_progress_message(f"T {t_float} : applying transformation")
            reference_imgs[t_float] = apply_trsf(registration_imgs[t_float], transform, template_img=reference_imgs[t_ref])

            logging.info("transform: " + str(transform))

            temp_output_images = {}
            for channel in imgs[t_ref].keys():
                temp_output_images[channel] = apply_trsf(imgs[t_float].get_channel(channel), transform, template_img=imgs[t_ref].get_channel(channel))

            self._out_img[t_float] = MultiChannelImage(temp_output_images)
            self._out_img_transform_dict[t_float] = {}
            self._out_img_transform_dict[t_float]["transform"] = transform.get_array().astype(float)
            self.increment_progress()

        # last image which is the reference and is not transformed
        self._out_img[timestamps[-1]] = imgs[timestamps[-1]]
        self._out_img_transform_dict[timestamps[-1]] = {}
        self._out_img_transform_dict[timestamps[-1]]["transform"] = np.eye(4)

        # trsfs = sequence_registration(imgs, method=self['method'])
        # for time, (trsf, img_dict) in enumerate(zip(trsfs+[None], img_dicts)):

        #     self._out_img[time] = {}

        #     for channel in img_dict.keys():
        #         if time<len(img_dicts)-1:
        #             transormed_img = apply_trsf(img_dict[channel],trsf,template_img=imgs[-1])
        #         else:
        #             transormed_img = img_dict[channel]

        #         self._out_img[time][channel] = transormed_img

        # self._out_img_transform_dict["transform"] = trsfs.mat.to_np_array().astype(float)

