import numpy as np
import pandas as pd

from dtkcore import d_inliststringlist

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import cellImageInput, imageInput, pointCloudOutput
from gnomon.core import gnomonAbstractPointCloudFromImage

from timagetk.algorithms.peak_detection import detect_nuclei


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Cell Centers")
@imageInput('img', data_plugin='gnomonImageDataMultiChannelImage')
@cellImageInput('seg_img', data_plugin='gnomonCellImageDataTissueImage')
@pointCloudOutput('df', data_plugin="gnomonPointCloudDataPandas")
class cellCentersTimagetk(gnomonAbstractPointCloudFromImage):
    """Retrieve the center of cells in a segmented image as a point cloud.

    The function returns a point cloud of the 3D barycenters of the cells in
    the segmented image. The properties associated with the cells can be
    transferred to the corresponding point in the point cloud.
    """

    def __init__(self):
        super().__init__()

        self.img = {}
        self.seg_img = {}
        self.df = {}

        self._parameters = {}
        self._parameters['cell_properties'] = d_inliststringlist("Cell properties", [""], [""], "List of the cell properties to transfer to the points")

        self._parameter_groups = {}

    def refreshParameters(self):
        if len(self.seg_img) > 0:
            seg_img = list(self.seg_img.values())[0]
            self._parameters['cell_properties'].setValues(seg_img.cells.feature_names())
            self._parameters['cell_properties'].setValue(seg_img.cells.feature_names())

    def run(self):
        self.set_max_progress(2*(len(self.seg_img)))
        self.df = {}

        for time in self.seg_img.keys():
            seg_img = self.seg_img[time]
            self.set_progress_message(f"T {time} : computing barycenters")
            cell_centers = seg_img.cells.barycenter()
            self.increment_progress()

            self.set_progress_message(f"T {time} : extracting cell centers")
            df = pd.DataFrame()
            df['label'] = [c for c in seg_img.cell_ids()]
            for i, dim in enumerate('xyz'):
                df['center_'+dim] = [cell_centers[c][i] for c in seg_img.cell_ids()]
            for property_name in self['cell_properties']:
                self.set_progress_message(f"T {time} : extracting {property_name}")
                property_dict = seg_img.cells.feature(property_name)
                df[property_name] = [property_dict[c] if c in property_dict else None for c in seg_img.cell_ids()]
            self.increment_progress()

            self.df[time] = df
