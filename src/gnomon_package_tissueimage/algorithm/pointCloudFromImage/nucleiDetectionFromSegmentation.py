import numpy as np
import pandas as pd

from dtkcore import d_inliststring, d_inliststringlist, d_range_real, array_real_2

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import imageInput, cellImageInput, pointCloudOutput
from gnomon.core import gnomonAbstractPointCloudFromImage

from timagetk.algorithms.peak_detection import detect_segmentation_nuclei


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Segmentation Nuclei Detection")
@imageInput('img', data_plugin='gnomonImageDataMultiChannelImage')
@cellImageInput('seg_img', data_plugin='gnomonCellImageDataTissueImage')
@pointCloudOutput('df', data_plugin="gnomonPointCloudDataPandas")
class nucleiDetectionFromSegmentation(gnomonAbstractPointCloudFromImage):
    """Compute a point cloud DataFrame with one nucleus per segmented cell

    The function runs a nuclei detection within each cell of a segmented image
    using the reference channel of an intensity image. The selected properties
    existing on the cells of the segmented image are transferred to the points
    representing the cell nuclei.
    """

    def __init__(self):
        super().__init__()

        self.img = {}
        self.seg_img = {}
        self.df = {}

        self._parameters = {}

        self._parameters['nuclei_channel'] = d_inliststring("Channel", "", [""], "Channel on which to extract the point_cloud")
        self._parameters['radius_range'] = d_range_real("Radius range", array_real_2([0.8, 1.4]), 0., 10., "Minimal and maximal radius of a nucleus")
        self._parameters['property_names'] = d_inliststringlist("Cell Properties", [""], [""], "List of cell properties to transfer to the nuclei points")

    def refreshParameters(self):
        if len(self.img)>0:
            img = list(self.img.values())[0]
            if len(img.channel_names) == 1:
                if 'nuclei_channel'  in self._parameters.keys():
                    del self._parameters['nuclei_channel']
            else:
                if 'nuclei_channel' not in self._parameters.keys():
                    self._parameters['nuclei_channel'] = d_inliststring("Channel", "", [""], "Channel on which to extract the point_cloud")
                nuclei_channel = self['nuclei_channel']
                self._parameters['nuclei_channel'].setValues(img.channel_names)

                if nuclei_channel in img.channel_names:
                    self._parameters['nuclei_channel'].setValue(nuclei_channel)
                else:
                    self._parameters['nuclei_channel'].setValue(img.channel_names[0])

        if len(self.seg_img)>0:
            seg_img = list(self.seg_img.values())[0]

            property_names = self['property_names']
            feature_names = [f for f in seg_img.cells.feature_names() if len(seg_img.cells.feature(f)) > 0]
            if len(feature_names) == 0:
                feature_names = [""]
            property_names = [p for p in property_names if p in feature_names]
            if len(property_names) == 0:
                property_names = feature_names
            self._parameters['property_names'].setValues(feature_names)
            self._parameters['property_names'].setValue(property_names)

    def run(self):
        self.set_max_progress(2*(len(self.img)))
        self.df = {}

        for time in self.img.keys():
            img = self.img[time]
            seg_img = self.seg_img[time]

            nuclei_channel = self['nuclei_channel'] if 'nuclei_channel' in self._parameters.keys() else ""
            nuclei_img = img.get_channel(nuclei_channel)

            self.set_progress_message(f"T {time} : detecting segmentation nuclei")
            nuclei_points = detect_segmentation_nuclei(nuclei_img,
                                                       seg_img,
                                                       radius_range=self['radius_range'],
                                                       step=0.2)
            self.increment_progress()

            self.set_progress_message(f"T {time} : extracting cell centers")
            df = pd.DataFrame()
            df['label'] = [l for l in nuclei_points]
            for i, dim in enumerate('xyz'):
                df['center_'+dim] = [nuclei_points[l][i] for l in nuclei_points]

            for property_name in self['property_names']:
                self.set_progress_message(f"T {time} : extracting {property_name}")
                cell_property = seg_img.cells.feature(property_name)
                df[property_name] = [cell_property[l] if l in cell_property else np.nan for l in nuclei_points]
            self.increment_progress()

            self.df[time] = df
