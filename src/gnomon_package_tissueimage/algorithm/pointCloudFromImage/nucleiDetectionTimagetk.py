import numpy as np
import pandas as pd

from dtkcore import d_int, d_real, d_range_real, d_inliststring
from dtkcore import array_real_2

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import imageInput
from gnomon.utils.decorators import pointCloudOutput
from gnomon.core import gnomonAbstractPointCloudFromImage

from timagetk.algorithms.peak_detection import detect_nuclei


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Nuclei Detection")
@imageInput('img', data_plugin='gnomonImageDataMultiChannelImage')
@pointCloudOutput('df', data_plugin="gnomonPointCloudDataPandas")
class nucleiDetectionTimagetk(gnomonAbstractPointCloudFromImage):
    """Compute a point cloud DataFrame with image nuclei.

    The function runs a nuclei detection on the reference channel of
    the image.
    """

    def __init__(self):
        super().__init__()

        self.img = {}
        self.df = {}

        self._parameters = {}

        self._parameters['nuclei_channel'] = d_inliststring("Channel", "", [""], "Channel on which to extract the point_cloud")
        self._parameters['threshold'] = d_int("Threshold", 1000, 0, 65535, "Intensity threshold")
        self._parameters['step'] = d_real("Radius step", 0.2, 0, 1, 1, "Step between radius scales")
        self._parameters['radius_range'] = d_range_real("Radius range", array_real_2([0.8, 1.4]), 0., 10., "Minimal and maximal radius of a nucleus")
        self._parameters['patch_size'] = d_int("Patch size", 200, 64, 512, "Size of the image patches for detection")

    def refreshParameters(self):
        if len(self.img)>0:
            img = list(self.img.values())[0]
            if len(img.channel_names) == 1:
                if 'nuclei_channel'  in self._parameters.keys():
                    del self._parameters['nuclei_channel']
            else:
                if 'nuclei_channel' not in self._parameters.keys():
                    self._parameters['nuclei_channel'] = d_inliststring("Channel", "", [""], "Channel on which to extract the point_cloud")
                nuclei_channel = self['nuclei_channel']
                self._parameters['nuclei_channel'].setValues(img.channel_names)

                if nuclei_channel in img.channel_names:
                    self._parameters['nuclei_channel'].setValue(nuclei_channel)
                else:
                    self._parameters['nuclei_channel'].setValue(img.channel_names[0])

            img = img.get_channel(img.channel_names[0])

            if img.dtype == np.uint8:
                self._parameters['threshold'].setMax(255)
                self._parameters['threshold'].setValue(20)
            elif img.dtype == np.uint16:
                self._parameters['threshold'].setMax(65535)
                self._parameters['threshold'].setValue(1000)
            else:
                self._parameters['threshold'].setMax(img.max())
                self._parameters['threshold'].setValue(int(img.max() / 10.))

    def run(self):
        self.set_max_progress(1*(len(self.img)))
        self.df = {}

        for time in self.img.keys():

            img = self.img[time]

            nuclei_channel = self['nuclei_channel'] if 'nuclei_channel' in self._parameters.keys() else ""
            nuclei_img = img.get_channel(nuclei_channel)

            n_sigmas = int(np.ceil(np.log(self['radius_range'][1] / self['radius_range'][0]) / self['step']))

            self.set_progress_message(f"T {time} : detecting nuclei over {n_sigmas} scales")
            nuclei_points, nuclei_scales = detect_nuclei(
                nuclei_img,
                threshold=self['threshold'],
                patch_size=(self['patch_size'],)*3,
                radius_range=self['radius_range'],
                step=self['step'],
                return_scales=True
            )
            self.increment_progress()

            df = pd.DataFrame()
            for i, dim in enumerate('xyz'):
                df['center_'+dim] = nuclei_points[:, i]
            df['label'] = [2 + l for l in range(len(nuclei_points))]
            df['scale'] = nuclei_scales

            self.df[time] = df
