import logging

import numpy as np
import pandas as pd
import scipy.ndimage as nd

from dtkcore import d_inliststring
from dtkcore import d_int
from dtkcore import d_real

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import imageInput
from gnomon.utils.decorators import pointCloudOutput
from gnomon.core import gnomonAbstractPointCloudFromImage

from timagetk import SpatialImage, TissueImage3D
from timagetk.tasks.segmentation import seed_detection


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Seed Detection")
@imageInput('img', data_plugin='gnomonImageDataMultiChannelImage')
@pointCloudOutput('df', data_plugin="gnomonPointCloudDataPandas")
class seedDetectionTimagetk(gnomonAbstractPointCloudFromImage):
    """Perform a seed detection of a membrane-marked tissue 3D image.

    The method detects cell seeds for a watershed segmentation. Seeds are detected
    for each cell in the tissue using a local intensity minimum detector called
    H-Transform. The original image is first smoothed by a gaussian filter to
    make the seed detection more robust. A low value of the h_min parameter will
    tend to detect more seeds, while a higher value will make the detection more
    stringent.

    """

    def __init__(self):
        super().__init__()

        self.img = {}
        self.df = {}

        self._parameters = {}

        self._parameters['membrane_channel'] = d_inliststring("Channel",  "", [""], "Membrane or cell wall marker channel on which to perform the seed detection")
        self._parameters['h_min'] = d_int("H-min", 2, 0, 255, "High threshold of the H-transform for the detection of seeds")
        self._parameters['gaussian_sigma'] = d_real("Sigma", 0.66, 0., 50., 2, "Standard deviation of the Gaussian kernel used for the detection of seeds")

        self._parameter_groups = {}
        
    def refreshParameters(self):
        if len(self.img)>0:
            img = list(self.img.values())[0]
            if len(img.channel_names) == 1:
                del self._parameters['membrane_channel']
            else:
                if 'membrane_channel' not in self._parameters.keys():
                    self._parameters['membrane_channel'] = d_inliststring("Channel",  "", [""], "Membrane or cell wall marker channel on which to perform the seed detection")
                membrane_channel = self['membrane_channel']
                self._parameters['membrane_channel'].setValues(img.channel_names)

                if membrane_channel in img.channel_names:
                    self._parameters['membrane_channel'].setValue(membrane_channel)
                else:
                    self._parameters['membrane_channel'].setValue(img.channel_names[0])

            img = img.get_channel(img.channel_names[0])

            if img.dtype == np.uint8:
                self._parameters['h_min'].setMax(255)
                self._parameters['h_min'].setValue(2)
            elif img.dtype == np.uint16:
                self._parameters['h_min'].setMax(65535)
                self._parameters['h_min'].setValue(200)

    def run(self):
        self.set_max_progress(3*(len(self.img)))
        self.df = {}

        for time in self.img.keys():
            if 'membrane_channel' in self._parameters.keys():
                img = self.img[time].get_channel(self['membrane_channel'])
            else:
                img = self.img[time].get_channel(self.img[time].channel_names[0])

            voxelsize = np.array(img.voxelsize)

            self.set_progress_message(f"T {time} : applying filter")
            smooth_img = nd.gaussian_filter(img, sigma=self['gaussian_sigma']/voxelsize).astype(img.dtype)
            smooth_img = SpatialImage(smooth_img, voxelsize=voxelsize)
            self.increment_progress()

            self.set_progress_message(f"T {time} : detecting seeds")
            seed_img, _ = seed_detection(smooth_img, h_min=self['h_min'])
            seed_img = TissueImage3D(seed_img, not_a_label=0, background=1)
            self.increment_progress()

            self.set_progress_message(f"T {time} : computing seed positions")
            seed_positions = seed_img.cells.barycenter()
            self.increment_progress()

            df = pd.DataFrame({'label': seed_img.cell_ids()})
            for i, dim in enumerate('xyz'):
                df['center_'+dim] = [seed_positions[l][i] for l in df['label'].values]

            self.df[time] = df
