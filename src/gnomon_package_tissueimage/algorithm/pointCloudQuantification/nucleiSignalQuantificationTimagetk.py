from copy import deepcopy

import numpy as np
import pandas as pd

from dtkcore import d_inliststringlist
from dtkcore import d_real

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import dataFrameOutput
from gnomon.utils.decorators import imageInput
from gnomon.utils.decorators import pointCloudInput
from gnomon.utils.decorators import pointCloudOutput

from gnomon.core import gnomonAbstractPointCloudQuantification

from timagetk.algorithms.signal_quantification import quantify_nuclei_signal_intensity


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Nuclei Signal Quantification")
@imageInput('img', data_plugin='gnomonImageDataMultiChannelImage')
@pointCloudInput("df", data_plugin="gnomonPointCloudDataPandas")
@pointCloudOutput('out_df', data_plugin="gnomonPointCloudDataPandas")
@dataFrameOutput('data_df', data_plugin="gnomonDataFrameDataPandas")
class nucleiSignalQuantificationTimagetk(gnomonAbstractPointCloudQuantification):
    """Quantify image signal intensities on a 3D nuclei point cloud.

    The function computes for each nucleus in the point cloud and for each
    selected channel in the image a value of signal corresponding to the local
    image intensity. The algorithm uses a Gaussian filter to average signal
    around the nuclei points, for which the standard deviation is controlled
    by the gaussian_sigma parameter.
    """

    def __init__(self):
        super().__init__()

        self.img = {}
        self.df = {}
        self.out_df = {}
        self.data_df = {}

        self._parameters = {}
        self._parameters['signal_channels'] = d_inliststringlist("Channels", [""], [""], "Channels on which to compute the signal intensity")
        self._parameters['gaussian_sigma'] = d_real("Sigma", 0.5, 0., 50., 2, "Standard deviation of the Gaussian kernel used to smooth signal")

    def refreshParameters(self):
        if len(self.img)>0:
            img = list(self.img.values())[0]
            if len(img.channel_names) == 1:
                if 'signal_channels' in self._parameters.keys():
                    del self._parameters['signal_channels']
            else:
                if 'signal_channels' not in self._parameters.keys():
                    self._parameters['signal_channels'] = d_inliststringlist("Channels on which to compute the signal intensity", [""], [""])
                self._parameters['signal_channels'].setValues(img.channel_names)
                self._parameters['signal_channels'].setValue(img.channel_names)

    def run(self):
        n_signals = 1 if ('signal_channels' not in self._parameters) else len(self['signal_channels'])

        self.set_max_progress(len(self.df)*n_signals)
        self.data_df = {}
        self.out_df = {}

        for time in self.df.keys():
            df = self.df[time]
            out_df = deepcopy(df)

            nuclei_points = df[['center_'+dim for dim in 'xyz']].values

            assert (time in self.img) and (self.img[time] is not None)

            img = self.img[time]

            signal_names = img.channel_names if 'signal_channels' not in self._parameters else self['signal_channels']
            for signal_name in signal_names:
                self.set_progress_message(f"T {time} : quantifying {signal_name}")
                signal_img = img[signal_name]
                column_name = signal_name if signal_name != "" else "signal"
                out_df[column_name] = quantify_nuclei_signal_intensity(signal_img, nuclei_points, nuclei_sigma=self['gaussian_sigma'])
                self.increment_progress()

            self.out_df[time] = out_df
            
            self.data_df[time] = deepcopy(out_df)
