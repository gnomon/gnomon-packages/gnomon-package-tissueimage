from dtkcore import d_int
from dtkcore import d_real

import gnomon.core
from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import imageOutput

from timagetk import MultiChannelImage
from timagetk.synthetic_data.nuclei_image import example_nuclei_signal_images


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Nuclei Image Constructor")
@imageOutput(attr='img', data_plugin='gnomonImageDataMultiChannelImage')
class nucleiImageConstructor(gnomon.core.gnomonAbstractImageConstructor):
    """Create a nuclei image with a random signal channel.

    """

    def __init__(self):
        super().__init__()

        self._parameters = {}
        self._parameters['n_points'] = d_int(10,10,500,"The number of nuclei in the image")
        self._parameters['extent'] = d_int("The extent of the image in all dimensions", 15,10,500)
        self._parameters['nuclei_radius'] = d_real("The radius of nuclei in the image", 1.5,0,10,2)
        self._parameters['signal_intensity'] = d_int("The maximal level of intensity in the image", 50000,0,65535)

        self.img = {}

    def run(self):
        self.img = {}

        nuclei_img, signal_img = example_nuclei_signal_images(n_points=self['n_points'],
                                                              extent=self['extent'],
                                                              nuclei_radius=self['nuclei_radius'],
                                                              signal_intensity=self['signal_intensity'])

        img = MultiChannelImage({'nuclei':nuclei_img, 'signal':signal_img})

        self.img[0] = img
