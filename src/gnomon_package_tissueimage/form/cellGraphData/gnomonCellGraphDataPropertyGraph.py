from copy import deepcopy

from gnomon.utils import algorithmPlugin
from gnomon.core import gnomonAbstractCellGraphData


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="PropertyGraph")
class gnomonCellGraphDataPropertyGraph(gnomonAbstractCellGraphData):

    def __init__(self, cellgraph=None):
        super(gnomonCellGraphDataPropertyGraph, self).__init__()
        if cellgraph is None:
            self._graph = PropertyGraph()
        else:
            self._graph = deepcopy(cellgraph._graph)

    def __del__(self):
        #del self._graph
        self._graph = None

    def clone(self):
        return gnomonCellGraphDataPropertyGraph(self)

# Metadata

    def metadata(self):
        metadata = {}
        metadata['Number of vertices'] = str(self._graph.nb_vertices())
        metadata['Number of edges'] = str(self._graph.nb_edges())
        for property_name in self._graph.vertex_property_names():
            metadata['Vertex property '+property_name] = str(np.array(self._graph.vertex_property(property_name).values()).dtype)
        for property_name in self._graph.edge_property_names():
            metadata['Edge property '+property_name] = str(np.array(self._graph.edge_property(property_name).values()).dtype)

        return metadata

    def dataName(self):
        return "cellcomplex.PropertyGraph"

    def isValid(self):
        return self._graph.is_valid()

# Graph concept

    def edgeVertexIds(self, edgeId):
        return list(self._graph.edge_vertices(eid=edgeId))

    def verticesEdgeId(self, vertexIds):
        print(vertexIds)
        return self._graph.edge(source=vertexIds[0],target=vertexIds[1])

    def hasVertex(self, vertexId):
        return self.graph.has_vertex(vid=vertexId)

    def hasEdge(self, edgeId):
        return self.graph.has_labe(eid=edgeId)

# Vertex list concept

    def vertexIds(self):
        return list(self._graph.vertices())
        # return np.array(list(self._graph.vertices())).astype(long)
        # return [long(vid) for vid in self._graph.vertices()]

    def vertexCount(self):
        return self._graph.nb_vertices()

    def adjacentVertexIds(self, vertexId):
        return list(self._graph.neighbors(vid=vertexId))

    def adjacentVertexCount(self, vertexId):
        return self._graph.nb_neighbors(vid=vertexId)

# Edge list concept

    def edgeIds(self):
        return list(self._graph.edges())

    def edgeCount(self):
        return self._graph.nb_edges()

    def incidentEdgeIds(self, vertexId):
        return list(self._graph.in_edges(vid=vertexId))

    def incidentCount(self, vertexId):
        return self._graph.nb_edges(vid=vertexId)

# Mutation concept

    def addVertex(self, vertexId=None):
        return self._graph.add_vertex(vid=vertexId)

    def removeVertex(self, vertexId):
        return self._graph.remove_vertex(vid=vertexId)

    def addEdge(self, vertexIds, edgeId=None):
        return self._graph.add_edge(sid=vertexIds[0],tid=vertexIds[1],eid=edgeId)

    def removeEdge(self, edgeId=None):
        return self._graph.remove_edge(eid=edgeId)

# Property concept

    def vertexPropertyNames(self):
        return list(self._graph.vertex_property_names())

    def hasVertexProperty(self, propertyName):
        property_name=propertyName
        return property_name in self._graph.vertex_property_names()

    def vertexProperty(self, propertyName):
        property_dict = self._graph.vertex_property(property_name=propertyName)
        return property_dict
        # return dict(zip(property_dict.keys(),[QVariant(property_dict[k]) for k in property_dict.keys()]))
        # return dict(zip([long(k) for k in property_dict.keys()],[property_dict[k] for k in property_dict.keys()]))

    def addVertexProperty(self, propertyName):
        self._graph.add_vertex_property(property_name=propertyName)
        property_dict = self._graph.vertex_property(property_name=propertyName)
        return property_dict
        # return dict(zip([long(k) for k in property_dict.keys()],[property_dict[k] for k in property_dict.keys()]))

    def removeVertexProperty(self, propertyName):
        return self._graph.remove_vertex_property(property_name=propertyName)

    def edgePropertyNames(self):
        return list(self._graph.edge_property_names())

    def hasEdgeProperty(self, propertyName):
        property_name=propertyName
        return property_name in self._graph.edge_property_names()

    def edgeProperty(self, propertyName):
        property_dict = self._graph.edge_property(property_name=propertyName)
        return property_dict
        # return dict(zip(property_dict.keys(),[QVariant(property_dict[k]) for k in property_dict.keys()]))
        # return dict(zip([long(k) for k in property_dict.keys()],[property_dict[k] for k in property_dict.keys()]))

    def addEdgeProperty(self, propertyName):
        self._graph.add_edge_property(property_name=propertyName)
        property_dict = self._graph.edge_property(property_name=propertyName)
        return property_dict
        # return dict(zip(property_dict.keys(),[QVariant(property_dict[k]) for k in property_dict.keys()]))
        # return dict(zip([long(k) for k in property_dict.keys()],[property_dict[k] for k in property_dict.keys()]))

    def removeEdgeProperty(self, propertyName):
        return self._graph.remove_edge_property(property_name=propertyName)

    def graphPropertyNames(self):
        return list(self._graph.graph_property_names())

    def hasGraphProperty(self, propertyName):
        property_name=propertyName
        return property_name in self._graph.graph_property_names()

    def graphProperty(self, propertyName):
        property_name=propertyName
        return self._graph.graph_properties()[property_name]

    def addGraphProperty(self, propertyName):
        self._graph.add_graph_property(property_name=propertyName)
        property_name=propertyName
        return self._graph.graph_properties()[property_name]

    def removeGraphProperty(self, propertyName):
        return self._graph.remove_graph_property(property_name=propertyName)

    def set_property_graph(self, prop_graph):
        self._graph = deepcopy(prop_graph)
        return self

    def from_property_graph(self, graph):
        cellgraph = gnomonCellGraphDataPropertyGraph()
        cellgraph._graph = deepcopy(graph)
        return cellgraph
