import logging
import pickle
from base64 import b64decode, b64encode
from copy import deepcopy
from typing import Optional, List, Tuple

import numpy as np
import vtk

import gnomon.core
from gnomon.core import gnomonAbstractCellImageData, gnomonCellImage
from gnomon.utils import formDataPlugin

from gnomon_package_tissueimage.utils import sp_img_to_vtk_img, vtk_img_to_sp_img

from timagetk import TissueImage3D

cell_feature_names = ['barycenter', 'inertia_axis', 'principal_direction_norms', 'shape_anisotropy', 'area', 'volume']
wall_feature_names = ['inertia_axis', 'area']

@formDataPlugin(version="1.0.0", coreversion="1.0.0", data_setter="set_tissue_image", data_getter='get_tissue_image', name="Cell Image TissueImage3D")
class gnomonCellImageDataTissueImage(gnomonAbstractCellImageData):
    _tissue: Optional[TissueImage3D]

    def __init__(self, cellimagedata=None):
        super().__init__()
        self._tissue = None
        self._vtk_image = None
        if cellimagedata is not None:
            self.set_tissue_image(cellimagedata._tissue)

    def __del__(self):
        self._tissue = None
        self._vtk_image = None

    def set_tissue_image(self, tissue: TissueImage3D):
        self._vtk_image = None
        if tissue is None:
            self._tissue = None
        else:
            self._tissue = TissueImage3D(tissue, not_a_label=0, background=1)
            self._tissue.cells = deepcopy(tissue.cells)
            self._tissue.cells.image = self._tissue
            self._tissue.walls = deepcopy(tissue.walls)
            self._tissue.walls.image = self._tissue

    def get_tissue_image(self) -> TissueImage3D:
        return self._tissue

    def clone(self):
        _clone = gnomonCellImageDataTissueImage(self)
        _clone.__disown__()
        return _clone

    def fromGnomonForm(self, cellimage: gnomonCellImage) -> None:
        img = vtk_img_to_sp_img(cellimage.image())
        tissue = TissueImage3D(img, not_a_label=0, background=1)

        for property_name in cellimage.cellPropertyNames():
            cell_property = cellimage.cellProperty(property_name)
            property_dict = {l:cell_property[l] if l in cell_property else np.nan for l in tissue.cell_ids()}
            tissue.cells.set_feature(property_name, property_dict)

        for property_name in cellimage.wallPropertyNames():
            wall_property = cellimage.walllProperty(property_name)
            property_dict = {w: wall_property[i] if i in wall_property else np.nan for i, w in enumerate(tissue.wall_ids())}
            tissue.walls.set_feature(property_name, property_dict)

        self.set_tissue_image(tissue)
        self._vtk_image = cellimage.image()

    def metadata(self) -> dict:
        metadata = {}
        metadata['Number of cells'] = str(self._tissue.nb_cells())
        for property_name in cell_feature_names:
            if self.hasCellProperty(property_name):
                metadata['Cell property ' + property_name] = str(np.array(list(self.cellProperty(property_name).values())).dtype)
        for property_name in wall_feature_names:
            if self.hasWallProperty(property_name):
                metadata['Wall property ' + property_name] = str(np.array(list(self.wallProperty(property_name).values())).dtype)
        metadata["Dimensions"] = str(tuple(self._tissue.shape))
        metadata["Voxel Size"] = "[{:.3e}, {:.3e}, {:.3e}]".format(*self._tissue.voxelsize)


        return metadata

    def dataName(self) -> str:
        return "timagetk.TissueImage"

    def setImage(self, image: vtk.vtkImageData):
        self._vtk_image = image
        img: SpatialImage = vtk_img_to_sp_img(image)
        self._tissue = TissueImage3D(img, not_a_label=0, background=1)

    def image(self) -> vtk.vtkImageData:
        if not self._vtk_image:
            positions = self._tissue.cells.barycenter()
            for k, dim in enumerate(['x','y','z']):
                self._tissue.cells.set_feature('barycenter_'+dim, {l:p[k] for l,p in positions.items()})
            self._vtk_image = sp_img_to_vtk_img(self._tissue)
        return self._vtk_image

    def cellIds(self) -> List[int]:
        return list(self._tissue.cell_ids())

    def cellCount(self) -> int:
        return self._tissue.nb_cells()

    def adjacentCellIds(self, cellId: int) -> List[int]:
        return list(self._tissue.cells.neighbors(cellId)[cellId])

    def adjacentCellCount(self, cellId: int) -> int:
        return int(self._tissue.cells.number_of_neighbors(cellId)[cellId])

    def cellPropertyNames(self) -> List[str]:
        return [p for p in self._tissue.cells.feature_names() if self._tissue.cells.feature(p)!={}]

    def hasCellProperty(self, propertyName: str) -> bool:
        return (propertyName in self._tissue.cells.feature_names()) and (self._tissue.cells.feature(propertyName) != {})

    def cellProperty(self, propertyName: str) -> dict:
        if propertyName in self._tissue.cells.feature_names():
            cell_property = self._tissue.cells.feature(propertyName)
            property_values = np.array(list(cell_property.values()))
            if property_values.ndim==1 and np.issubdtype(property_values.dtype, np.number):
                property_dict = dict([(k, float(v)) for k, v in cell_property.items()])
            else:
                property_dict = {}
        else:
            property_dict = {}
        return property_dict

    def addCellProperty(self, propertyName: str):
        if propertyName not in self._tissue.cells.feature_names():
            self._tissue.cells.set_feature(propertyName, {})

    def updateCellProperty(self, propertyName: str, values: dict, eraseProperty: bool = True):
        property_dict = {l:values[l] if l in values else np.nan for l in self._tissue.cell_ids()}
        self._tissue.cells.set_feature(propertyName, property_dict, update=not eraseProperty)

    def removeCellProperty(self, propertyName: str):
        if propertyName in self._tissue.cells.feature_names:
            self._tissue.cells.set_feature(propertyName, {}, update=False)

    def computeCellProperty(self, propertyName: str) -> dict:
        if propertyName in cell_feature_names:
            getattr(self._tissue.cells, propertyName)()
            return self._tissue.cells.feature(propertyName)
        else:
            logging.warning(f"Unable to compute property {propertyName} on image")
            return {}

    def wallIds(self) -> List[Tuple[int, int]]:
        return list(range(len(self._tissue.wall_ids())))

    def wallCellIds(self, wallId: int) -> List[Tuple[int, int]]:
        return list(self._tissue.wall_ids())[wallId]

    def wallPropertyNames(self) -> List[str]:
        return [p for p in self._tissue.walls.feature_names() if self._tissue.walls.feature(p)!={}]

    def hasWallProperty(self, propertyName: str) -> bool:
        return (propertyName in self._tissue.walls.feature_names()) and (self._tissue.walls.feature(propertyName) != {})

    def wallProperty(self, propertyName: str) -> dict:
        if propertyName in self._tissue.walls.feature_names():
            wall_property = self._tissue.walls.feature(propertyName)
            property_values = np.array(list(wall_property.values()))
            if property_values.ndim==1 and np.issubdtype(property_values.dtype, np.number):
                property_dict = dict([(i, float(wall_property[w])) for i, w in enumerate(self._tissue.wall_ids()) if w in wall_property])
            else:
                property_dict = {}
        else:
            property_dict = {}
        return property_dict

    def addWallProperty(self, propertyName: str):
        if propertyName not in self._tissue.walls.feature_names():
            self._tissue.walls.set_feature(propertyName, {})

    def updateWallProperty(self, propertyName: str, values: dict, eraseProperty: bool = True):
        property_dict = {w: values[i] if i in values else np.nan for i, w in enumerate(self._tissue.wall_ids())}
        self._tissue.walls.set_feature(propertyName, property_dict, update=not eraseProperty)

    def removeWallProperty(self, propertyName: str):
        if propertyName in self._tissue.walls.feature_names:
            self._tissue.walls.set_feature(propertyName, {}, update=False)

    def serialize(self):
        return b64encode(pickle.dumps(self._tissue)).decode("ascii")

    def deserialize(self, serialization: str):
        self._tissue = pickle.loads(b64decode(serialization.encode("ascii")))
