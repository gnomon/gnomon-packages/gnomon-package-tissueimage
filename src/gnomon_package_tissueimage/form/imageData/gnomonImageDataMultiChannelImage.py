import pickle
from base64 import b64decode
from base64 import b64encode
from copy import deepcopy
from typing import List, Dict

import vtk
import numpy as np
from gnomon_package_tissueimage.utils import sp_img_to_vtk_img, vtk_img_to_sp_img
from gnomon.utils import formDataPlugin
from gnomon.core import gnomonAbstractImageData, gnomonImage

from timagetk.components.multi_channel import MultiChannelImage


@formDataPlugin(version="1.0.0", coreversion="1.0.0", data_setter='set_image', data_getter='get_image', name="MultiChannelImage")
class gnomonImageDataMultiChannelImage(gnomonAbstractImageData):
    _image: MultiChannelImage

    def __init__(self, image=None):
        super().__init__()
        self._image = None
        self._vtk_image = {}
        if image is not None:
            self.set_image(image._image)

    def __del__(self):
        self._image = None
        self._vtk_image = {}

    def set_image(self, img: MultiChannelImage):
        self._vtk_image.clear()
        if isinstance(img, MultiChannelImage):
            self._image = img
        elif img is None:
            self._image = None
            self._vtk_image = {}
        else:
            self._image = MultiChannelImage(img)

    def get_image(self) -> MultiChannelImage:
        return self._image

    def clone(self):
        _clone = gnomonImageDataMultiChannelImage(self)
        _clone.__disown__()
        return _clone

    def fromGnomonForm(self, form: gnomonImage) -> None:
        channels = {}
        vtk_images = {}
        for channel_name in form.channels():
            vtk_images[channel_name] = form.image(channel_name)
            channels[channel_name] = vtk_img_to_sp_img(form.image(channel_name))
        img = MultiChannelImage(channels)
        self.set_image(img)
        self._vtk_image = vtk_images

    # Metadata
    def metadata(self) -> Dict[str, str]:
        metadata = {
            "Number of channels": str(self._image.n_ch),
            "Channels": str(self._image.channel_names),
            "Dimensions": str(self._image.ndim),
            "Size": str(self._image.shape),
            "Voxel Type": str(self._image.dtype),
            "Voxel Size": "[{:.3e}, {:.3e}, {:.3e}]".format(*self._image.voxelsize),
        }
        return metadata

    def dataName(self) -> str:
        return "timagetk.MultiChannelImage"

    def setImage(self, image: vtk.vtkImageData, channel: str):
        img: SpatialImage = vtk_img_to_sp_img(image)
        self._image[channel] = img
        self._vtk_image[channel] = image


    def image(self, channel: str) -> vtk.vtkImageData:
        if channel in self._image.channel_names:
            print("getting channel ", channel)
            if channel not in self._vtk_image:
                self._vtk_image[channel] = sp_img_to_vtk_img(self._image.get_channel(channel))
            return self._vtk_image[channel]
        else:
            return None

    def setChannelName(self, channel: str, name: str):
        self._image.set_channel_name(channel, name)

    def channels(self) -> List[str]:
        return self._image.channel_names

    def minValue(self , channel: str) -> int:
        if channel in self._image.channel_names:
            return int(np.min(self._image.get_channel(channel)))
        else:
            return 0

    def maxValue(self , channel: str) -> int:
        if channel in self._image.channel_names:
            return int(np.max(self._image.get_channel(channel)))
        else:
            return 1

    def serialize(self) -> str:
        return b64encode(pickle.dumps(self._image)).decode("ascii")

    def deserialize(self, serialization: str):
        self._image = pickle.loads(b64decode(serialization.encode("ascii")))
