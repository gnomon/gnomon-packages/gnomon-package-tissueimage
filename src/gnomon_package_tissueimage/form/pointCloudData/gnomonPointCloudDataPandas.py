import pickle
from base64 import b64decode
from base64 import b64encode
from copy import deepcopy

import numpy as np
import pandas as pd

import gnomon.core
from gnomon.utils import formDataPlugin
from gnomon.core import gnomonAbstractPointCloudData, gnomonPointCloud


def _position_prefixes(df):
    columns = [v for v in df.columns if not 'Unnamed:' in v]
    columns = [v for v in columns if np.array(df[v]).ndim == 1]
    property_prefixes = np.unique([v[:-2] for v in columns])
    return [v for v in property_prefixes if all([f"{v}_{dim}" in columns for dim in "xyz"])]

@formDataPlugin(version="1.0.0", coreversion="1.0.0", data_setter="set_data", data_getter="get_data", name="DataFrame")
class gnomonPointCloudDataPandas(gnomonAbstractPointCloudData):

    def __init__(self, data=None):
        super().__init__()
        if data is None:
            self._df = pd.DataFrame(columns=['center_'+dim for dim in 'xyz'])
        else:
            self._df = deepcopy(data._df)

    def __del__(self):
        #del self._df
        self._df = None

    def set_data(self, df: pd.DataFrame):
        self._df = deepcopy(df)

    def get_data(self) -> pd.DataFrame:
        return self._df

    def fromGnomonForm(self, pointCloud: gnomonPointCloud):
        point_index = pointCloud.pointIds()

        x_dict = pointCloud.pointX()
        point_x = [x_dict[p] for p in point_index]
        y_dict = pointCloud.pointY()
        point_y = [y_dict[p] for p in point_index]
        z_dict = pointCloud.pointZ()
        point_z = [z_dict[p] for p in point_index]

        df = pd.DataFrame({
            'center_x': point_x,
            'center_y': point_y,
            'center_z': point_z
        }, index=point_index)

        for property_name in pointCloud.pointPropertyNames():
            property_dict = pointCloud.pointProperty(property_name)
            df[property_name] = [property_dict[p] for p in point_index]

        self.set_data(df)

    def clone(self):
        _clone = gnomonPointCloudDataPandas(self)
        _clone.__disown__()
        return _clone

    # Metadata

    def metadata(self):
        metadata = {}
        metadata['Number of points'] = str(len(self._df))
        for property_name in self._df.columns:
            metadata['Point Property ' + property_name] = str(
                self._df[property_name].values.dtype)

        return metadata

    def dataName(self):
        return "pandas.DataFrame"

    # Point list concept

    def pointIds(self):
        return list(self._df.index)

    def pointCount(self):
        return len(self._df)

    # Point edition concept

    def setPointPosition(self, pointId, x, y, z):
        position_prefixes = _position_prefixes(self._df)
        position_prefix = 'center' if 'center' in position_prefixes else position_prefixes[0]
        if pointId in self._df.index:
            for c, p in zip([f'{position_prefix}_{dim}' for dim in 'xyz'], [x,y,z]):
                self._df[c].iloc[pointId] = p

    def addPoint(self, x, y, z):
        if len(self._df) > 0:
            pointId = int(self._df.index.max() + 1)
        else:
            pointId = 0
        position_prefixes = _position_prefixes(self._df)
        position_prefix = 'center' if 'center' in position_prefixes else position_prefixes[0]
        point_dict = dict(zip([f'{position_prefix}_{dim}' for dim in 'xyz'], [x,y,z]))
        self._df = pd.concat([self._df, pd.DataFrame(point_dict, index=[pointId])])
        return pointId

    def removePoint(self, pointId):
        if pointId in self._df.index:
            self._df.drop(index=[pointId], inplace=True)

    # Property concept

    def pointPropertyNames(self):
        return [c for c in self._df.columns]

    def hasPointProperty(self, propertyName):
        return propertyName in self._df.columns

    def pointProperty(self, propertyName):
        property_dict = dict(zip(self._df.index, self._df[propertyName]))
        return property_dict

    def pointX(self):
        position_prefixes = _position_prefixes(self._df)
        position_prefix = 'center' if 'center' in position_prefixes else position_prefixes[0]
        property_dict = dict(zip(self._df.index, self._df[f'{position_prefix}_x']))
        return property_dict

    def pointY(self):
        position_prefixes = _position_prefixes(self._df)
        position_prefix = 'center' if 'center' in position_prefixes else position_prefixes[0]
        property_dict = dict(zip(self._df.index, self._df[f'{position_prefix}_y']))
        return property_dict

    def pointZ(self):
        position_prefixes = _position_prefixes(self._df)
        position_prefix = 'center' if 'center' in position_prefixes else position_prefixes[0]
        property_dict = dict(zip(self._df.index, self._df[f'{position_prefix}_z']))
        return property_dict

    def serialize(self):
        return b64encode(pickle.dumps(self._df)).decode("ascii")

    def deserialize(self, serialization: str):
        self._df = pickle.loads(b64decode(serialization.encode("ascii")))
