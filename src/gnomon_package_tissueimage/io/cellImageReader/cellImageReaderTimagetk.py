import os
import re
from pathlib import Path

import numpy as np
import pandas as pd

from dtkcore import d_int

from gnomon.utils import algorithmPlugin, seriesReader
from gnomon.utils.decorators import cellImageOutput
from gnomon.core import gnomonAbstractCellImageReader

from timagetk.components.tissue_image import SpatialImage, TissueImage3D
from timagetk.io import imread

def array_from_printed_string(string, null_value=np.nan):
    if not pd.isnull(string):
        s = re.sub("[ ]+"," ",string)
        s = re.sub("\[ ","[",s)
        s = re.sub("\n","",s)
        s = re.sub(" ",",",s)
        s = re.sub("nan","np.nan",s)
        return np.array(eval(s))
    else:
        return null_value

@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Cell Image Reader")
@cellImageOutput('tissue', data_plugin="gnomonCellImageDataTissueImage")
@seriesReader('tissue', "path")
class cellImageReaderTimagetk(gnomonAbstractCellImageReader):
    """Read a 3D image file as a segmented cell image.

    The reader loads a .tif or a .inr image file that contains uniquely
    labelled cell regions as a data structure representing a multicellular
    tissue.
    """

    def __init__(self):
        super().__init__()

        self.tissue = {}
        self.path = None

        self._parameters = {}
        self._parameters['background_label'] = d_int("Value of the label assigned to image background", 1, 0, 255)

    def __del__(self):
        pass

    def run(self):
        self.tissue = {}
        paths = self.path.split(",")
        self.set_max_progress(3*(len(paths)))

        for time, path in enumerate(paths):
            self.set_progress_message(f"T {time} : reading CellImage")
            old_path = path
            path = Path(path)
            extension_matching = [path.name.lower().endswith(e) for e in self.extensions()]
            if any(extension_matching) and (path.exists() or old_path.__contains__("http")):
                _ext = self.extensions()[extension_matching.index(True)]
                img_basename = path.name[:-len(_ext)] + _ext
                img_path = path.parent.joinpath(img_basename)
                if old_path.__contains__("http"):
                    img_path = old_path
                img = imread(img_path)
                if img.ndim == 2:
                    md = {
                        f: img.metadata[f]
                        for f in ["acquisition_date", "filepath", "filename"]
                    }
                    img = SpatialImage(
                        img.get_array()[np.newaxis],
                        voxelsize=(1,) + tuple(img.voxelsize),
                        metadata=md
                    )
                tissue = TissueImage3D(
                    img.get_array().astype(np.uint16), voxelsize=img.voxelsize,
                    not_a_label=0, background=self['background_label']
                )
                self.increment_progress()

                self.set_progress_message(f"T {time} : computing barycenters")
                positions = tissue.cells.barycenter()
                for k, dim in enumerate(['x','y','z']):
                    tissue.cells.set_feature('barycenter_'+dim, {l:p[k] for l,p in positions.items()})
                self.increment_progress()

                data_path = os.path.join(
                    os.path.split(path)[0],
                    os.path.split(path)[1].split(".")[0] + ".csv"
                )

                if os.path.exists(data_path):
                    self.set_progress_message(f"T {time} : reading features")
                    cell_df = pd.read_csv(data_path)
                    if 'label' not in cell_df.columns:
                        cell_df['label'] = tissue.cell_ids()
                    for property_name in cell_df.columns:
                        cell_property = cell_df[property_name].values
                        if isinstance(cell_property[0], str):
                            cell_property = [array_from_printed_string(p) for p in cell_property]
                        tissue.cells.set_feature(property_name, {l:p for l,p in zip(cell_df['label'].values, cell_property)})
                self.increment_progress()

                self.tissue[time] = tissue
            else:
                self.increment_progress(3)

    def setPath(self,path):
        self.path = path

    def extensions(self):
        return ["tif", "tiff", "mha", ".mha.gz", "inr.gz", "inr"]
