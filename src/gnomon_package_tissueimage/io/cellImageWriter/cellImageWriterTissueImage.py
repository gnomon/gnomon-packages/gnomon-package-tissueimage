import logging
import os

import numpy as np
import pandas as pd

from gnomon.utils import algorithmPlugin, seriesWriter
from gnomon.utils.decorators import cellImageInput
from gnomon.core import gnomonAbstractCellImageWriter

from timagetk.io import imsave
from timagetk.features.pandas_tools import cells_to_dataframe


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Cell Image Writer")
@cellImageInput('tissue', data_plugin="gnomonCellImageDataTissueImage")
@seriesWriter('tissue', 'filepath')
class cellImageWriterTissueImage(gnomonAbstractCellImageWriter):
    def __init__(self):
        super().__init__()
        self.filepath = None
        self.tissue = {}

    def setPath(self, filepath):
        self.filepath = filepath

    def run(self):
        ext = self.filepath.split(".")
        if ext[-1] == "gz":
            ext = ext[-2] + "." +  ext[-1]
        else:
            ext = ext[-1]

        if ext in self.extensions():
            times = np.sort(list(self.tissue.keys()))
            if len(times) > 1:
                for time in self.tissue.keys():
                    (dirname, filename) = os.path.split(self.filepath)
                    dirpath = dirname + "/" + os.path.splitext(filename)[0]
                    if not os.path.exists(dirpath):
                        os.makedirs(dirpath)
                    time_filepath = os.path.join(dirpath, os.path.splitext(filename)[0] + "_t" + "%05.2f" % (time) + ext)

                    tissue = self.tissue[time]
                    imsave(time_filepath, tissue, force=True)

                    time_data_path = os.path.join(
                        os.path.split(time_filepath)[0],
                        os.path.split(time_filepath)[1].split(".")[0] + ".csv"
                    )
                    cell_df = cells_to_dataframe(tissue.cells)
                    cell_df.to_csv(time_data_path, index=False)
            else:
                tissue = list(self.tissue.values())[0]
                imsave(self.filepath, tissue, force=True)

                data_path = os.path.join(
                    os.path.split(self.filepath)[0],
                    os.path.split(self.filepath)[1].split(".")[0] + ".csv"
                )
                cell_df = cells_to_dataframe(tissue.cells, cell_ids=tissue.cell_ids())
                cell_df.to_csv(data_path, index=False)
        else:
            logging.info(f"Unexpected extension {ext}. Expected one of {self.extensions()}")

    def extensions(self):
        return ["tif", "inr.gz", "inr"]
