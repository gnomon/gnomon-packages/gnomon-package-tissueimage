import logging
import os
from pathlib import Path

import numpy as np

from gnomon.utils import algorithmPlugin, seriesReader
from gnomon.utils.decorators import imageOutput
from gnomon.core import gnomonAbstractImageReader

from timagetk import MultiChannelImage, SpatialImage
from timagetk.io import imread

@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Intensity Image Reader")
@imageOutput(attr='img', method='image', data_plugin='gnomonImageDataMultiChannelImage')
@seriesReader('img', 'path')
class imageReaderTimagetk(gnomonAbstractImageReader):
    """Read a 3D microscopy intensity image file.

    The function allows to read a single channel image file in
    .inr, .tif or .mha format into an image data structure.
    """

    def __del__(self):
        print("DEL", self.__class__)
        self.img = {}
        self.path = None

    def __init__(self):
        super().__init__()
        print("INIIIIT", self.__class__)
        self.img = {}
        self.path = None

    def setPath(self,path):
        self.path = path

    def run(self):
        self.img = {}

        if os.path.isdir(self.path):
            paths = [os.path.join(self.path, path) for path in sorted(os.listdir(self.path))]
        else:
            paths = self.path.split(",")
        self.set_max_progress(1*(len(paths)))

        for time, path in enumerate(paths):
            self.set_progress_message(f"T {time}")
            old_path = path
            path = Path(path)
            extension_matching = [path.name.lower().endswith(e) for e in self.extensions()]
            if any(extension_matching) and (path.exists() or old_path.__contains__("http")):
                _ext = self.extensions()[extension_matching.index(True)]
                self.img[time] = {}
                img_basename = path.name[:-len(_ext)] + _ext
                img_path = path.parent.joinpath(img_basename)
                if old_path.__contains__("http"):
                    img_path = old_path
                img = imread(img_path)
                if img.ndim == 2:
                    md = {
                        f: img.metadata[f]
                        for f in ["acquisition_date", "filepath", "filename"]
                    }
                    img = SpatialImage(
                        img.get_array()[np.newaxis],
                        voxelsize=(1,) + tuple(img.voxelsize),
                        metadata=md
                    )
                if isinstance(img, SpatialImage):
                    img = MultiChannelImage([img], channel_names=[""])
                self.img[time] = img
            else:
                logging.error("--> Could not find file : " + str(path))
            self.increment_progress()

    def extensions(self):
        return ["inr", "inr.gz", "mha", ".mha.gz", "tif", "tiff", "czi", "lsm"]
