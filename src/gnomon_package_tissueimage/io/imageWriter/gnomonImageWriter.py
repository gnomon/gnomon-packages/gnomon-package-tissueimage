import os

import numpy as np

from gnomon.utils import algorithmPlugin, seriesWriter
from gnomon.utils.decorators import imageInput
from gnomon.core import gnomonAbstractImageWriter

from timagetk.io.image import imsave


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Intensity Image Writer")
@imageInput('img', data_plugin='gnomonImageDataMultiChannelImage')
@seriesWriter('img', 'filepath')
class gnomonImageWriter(gnomonAbstractImageWriter):
    def __init__(self):
        super().__init__()
        self.filepath = None
        self.img = {}

    def setPath(self, filepath):
        self.filepath = filepath

    def run(self):
        ext = os.path.splitext(self.filepath)[1]
        if ext in [".tif", ".tiff"]:
            times = np.sort(list(self.img.keys()))
            if len(times) > 1:
                for time in times:
                    img = self.img[time]
                    (dirname, filename) = os.path.split(self.filepath)
                    dirpath = dirname + "/" + os.path.splitext(filename)[0]
                    if not os.path.exists(dirpath):
                        os.makedirs(dirpath)
                    time_filepath = os.path.join(dirpath, os.path.splitext(filename)[0] + "_t" + "%05.2f" % (time) + ext)
                    imsave(time_filepath, img, force=True)
            else:
                img = list(self.img.values())[0]
                imsave(self.filepath, img, force=True)

    def extensions(self):
        return ["tif", "tiff"]
