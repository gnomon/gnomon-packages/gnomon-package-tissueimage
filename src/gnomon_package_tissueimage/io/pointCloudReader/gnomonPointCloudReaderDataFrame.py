import os

import numpy as np
import pandas as pd

from gnomon.core import gnomonAbstractPointCloudReader

from gnomon.utils import algorithmPlugin, seriesReader
from gnomon.utils.decorators import pointCloudOutput


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Point Cloud Reader")
@pointCloudOutput('df', data_plugin="gnomonPointCloudDataPandas")
@seriesReader('df', 'path')
class gnomonPointCloudReaderDataFrame(gnomonAbstractPointCloudReader):
    """Load a CSV file as a 3D point cloud.

    The reader loads a data table and interprets it as a 3D point cloud
    using the columns center_x, center_y and center_z as coordinates for the
    points. The remaining columns are seen as properties attached to each
    point in the point cloud.
    """

    def __init__(self):
        super().__init__()
        self.df = {}

        self.path = None

    def __del__(self):
        pass

    def run(self):
        self.df = {}

        paths = self.path.split(",")
        self.set_max_progress(1*(len(paths)))

        for time, path in enumerate(paths):
            self.set_progress_message(f"T {time}")
            if os.path.exists(path) and any([path.endswith(e) for e in self.extensions()]):
                df = pd.read_csv(path, sep=None)
                columns = [v for v in df.columns if not 'Unnamed:' in v]
                columns = [v for v in columns if not np.array(df[v]).dtype == np.dtype('O')]
                columns = [v for v in columns if np.array(df[v]).ndim == 1]
                property_prefixes = np.unique([v[:-2] for v in columns])
                position_columns = [v for v in property_prefixes if all([f"{v}_{dim}" in columns for dim in "xyz"])]
                if len(position_columns) > 0:
                    self.df[time] = df
            self.increment_progress()


    def setPath(self,path):
        self.path = path

    def extensions(self):
        return ["csv"]
