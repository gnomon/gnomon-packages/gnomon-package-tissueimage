import os
import logging

from gnomon.core import gnomonAbstractPointCloudWriter

from gnomon.utils import algorithmPlugin, seriesWriter
from gnomon.utils.decorators import pointCloudInput


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Point Cloud Writer")
@pointCloudInput('df', data_plugin="gnomonPointCloudDataPandas")
@seriesWriter('df', 'filepath')
class pointCloudWriterDataFrame(gnomonAbstractPointCloudWriter):
    """Save a 3D point cloud as a CSV file.

    """

    def __init__(self):
        super().__init__()
        self.filepath = None
        self.df = {}

    def setPath(self, filepath):
        self.filepath = filepath

    def run(self):
        ext = os.path.splitext(self.filepath)[1]

        if ext in [".csv"]:
            times = list(self.df.keys())
            if len(times) > 1:
                for time in times:
                    df = self.df[time]
                    (dirname, filename) = os.path.split(self.filepath)
                    dirpath = dirname + "/" + os.path.splitext(filename)[0]
                    if not os.path.exists(dirpath):
                        os.makedirs(dirpath)
                    time_filepath = os.path.join(dirpath, os.path.splitext(filename)[0] + "_t" + "%05.2f" % (time) + ext)
                    df.to_csv(time_filepath, index=False)
            elif len(times) > 0:
                df = self.df[times[0]]
                df.to_csv(self.filepath, index=False)

    def extensions(self):
        return ["csv"]
