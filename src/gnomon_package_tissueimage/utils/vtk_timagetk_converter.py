""" Converter from vtkImage to SpatialImage """

import ctypes
import numpy as np

import vtk
from vtk.util.numpy_support import numpy_to_vtk, vtk_to_numpy, get_vtk_array_type

from timagetk import SpatialImage

__all__ = ['vtk_img_to_sp_img', 'sp_img_to_vtk_img']


def vtk_img_to_sp_img(vtk_img: vtk.vtkImageData) -> SpatialImage:
    """
    vtkImage to SpatialImage
    """
    if isinstance(vtk_img, vtk.vtkImageData):
        ori = vtk_img.GetOrigin()[::-1]
        vs = vtk_img.GetSpacing()[::-1]
        dims = vtk_img.GetDimensions()
        out_arr = vtk_to_numpy(vtk_img.GetPointData().GetScalars())
        out_arr = out_arr.reshape(dims[2], dims[1], dims[0])
        #out_arr = out_arr.transpose()
        out_sp_img = SpatialImage(out_arr, origin=ori, voxelsize=vs)

        #dtype = dtk_type_to_c_type(dtk_img.storageType())
        #x, y, z = dtk_img.xDim(), dtk_img.yDim(), dtk_img.zDim()
        #size = x * y * z
        #sp = dtk_img.spacing()[::-1]
        #ori = dtk_img.origin()[::-1]

        #out_arr = dtk_img.array().transpose() # if it breaks it probably comes from here

        #out_sp_img = SpatialImage(out_arr, origin=ori, voxelsize=sp)
        if 1 in out_sp_img.shape:  # 2D management
            out_sp_img = out_sp_img.to_2d()

        return out_sp_img
    else:
        print('Input is not a vtk.vtkImageData instance')
        return None



def sp_img_to_vtk_img(sp_img: SpatialImage, copy: bool=False) -> vtk.vtkImageData:
    """
    SpatialImage to vtkImage
    """
    if isinstance(sp_img, SpatialImage):
        if sp_img.is2D():  # 2D management
            sp_img = sp_img.to_3d()


        image_data = vtk.vtkImageData()
        image_array = numpy_to_vtk(sp_img.get_array().ravel(),
                                   array_type=get_vtk_array_type(sp_img.dtype),
                                   deep=copy)
        image_data.SetDimensions(sp_img.shape[::-1])
        image_data.SetSpacing(sp_img.voxelsize[::-1])
        image_data.SetOrigin(sp_img.origin[::-1])
        image_data.GetPointData().SetScalars(image_array)

        return image_data

    else:
        print('Input is not a SpatialImage instance')
        return None
