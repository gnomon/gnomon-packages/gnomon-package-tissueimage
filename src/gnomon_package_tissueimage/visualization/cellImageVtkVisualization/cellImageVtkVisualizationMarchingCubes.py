from time import time
import logging
from tqdm import tqdm

import numpy as np
import pyvista as pv

from dtkcore import d_int, d_bool, d_range_real, d_real, d_inliststring, d_string
from dtkcore import array_real_2

import gnomon.visualization
from gnomon.visualization import gnomonAbstractCellImageVtkVisualization

from gnomon.utils.decorators import cellImageInput
from gnomon.utils import visualizationPlugin

from timagetk.visu.pyvista import tissue_image_cell_polydatas, tissue_image_unstructured_grid

from .marchingCubesInteractorStyle import marchingCubesInteractorStyle


@visualizationPlugin(version="1.0.0", coreversion="1.0.0", name="Marching Cubes")
@cellImageInput('tissue', data_plugin="gnomonCellImageDataTissueImage")
class cellImageVtkVisualizationMarchingCubes(gnomonAbstractCellImageVtkVisualization):
    """Render cells as meshes using PyVista
    """
    
    def __init__(self):
        super().__init__()
        self._parameters = {}

        self._parameters["size_factor"] = d_real("Size factor", 1, 0, 1.5, 2, "Size factor applied to the cells")
        self._parameters["property_name"] = d_inliststring("Property", "", [""], "CellImage property to be displayed")

        self._parameters["value_range"] = d_range_real("Value range", array_real_2([0., 1.]), 0., 1., "Value range for color adjustment")
        self._parameters["colormap"] = gnomon.visualization.ParameterColorMap("Colormap", "glasbey", "Colormap to apply to the cellImage")
        self._parameters["alpha"] = d_real("Opacity", 1, 0, 1, 2, "Transparency value for the cellImage rendering")
        self._parameters["scalar_bar"] = d_bool("Show Scalar Bar", False, "Whether to display the scalar bar for the rendered property")

        self._parameters["resolution"] = d_real("Resolution", 1.5, 0.1, 5., 1, "Resampling voxelsize for computing the Marching Cubes")
        self._parameters['slice_thickness'] = d_real("Slice thickness", 2, 0, 5, 1, "Thickness of the 2D slice to display")

        self._parameters["x_range"] = d_range_real("X range", array_real_2([0, 100]), 0, 100, "Range of x positions of cells to display")
        self._parameters["y_range"] = d_range_real("Y range", array_real_2([0, 100]), 0, 100, "Range of y positions of cells to display")
        self._parameters["z_range"] = d_range_real("Z range", array_real_2([0, 100]), 0, 100, "Range of z positions of cells to display")

        self._parameters['show_labels'] = d_bool("Show labels", False, "Whether to display cell labels next to each point")
        self._parameters['font_size'] = d_int("Label size", 12, 0, 64, "Font size for the displayed cell labels")

        self._parameters["filter_name"] = d_inliststring("Filter", "", [""], "Cell property used to filter out cells")
        self._parameters["filter_range"] = d_range_real("Filter range", array_real_2([0., 1.]), 0., 1., "Value range for cell filtering")

        self._parameters["selected_cells"] = d_string("Selected cells", "[]", "Manually selected cells (for instance used to define a lineage)")
        self._parameters["show_selection"] = d_bool("Show selection", True, "Whether to display the selected cells")


        self._parameter_groups = {}
        for parameter_name in ['colormap', 'value_range', 'alpha']:
            self._parameter_groups[parameter_name] = 'rendering'
        for parameter_name in ['resolution', 'slice_thickness']:
            self._parameter_groups[parameter_name] = 'mesh_generation'
        for parameter_name in ['x_range', 'y_range', 'z_range', 'show_labels', 'font_size']:
            self._parameter_groups[parameter_name] = 'cell_display'
        for parameter_name in ['filter_name', 'filter_range']:
            self._parameter_groups[parameter_name] = 'filter'
        for parameter_name in ['selected_cells', 'show_selection']:
            self._parameter_groups[parameter_name] = 'selection'

        for parameter_name in ['property_name', 'filter_name', 'selected_cells']:
            self.connectParameter(parameter_name)

        self.cell_polydatas = {}
        self.cell_grid = {}
        self.resized_grid = {}
        self.display_grid = {}
        self.selection_grid = {}
        self.display_polyata = {}
        self.cell_actor3D = None
        self.cell_actor2D = None

        self.selection_actor3D = None
        self.selection_actor2D = None

        self.scalar_bar_actor = None

        self.style = marchingCubesInteractorStyle()
        self.style.setVisualization(self)
        self.style.pickedCell.connect(self._get_picked_label)

        self.picked_cells = {}
        self.picked_cells_actor3D = None
        self.picked_cells_actor2D = None

        self.label_polydata = {}
        self.label_actor3D = None
        self.label_actor2D = None

        self.slice_orientation = 2
        self.slice_position = 0

        self._current_time = None
        self._current_value = {}


    def __del__(self):
        if self.cell_actor3D is not None:
            del self.cell_actor3D
        if self.cell_actor2D is not None:
            del self.cell_actor2D
        if self.label_actor3D is not None:
            del self.label_actor3D
        if self.label_actor2D is not None:
            del self.label_actor2D
        if self.selection_actor3D is not None:
            del self.selection_actor3D
        if self.selection_actor2D is not None:
            del self.selection_actor2D

    def clear(self):
        if self.vtkView():
            self.renderer3D = self.vtkView().renderer3D()
            self.renderer2D = self.vtkView().renderer2D()
            if self.cell_actor3D is not None:
                self.renderer3D.RemoveActor(self.cell_actor3D)
            if self.cell_actor2D is not None:
                self.renderer2D.RemoveActor(self.cell_actor2D)
            if self.label_actor3D is not None:
                self.renderer3D.RemoveActor(self.label_actor3D)
            if self.label_actor2D is not None:
                self.renderer2D.RemoveActor(self.label_actor2D)
            if self.selection_actor3D is not None:
                self.renderer3D.RemoveActor(self.selection_actor3D)
            if self.selection_actor2D is not None:
                self.renderer2D.RemoveActor(self.selection_actor2D)

    def fill(self):
        if self.vtkView():
            self.renderer3D = self.vtkView().renderer3D()
            self.renderer2D = self.vtkView().renderer2D()
            if self.cell_actor3D is not None:
                self.renderer3D.AddActor(self.cell_actor3D)
            if self.cell_actor2D is not None:
                self.renderer2D.AddActor(self.cell_actor2D)
            if self.label_actor3D is not None:
                self.renderer3D.AddActor(self.label_actor3D)
            if self.label_actor2D is not None:
                self.renderer2D.AddActor(self.label_actor2D)
            if self.selection_actor3D is not None:
                self.renderer3D.AddActor(self.selection_actor3D)
            if self.selection_actor2D is not None:
                self.renderer2D.AddActor(self.selection_actor2D)

    def setVisible(self, visible):
        if self.cell_actor3D is not None:
            self.cell_actor3D.visibility = visible
        if self.cell_actor2D is not None:
            self.cell_actor2D.visibility = visible
        if self.label_actor3D is not None:
            self.label_actor3D.SetVisibility(visible and self['show_labels'])
        if self.label_actor2D is not None:
            self.label_actor2D.SetVisibility(visible and self['show_labels'])
            if self.selection_actor3D is not None:
                self.selection_actor3D.visibility = visible and self['show_selection']
            if self.selection_actor2D is not None:
                self.selection_actor2D.visibility = visible and self['show_selection']

    def imageRendering(self):
        if self.cell_actor3D is not None:
            self.updateOffscreenRenderer(*self.cell_actor3D.GetMapper().GetInput().GetBounds())
            self.offscreenRenderer().AddActor(self.cell_actor3D)
        return self.offscreenImageRendering()

    def interactorStyle(self):
        return self.style

    def render(self):
        self.view().render()

    def refreshParameters(self):
        self.cell_polydatas = {t: None for t in self.tissue.keys()}
        self.cell_grid = {t: None for t in self.tissue.keys()}
        self.resized_grid = {t: None for t in self.tissue.keys()}
        self.display_grid = {t: None for t in self.tissue.keys()}

        self.picked_cells = {t: [] for t in self.tissue.keys()}

        self._current_value = {
            t: {p: None for p in self._parameters.keys()}
            for t in self.tissue.keys()
        }

        if len(self.tissue) > 0:
            if self.view() and self.vtkView().currentTime() in self.tissue:
                self.current_time = self.vtkView().currentTime()
            else:
                self.current_time = list(self.tissue.keys())[0]

            tissue = self.tissue[self.current_time]
            prop = [p for p in tissue.cells.feature_names() if tissue.cells.feature(p) != {}]
            prop = [p for p in prop if np.array(list(tissue.cells.feature(p).values())).dtype != np.dtype('O')]
            prop = [""] + prop

            property_name = self["property_name"]
            self._parameters["property_name"].setValues(prop)
            self._parameters["property_name"].setValue(property_name if property_name in prop else "")

            filter_name = self["filter_name"]
            self._parameters["filter_name"].setValues(prop)
            self._parameters["filter_name"].setValue(filter_name if filter_name in prop else "")

            self._parameters["slice_thickness"].setValue(np.around(2*np.mean(tissue.voxelsize), decimals=1))
            self._parameters["resolution"].setValue(np.around(2*np.mean(tissue.voxelsize), decimals=1))

            self._update_value_range()
            self._update_value_range(filter=True)
            self._update_xyz_ranges()

    def onParameterChanged(self, parameter_name=None):
        if parameter_name == "property_name":
            self._update_value_range()
        elif parameter_name == "filter_name":
            self._update_value_range(filter=True)
        elif parameter_name == "selected_cells":
            self._update_selection()

    def _get_picked_label(self, cell_id):
        label = -1

        if self.display_grid[self.current_time] is not None:
            display_grid = self.display_grid[self.current_time]
            label = display_grid.cell_data['label'][cell_id]

        if label >= 0:
            if label in self.picked_cells[self.current_time]:
                self.picked_cells[self.current_time].remove(label)
            else:
                self.picked_cells[self.current_time].append(label)
        self._update_picked_cells()
        super().pickedCells(self.picked_cells[self.current_time])

        return label

    def stopPicking(self):
        self.style.disable()
        if self.picked_cells_actor3D is not None:
            self.renderer3D.RemoveActor(self.picked_cells_actor3D)
            del self.picked_cells_actor3D
            self.picked_cells_actor3D = None
        if self.picked_cells_actor2D:
            self.renderer2D.RemoveActor(self.picked_cells_actor2D)
            del self.picked_cells_actor2D
            self.picked_cells_actor2D = None

        self.picked_cells[self.current_time].clear()
        self._update_selection()

    def _update_picked_cells(self):
        start_time = time()
        picked_cells = self.picked_cells[self.current_time]
        
        if len(picked_cells)>0:
            display_grid = self.display_grid[self.current_time]
            cell_ids = [c for c, l in enumerate(display_grid.cell_data['label']) if l in picked_cells]
            picked_cells_grid = display_grid.extract_cells(cell_ids)
            picked_cells_grid.cell_data['selection_label'] = [
                len(eval(self["selected_cells"]))%256
                for l in picked_cells_grid.cell_data['label']
            ]
            point_cell_centers = picked_cells_grid.point_data['cell_center']
            picked_cells_grid.points = point_cell_centers + 1.05*(picked_cells_grid.points-point_cell_centers)

            picked_cells_grid.cell_data.active_scalars_name = 'selection_label'
        else:
            picked_cells_grid = pv.UnstructuredGrid()

        plotter = pv.Plotter(off_screen=True)

        if self.picked_cells_actor3D is not None:
            self.renderer3D.RemoveActor(self.picked_cells_actor3D)
        self.picked_cells_actor3D = plotter.add_mesh(
            picked_cells_grid,
            pbr=True, metallic=0.3, roughness=0.2, ambient=1,
            scalars='selection_label', cmap='glasbey', clim=(0, 255)
        )
        self.renderer3D.AddActor(self.picked_cells_actor3D)

        if self.picked_cells_actor2D is not None:
            self.renderer3D.RemoveActor(self.picked_cells_actor2D)
        self.picked_cells_actor2D = plotter.add_mesh(
            picked_cells_grid,
            pbr=True, metallic=0.3, roughness=0.2, ambient=1,
            scalars='selection_label', cmap='glasbey', clim=(0, 255)
        )
        self.renderer2D.AddActor(self.picked_cells_actor2D)

        logging.debug(f"Displaying picked cells [{time() - start_time}s]")

    def _update_value_range(self, filter=False):
        if self.view():
            self.current_time = self.vtkView().currentTime()
        name_param = "filter_name" if filter else "property_name"
        range_param = "filter_range" if filter else "value_range"

        if self.current_time in self.tissue.keys():
            tissue = self.tissue[self.current_time]

            if self[name_param] in tissue.cells.feature_names():
                cell_property = np.array([
                    tissue.cells.feature(self[name_param])[c]
                    for c in tissue.cell_ids()
                    if c in tissue.cells.feature(self[name_param])
                ])
                if cell_property.ndim > 1:
                    cell_property = np.array([np.linalg.norm(p) for p in cell_property])
            else:
                cell_property = tissue.cell_ids()

            min_p = float(np.around(np.nanmin(cell_property), decimals=3))
            max_p = float(np.around(np.nanmax(cell_property), decimals=3))

            self._parameters[range_param].setMin(np.around(min_p - (max_p - min_p)/2, decimals=3))
            self._parameters[range_param].setMax(np.around(max_p + (max_p - min_p)/2, decimals=3))
            self._parameters[range_param].setValueMin(min_p)
            self._parameters[range_param].setValueMax(max_p)

    def _update_xyz_ranges(self):
        if self.view():
            self.current_time = self.vtkView().currentTime()
        if self.current_time in self.tissue.keys():
            tissue = self.tissue[self.current_time]

            for i, dim in enumerate('zyx'):
                dim_range_min = np.around(float(tissue.origin[i]), decimals=3)
                dim_range_max = np.around(float(tissue.origin[i] + tissue.extent[i]), decimals=3)
                self._parameters[dim+"_range"].setMin(dim_range_min)
                self._parameters[dim+"_range"].setMax(dim_range_max)
                self._parameters[dim+"_range"].setValueMin(dim_range_min)
                self._parameters[dim+"_range"].setValueMax(dim_range_max)

    def _update_cell_meshes(self):
        current_value = self._current_value[self.current_time]
        tissue = self.tissue[self.current_time]

        start_time = time()
        resolution_changed = current_value["resolution"] != self["resolution"]
        if (self.cell_grid[self.current_time] is None) or resolution_changed:
            subsampling = [int(np.round(self["resolution"] / v)) for v in tissue.voxelsize]

            cell_polydatas = tissue_image_cell_polydatas(
                tissue.transpose('xyz'),
                resampling_voxelsize=self['resolution'],
                smoothing_iterations=10,
                target_reduction=0,
                use_resample=False
            )
            self.cell_polydatas[self.current_time] = cell_polydatas
            cell_grid = tissue_image_unstructured_grid(
                tissue,
                cell_polydatas=cell_polydatas
            )
            if 'ancestor' in cell_grid.cell_data:
                cell_grid.cell_data['display_ancestor'] = cell_grid.cell_data['ancestor']%256

            point_cell_ids = -np.ones(cell_grid.n_points, int)
            offset = 0
            for c in range(cell_grid.n_cells):
                cell_n_points = cell_grid.cells[offset]
                cell_point_ids = cell_grid.cells[offset+1:offset+cell_n_points+1]
                point_cell_ids[cell_point_ids] = c
                offset += cell_n_points+1
            cell_grid.point_data['cell_id'] = point_cell_ids

            cell_centers = tissue.cells.feature('barycenter')
            grid_cell_centers = np.array([cell_centers[l] for l in cell_grid.cell_data['label']])
            cell_grid.point_data['cell_center'] = grid_cell_centers[point_cell_ids]

            self.cell_grid[self.current_time] = cell_grid
            self.resized_grid[self.current_time] = None
            current_value["resolution"] = self["resolution"]

        logging.debug(f"Meshing cells ({resolution_changed}) [{time() - start_time}s]")

    def _update_resized_cells(self):
        current_value = self._current_value[self.current_time]

        start_time = time()
        size_factor_changed = current_value["size_factor"] != self["size_factor"]
        if (self.resized_grid[self.current_time] is None) or size_factor_changed:
            resized_grid = self.cell_grid[self.current_time].copy()
            point_cell_centers = resized_grid.point_data['cell_center']
            resized_grid.points = point_cell_centers + self['size_factor']*(resized_grid.points-point_cell_centers)
            self.resized_grid[self.current_time] = resized_grid
            self.display_grid[self.current_time] = None
            current_value["size_factor"] = self["size_factor"]
        logging.debug(f"Resizing cells ({size_factor_changed}) [{time() - start_time}s]")

    def _update_displayed_cells(self):
        current_value = self._current_value[self.current_time]
        tissue = self.tissue[self.current_time]

        start_time = time()
        displayed_cells_changed = any([
            (current_value[f"{dim}_range"] is None) or any([
                current_value[f"{dim}_range"][k] != self[f"{dim}_range"][k]
                for k in range(2)
            ])
            for dim in 'xyz'
        ])
        displayed_cells_changed |= any([
            current_value['filter_name'] != self['filter_name'],
            (current_value['filter_range'] is None) or any([
                current_value['filter_range'][k] != self['filter_range'][k]
                for k in range(2)
            ])
        ])
        if (self.display_grid[self.current_time] is None) or displayed_cells_changed:
            cell_labels = np.array(tissue.cell_ids())
            if not np.all([l in tissue.cells.feature('barycenter') for l in cell_labels]):
                positions = tissue.cells.barycenter()
                for k, dim in enumerate(['x','y','z']):
                    tissue.cells.set_feature('barycenter_'+dim, {l:p[k] for l,p in positions.items()})
            cell_points = np.array([tissue.cells.feature('barycenter')[l] for l in cell_labels])
            display_range = np.array([[self[f"{dim}_range"][k] for dim in 'xyz'] for k in range(2)])
            cell_display = np.all([cell_points > display_range[0], cell_points < display_range[1]], axis=-1).all(axis=0)
            cell_labels = cell_labels[cell_display]

            if self['filter_name'] in tissue.cells.feature_names():
                cell_filter = tissue.cells.feature(self['filter_name'])
                cell_labels = [l for l in cell_labels if cell_filter[l] >= self["filter_range"][0]]
                cell_labels = [l for l in cell_labels if cell_filter[l] <= self["filter_range"][1]]

            resized_grid = self.resized_grid[self.current_time]
            if resized_grid is not None:
                cell_ids = [c for c, l in enumerate(resized_grid.cell_data['label']) if l in cell_labels]
                display_grid = resized_grid.extract_cells(cell_ids)
                self.display_grid[self.current_time] = display_grid
                self.selection_grid[self.current_time] = None

                display_polyata = display_grid.extract_surface()
                display_point_cell_ids = display_grid.point_data['cell_id']
                for property_name in display_grid.cell_data.keys():
                    display_polyata.point_data[property_name] = resized_grid.cell_data[property_name][display_point_cell_ids]

                self.display_polyata[self.current_time] = display_polyata
            self.label_polydata[self.current_time] = None

            for dim in 'xyz':
                current_value[f"{dim}_range"] = [self[f"{dim}_range"][k] for k in range(2)]
            current_value['filter_name'] = self['filter_name']
            current_value['filter_range'] = [self['filter_range'][k] for k in range(2)]
        logging.debug(f"Extracting displayed cells ({displayed_cells_changed}) [{time() - start_time}s]")

    def _update_label_points(self):
        tissue = self.tissue[self.current_time]

        start_time = time()
        if self.label_polydata[self.current_time] is None and self['show_labels']:
            display_grid = self.display_grid[self.current_time]
            cell_polydatas = self.cell_polydatas[self.current_time]

            cell_neighbors = tissue.neighbors()
            cell_centers = tissue.cells.feature('barycenter')
            cell_volumes = tissue.cells.volume()

            display_labels = display_grid.cell_data['label']
            grid_bbox = pv.Cube(bounds=display_grid.bounds)

            label_points = []
            for c, l in tqdm(enumerate(display_labels), total=display_grid.n_cells, unit='cell'):
                cell_center = cell_centers[l]
                neighborhood = [l] + [n for n in cell_neighbors[l] if n in display_labels]
                neighborhood_center = sum(cell_centers[n]*cell_volumes[n] for n in neighborhood)/sum(cell_volumes[n] for n in neighborhood)
                direction = (cell_center - neighborhood_center)/np.linalg.norm(cell_center - neighborhood_center)

                cell_radius = np.power(cell_volumes[l]/(4*np.pi/3), 1/3)

                # intersection, _ = display_grid.extract_cell([c]).extract_surface.ray_trace(
                intersection, _ = cell_polydatas[l].ray_trace(
                    cell_center, cell_center + 4*cell_radius*direction, first_point=True
                )
                if any(intersection):
                    label_points.append(intersection + 0.1*direction)
                else:
                    intersection, _ = grid_bbox.ray_trace(
                        cell_center, cell_center + 16*cell_radius*direction, first_point=True
                    )
                    if any(intersection):
                        label_points.append(intersection + 0.1*direction)
                    else:
                        label_points.append(cell_center + 2*cell_radius*direction)

            label_polydata = pv.PolyData(label_points)
            label_polydata.point_data['label'] = display_labels
            self.label_polydata[self.current_time] = label_polydata
        logging.debug(f"Creating label points [{time() - start_time}s]")

    def _update_rendering(self, force=False):
        current_value = self._current_value[self.current_time]

        start_time = time()
        rendering_changed = force or any([
            current_value['property_name'] != self['property_name'],
            current_value['alpha'] != self['alpha'],
            (current_value['value_range'] is None) or any([
                current_value['value_range'][k] != self['value_range'][k]
                for k in range(2)
            ]),
            current_value['colormap'] != self._parameters['colormap'].name(),
        ])

        scalars = self['property_name']
        if self['property_name'] == "":
            scalars = 'display_label'
        elif self['property_name'] == 'ancestor':
            scalars = 'display_ancestor'

        if self.cell_actor3D is None or rendering_changed:
            plotter_args = {
                "scalars": scalars,
                "cmap": self._parameters['colormap'].name(),
                "clim": (0, 255) if self['property_name'] in ["", "ancestor"] else self["value_range"],
                "opacity": self['alpha']
            }

            plotter = pv.Plotter(off_screen=True)

            if self.cell_actor3D is not None:
                self.renderer3D.RemoveActor(self.cell_actor3D)
            self.cell_actor3D = plotter.add_mesh(
                self.display_grid[self.current_time],
                **plotter_args
            )
            self.renderer3D.AddActor(self.cell_actor3D)

            if self.scalar_bar_actor is not None:
                self.renderer3D.RemoveActor(self.scalar_bar_actor)

            self.scalar_bar_actor = plotter.add_scalar_bar(
                mapper=self.cell_actor3D.mapper,
                label_font_size=14,
                color='w',
                width=0.25,
                position_x=0.72,
                position_y=0.05
            )
            if self.scalar_bar_actor is not None:
                self.scalar_bar_actor.SetTitle(self['property_name'])
                self.renderer3D.AddActor(self.scalar_bar_actor)

            if self.cell_actor2D is not None:
                self.renderer2D.RemoveActor(self.cell_actor2D)
            self.cell_actor2D = plotter.add_mesh(
                self.display_polyata[self.current_time],
                lighting=False, **plotter_args
            )
            self.renderer2D.AddActor(self.cell_actor2D)

            current_value['property_name'] = self['property_name']
            current_value['alpha'] = self['alpha']
            current_value['value_range'] = [self['value_range'][k] for k in range(2)]
            current_value['colormap'] = self._parameters['colormap'].name()
        else:
            self.cell_actor3D.mapper.SetInputData(self.display_grid[self.current_time])
            self.display_grid[self.current_time].cell_data.active_scalars_name = scalars
            self.cell_actor2D.mapper.SetInputData( self.display_polyata[self.current_time])
            self.display_polyata[self.current_time].cell_data.active_scalars_name = scalars

        if self.label_polydata[self.current_time] is not None:
            labels_changed = current_value['font_size'] != self['font_size']
            if self.label_actor3D is None or labels_changed:
                plotter = pv.Plotter(off_screen=True)

                if self.label_actor3D is not None:
                    self.renderer3D.RemoveActor(self.label_actor3D)
                self.label_actor3D = plotter.add_point_labels(
                    self.label_polydata[self.current_time], labels='label',
                    show_points=False, shape=None, background_opacity=0,
                    italic=False, bold=True, font_size=self['font_size'], text_color='w', shadow=True,
                    justification_horizontal='center', justification_vertical='center',
                    always_visible=False
                )
                mapper = self.label_actor3D.GetMapper()
                hier = mapper.GetInputConnection(0, 0).GetProducer()
                vis_points = hier.GetInputConnection(0, 0).GetProducer()
                vis_points.SetRenderer(self.renderer3D)
                self.renderer3D.AddActor(self.label_actor3D)

                if self.label_actor2D is not None:
                    self.renderer2D.RemoveActor(self.label_actor2D)
                self.label_actor2D = plotter.add_point_labels(
                    self.label_polydata[self.current_time], labels='label',
                    show_points=False, shape=None, background_opacity=0,
                    italic=False, bold=True, font_size=self['font_size'], text_color='w', shadow=True,
                    justification_horizontal='center', justification_vertical='center',
                    always_visible=True
                )
                self.renderer2D.AddActor(self.label_actor2D)
            else:
                mapper = self.label_actor3D.GetMapper()
                hier = mapper.GetInputConnection(0, 0).GetProducer()
                vis_points = hier.GetInputConnection(0, 0).GetProducer()
                vis_points.SetInputData(self.label_polydata[self.current_time])

                mapper = self.label_actor2D.GetMapper()
                hier = mapper.GetInputConnection(0, 0).GetProducer()
                hier.SetInputData(self.label_polydata[self.current_time])

            self.label_actor3D.SetVisibility(self['show_labels'])
            self.label_actor2D.SetVisibility(self['show_labels'])

        logging.debug(f"Rendering actors ({rendering_changed}) [{time() - start_time}s]")

    def _update_selection(self):
        current_value = self._current_value[self.current_time]

        start_time = time()
        selection_changed = current_value['selected_cells'] != self['selected_cells']
        if self.selection_grid[self.current_time] is None or selection_changed:
            selected_cell_index = self._get_selected_cells()
            if len(selected_cell_index)>0:
                display_grid = self.display_grid[self.current_time]
                cell_ids = [c for c, l in enumerate(display_grid.cell_data['label']) if l in selected_cell_index]
                selection_grid = display_grid.extract_cells(cell_ids)
                selection_grid.cell_data['selection_label'] = [
                    selected_cell_index[l]%256
                    for l in selection_grid.cell_data['label']
                ]
                point_cell_centers = selection_grid.point_data['cell_center']
                selection_grid.points = point_cell_centers + 1.05*(selection_grid.points-point_cell_centers)

                plotter = pv.Plotter(off_screen=True)

                if self.selection_actor3D is not None:
                    self.renderer3D.RemoveActor(self.selection_actor3D)
                self.selection_actor3D = plotter.add_mesh(
                    selection_grid,
                    pbr=True, metallic=0.3, roughness=0.2, ambient=1,
                    scalars='selection_label', cmap='glasbey', clim=(0, 255)
                )
                self.renderer3D.AddActor(self.selection_actor3D)

                if self.selection_actor2D is not None:
                    self.renderer2D.RemoveActor(self.selection_actor2D)
                self.selection_actor2D = plotter.add_mesh(
                    selection_grid,
                    pbr=True, metallic=0.3, roughness=0.2, ambient=1,
                    scalars='selection_label', cmap='glasbey', clim=(0, 255)
                )
                self.renderer2D.AddActor(self.selection_actor2D)

                self.selection_grid[self.current_time] = selection_grid
                current_value['selected_cells'] = self['selected_cells']

        if self.selection_actor3D is not None:
            self.selection_actor3D.visibility = self['show_selection']
            self.selection_actor2D.visibility = self['show_selection']

        logging.debug(f"Displaying selection ({selection_changed}) [{time() - start_time}s]")

    def _update_slice(self):
        if self.display_polyata[self.current_time] is not None:
            display_grid = self.display_grid[self.current_time]
            display_polyata = self.display_polyata[self.current_time]

            start_time = time()
            slice_mask = display_polyata.points[:, self.slice_orientation] >  self.slice_position - self['slice_thickness']/2
            slice_mask &= display_polyata.points[:, self.slice_orientation] <  self.slice_position + self['slice_thickness']/2
            slice_point_ids = np.arange(display_polyata.n_points)[slice_mask]
            display_polyata_2D = display_polyata.extract_points(slice_point_ids, include_cells=True)
            logging.debug(f"Clipping surface [{time() - start_time}s]")

            self.cell_actor2D.GetMapper().SetInputData(display_polyata_2D)

            if self['show_labels']:
                start_time = time()
                if display_polyata_2D.n_points > 0:
                    display_labels = np.unique(display_polyata_2D.point_data['label'])

                    tissue = self.tissue[self.current_time]
                    cell_centers = tissue.cells.feature('barycenter')

                    label_points = np.array([cell_centers[l] for l in display_labels])
                    label_points[:, self.slice_orientation] = self.slice_position

                    label_polydata_2D = pv.PointSet(label_points)
                    label_polydata_2D.point_data['label'] = display_labels
                else:
                    label_polydata_2D = pv.PointSet()

                logging.debug(f"Clipping labels [{time() - start_time}s]")
                mapper = self.label_actor2D.GetMapper()
                hier = mapper.GetInputConnection(0, 0).GetProducer()
                hier.SetInputData(label_polydata_2D)

            if self['show_selection']:
                start_time = time()
                selection_grid_2D = pv.UnstructuredGrid()
                if display_polyata_2D.n_points > 0:
                    display_labels = np.unique(display_polyata_2D.point_data['label'])

                    selection_grid = self.selection_grid[self.current_time]
                    if selection_grid is not None:
                        selection_cells = [
                            c for c, l in enumerate(selection_grid.cell_data['label']) if l in display_labels
                        ]
                        if len(selection_cells) > 0:
                            selection_grid_2D = selection_grid.extract_cells(selection_cells)

                logging.debug(f"Clipping selection [{time() - start_time}s]")
                if self.selection_actor2D is not None:
                    self.selection_actor2D.mapper.SetInputData(selection_grid_2D)

    def _update_scalar_bar(self):
        if self.scalar_bar_actor is not None:
            self.scalar_bar_actor.SetVisibility(self['scalar_bar'])

    def _get_selected_cells(self):
        selected_cells = eval(self['selected_cells'])
        selected_cell_index = {}
        for i, l in enumerate(selected_cells):
            if isinstance(l, (list, tuple, set)):
                selected_cell_index.update({_l: i for _l in l})
            else:
                selected_cell_index[l] = i
        return selected_cell_index

    def update(self):
        self.renderer3D = self.vtkView().renderer3D()
        self.renderer2D = self.vtkView().renderer2D()
        self.current_time = self.vtkView().currentTime()

        if self.current_time in self.tissue.keys():
            self._update_cell_meshes()
            self._update_resized_cells()
            self._update_displayed_cells()

            self._update_label_points()

            self._update_rendering()
            self._update_scalar_bar()

            if self.display_grid[self.current_time] is not None:
                bounds = self.display_grid[self.current_time].bounds
                self.vtkView().setBounds(*bounds)

            self._update_selection()
            self._update_slice()
        self.render()

    def onTimeChanged(self, value):
        if value in self.tissue.keys():
            time_changed =  value != self.current_time
            self.current_time = value
            if time_changed:
                self._update_xyz_ranges()
                self.update()
        self.render()

    def on3D(self):
        self.style.on3D()
        self.render()

    def on2D(self):
        self.style.on2D()
        self.render()

    def onXY(self):
        self.render()

    def onYZ(self):
        self.render()

    def onXZ(self):
        self.render()

    def onSliceOrientationChanged(self, value):
        self.slice_orientation = value

    def onSliceChanged(self, value):
        if value != self.slice_position:
            self.slice_position = value
            self._update_slice()
        self.render()
