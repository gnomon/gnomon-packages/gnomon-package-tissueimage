from tqdm import tqdm

import numpy as np

import vtk
from vtk.util.numpy_support import numpy_to_vtk, vtk_to_numpy, get_vtk_array_type
import pyvista as pv

from dtkcore import d_bool, d_inliststring, d_range_real, d_real, d_string,  array_real_2

import gnomon.core
import gnomon.visualization
from gnomon.visualization import gnomonAbstractCellImageVtkVisualization

from gnomon.utils.decorators import cellImageInput
from gnomon.utils import visualizationPlugin, DEBUG

from timagetk import LabelledImage

from visu_core.vtk.utils.image_tools import _vtk_smooth_polydata
from visu_core.vtk.utils.polydata_tools import vtk_combine_polydatas, vtk_slice_polydata
from visu_core.vtk.actor import vtk_actor
from visu_core.vtk.utils.matplotlib_tools import vtk_lookuptable_from_mpl_cmap

orientation_axes = {0: 'x', 1: 'y', 2: 'z'}


def vtk_expanded_polydata(polydata, factor=1.05):
    expanded_polydata = vtk.vtkPolyData()
    expanded_polydata.DeepCopy(polydata)
    points = vtk_to_numpy(polydata.GetPoints().GetData())
    center = np.nanmean(points, axis=0)
    expanded_points = center + factor*(points - center)
    expanded_polydata.GetPoints().SetData(numpy_to_vtk(expanded_points, deep=True, array_type=vtk.VTK_DOUBLE))
    return expanded_polydata


@visualizationPlugin(version="1.0.0", coreversion="1.0.0", name="Wall Marching Cubes")
@cellImageInput('tissue_image', data_plugin="gnomonCellImageDataTissueImage")
class cellWallsMarchingCubes(gnomonAbstractCellImageVtkVisualization):
    '''Visualization of cell walls using marching cubes
    '''
    def __init__(self):
        super().__init__()
        self._parameters = {}

        self._parameters["property_name"] = d_inliststring("Property", "", [""], "CellImage property to be displayed")
        self._parameters["value_range"] = d_range_real("Value range", array_real_2([0., 1.]), 0., 1., "Value range for color adjustment")
        self._parameters["colormap"] = gnomon.visualization.ParameterColorMap("Colormap", "glasbey", "Colormap to apply to the cellImage")
        self._parameters["size_factor"] = d_real("Size factor", 1, 0, 1.5, 2, "Size factor applied to the cells")
        self._parameters["alpha"] = d_real("Opacity", 1, 0, 1, 2, "Transparency value for the cellImage rendering")
        self._parameters["resolution"] = d_real("Resolution", 1.5, 0.1, 5., 1, "Resampling voxelsize for computing the Marching Cubes")
        self._parameters['slice_thickness'] = d_real("Slice thickness", 2, 0, 5, 1, "Thickness of the 2D slice to display")

        self._parameters["outer_walls"] = d_bool("Outer walls", False, "Whether to display walls between cells and the background")
        self._parameters["x_range"] = d_range_real("X range", array_real_2([0, 100]), 0, 100, "Range of x positions of cells to display")
        self._parameters["y_range"] = d_range_real("Y range", array_real_2([0, 100]), 0, 100, "Range of y positions of cells to display")
        self._parameters["z_range"] = d_range_real("Z range", array_real_2([0, 100]), 0, 100, "Range of z positions of cells to display")

        self._parameters["filter_name"] = d_inliststring("Filter", "", [""], "Cell property used to filter out cells")
        self._parameters["filter_range"] = d_range_real("Filter range", array_real_2([0., 1.]), 0., 1., "Value range for cell filtering")
        self._parameters["filter_type"] = d_inliststring("Filter type", "all", ["all", "any"], "Whether to keep walls for which all or at least one cell lies within the range")

        self._parameter_groups = {}
        for parameter_name in ['colormap', 'value_range', 'alpha']:
            self._parameter_groups[parameter_name] = 'rendering'
        for parameter_name in ['resolution', 'slice_thickness']:
            self._parameter_groups[parameter_name] = 'mesh_generation'
        for parameter_name in ['outer_walls', 'x_range', 'y_range', 'z_range']:
            self._parameter_groups[parameter_name] = 'wall_display'
        for parameter_name in ['filter_name', 'filter_range', 'filter_type']:
            self._parameter_groups[parameter_name] = 'cell_filter'

        self.tissue_image = {}
        self.current_time = None

        self.wall_polydata3D = None
        self.wall_actor3D = None

        self.wall_polydata2D = None
        self.wall_actor2D = None

        self.all_polydatas = None

        self.slice_orientation = 2
        self.slice_position = 0
        self.is2D = False

        self.renderer3D = None
        self.renderer2D = None
        self.interactor = None

        self._current_resolution = None
        self._current_property_name = None
        self._current_filter_name = None

        self.connectParameter("property_name")
        self.connectParameter("filter_name")

    def __del__(self):
        if self.wall_actor3D is not None:
            del self.wall_actor3D
        if self.wall_actor2D is not None:
            del self.wall_actor2D

    def pluginName(self):
        return "CellImageMarchingCubesWalls"

    def clear(self):
        if self.vtkView():
            self.renderer3D = self.vtkView().renderer3D()
            self.renderer2D = self.vtkView().renderer2D()
            if self.wall_actor3D is not None:
                self.renderer3D.RemoveActor(self.wall_actor3D)
            if self.wall_actor2D is not None:
                self.renderer2D.RemoveActor(self.wall_actor2D)

    def fill(self):
        if self.vtkView():
            self.renderer3D = self.vtkView().renderer3D()
            self.renderer2D = self.vtkView().renderer2D()
            if self.wall_actor3D is not None:
                self.renderer3D.AddActor(self.wall_actor3D)
            if self.wall_actor2D is not None:
                self.renderer2D.AddActor(self.wall_actor2D)

    def setVisible(self, visible):
        if self.wall_actor3D is not None:
            self.wall_actor3D.SetVisibility(visible)
        if self.wall_actor2D is not None:
            self.wall_actor2D.SetVisibility(visible)

    def onParameterChanged(self, parameter_name=None):
        if parameter_name == "property_name":
            self._update_value_range()
            self._current_property_name = self['property_name']
        elif parameter_name == "filter_name":
            self._update_filter_range()
            self._current_filter_name = self['filter_name']

    def refreshParameters(self):
        if len(self.tissue_image) > 0:
            if self.view() and self.vtkView().currentTime() in self.tissue_image:
                self.current_time = self.vtkView().currentTime()
            else:
                self.current_time = list(self.tissue_image.keys())[0]

            tissue_image = self.tissue_image[self.current_time]
            wall_prop = [""]+[p for p in tissue_image.walls.feature_names() if tissue_image.walls.feature(p) != {}]
            cell_prop = [""]+[p for p in tissue_image.cells.feature_names() if tissue_image.cells.feature(p) != {}]

            property_name = self["property_name"]
            self._parameters["property_name"].setValues(wall_prop)
            self._parameters["property_name"].setValue(property_name if property_name in wall_prop else "")

            filter_name = self["filter_name"]
            self._parameters["filter_name"].setValues(cell_prop)
            self._parameters["filter_name"].setValue(filter_name if filter_name in cell_prop else "")

            self._parameters["slice_thickness"].setValue(np.around(2*np.sqrt(2)*np.mean(tissue_image.voxelsize), decimals=1))
            self._parameters["resolution"].setValue(np.around(2*np.sqrt(2)*np.mean(tissue_image.voxelsize), decimals=1))

            self._current_resolution = None
            self._current_property_name = None
            self._current_filter_name = None

            self._update_value_range()
            self._update_filter_range()
            self._update_xyz_ranges()

    def _update_value_range(self):
        if self.view():
            self.current_time = self.vtkView().currentTime()
        name_param = "property_name"
        range_param = "value_range"

        if self.current_time in self.tissue_image.keys():
            tissue_image = self.tissue_image[self.current_time]

            if self[name_param] in tissue_image.walls.feature_names():
                wall_property = np.array([tissue_image.walls.feature(self[name_param])[w]
                                          for w in tissue_image.wall_ids()
                                          if w in tissue_image.walls.feature(self[name_param])])
                if wall_property.ndim > 1:
                    wall_property = np.array([np.linalg.norm(p) for p in wall_property])
            else:
                wall_property = tissue_image.wall_ids()

            min_p = float(np.around(np.nanmin(wall_property), decimals=3))
            max_p = float(np.around(np.nanmax(wall_property), decimals=3))

            self._parameters[range_param].setMin(np.around(min_p - (max_p - min_p)/2, decimals=3))
            self._parameters[range_param].setMax(np.around(max_p + (max_p - min_p)/2, decimals=3))
            self._parameters[range_param].setValueMin(min_p)
            self._parameters[range_param].setValueMax(max_p)

    def _update_filter_range(self):
        if self.view():
            self.current_time = self.vtkView().currentTime()
        name_param = "filter_name"
        range_param = "filter_range"

        if self.current_time in self.tissue_image.keys():
            tissue_image = self.tissue_image[self.current_time]

            if self[name_param] in tissue_image.cells.feature_names():
                cell_property = np.array([tissue_image.cells.feature(self[name_param])[c]
                                          for c in tissue_image.cell_ids()
                                          if c in tissue_image.cells.feature(self[name_param])])
                if cell_property.ndim > 1:
                    cell_property = np.array([np.linalg.norm(p) for p in cell_property])
            else:
                cell_property = tissue_image.cell_ids()

            min_p = float(np.around(np.nanmin(cell_property), decimals=3))
            max_p = float(np.around(np.nanmax(cell_property), decimals=3))

            self._parameters[range_param].setMin(np.around(min_p - (max_p - min_p)/2, decimals=3))
            self._parameters[range_param].setMax(np.around(max_p + (max_p - min_p)/2, decimals=3))
            self._parameters[range_param].setValueMin(min_p)
            self._parameters[range_param].setValueMax(max_p)

    def _update_xyz_ranges(self):
        if self.view():
            self.current_time = self.vtkView().currentTime()
        if self.current_time in self.tissue_image.keys():
            tissue_image = self.tissue_image[self.current_time]

            for i, dim in enumerate('zyx'):
                dim_range_min = np.around(float(tissue_image.origin[i]), decimals=3)
                dim_range_max = np.around(float(tissue_image.origin[i] + tissue_image.extent[i]), decimals=3)
                self._parameters[dim+"_range"].setMin(dim_range_min)
                self._parameters[dim+"_range"].setMax(dim_range_max)
                self._parameters[dim+"_range"].setValueMin(dim_range_min)
                self._parameters[dim+"_range"].setValueMax(dim_range_max)

    def imageRendering(self):
        if self.wall_actor3D is not None:
            self.updateOffscreenRenderer(*self.wall_actor3D.GetMapper().GetInput().GetBounds())
            self.offscreenRenderer().AddActor(self.wall_actor3D)
        return self.offscreenImageRendering()

    def _update_lookuptable(self):
        value_range = self['value_range']
        if self._parameters['colormap'].name() == "glasbey":
            value_range = (0, 255)

        lut = vtk_lookuptable_from_mpl_cmap(self._parameters['colormap'].name(), value_range)
        if self.wall_actor3D is not None:
            self.wall_actor3D.GetMapper().SetLookupTable(lut)
            self.wall_actor3D.GetProperty().SetOpacity(self["alpha"])
        if self.wall_actor2D is not None:
            self.wall_actor2D.GetMapper().SetLookupTable(lut)
            self.wall_actor2D.GetProperty().SetOpacity(self["alpha"])

    def update(self):
        self.renderer3D = self.vtkView().renderer3D()
        self.renderer2D = self.vtkView().renderer2D()
        self.current_time = self.vtkView().currentTime()

        if self.current_time in self.tissue_image.keys():
            tissue = self.tissue_image[self.current_time]

            resolution_changed = self._current_resolution != self["resolution"]
            if (self.all_polydatas is None) or resolution_changed:
                subsampling = [int(np.round(self["resolution"]/v)) for v in tissue.voxelsize]

                subsampling = [int(np.round(self["resolution"]/v)) for v in tissue.voxelsize]
                img_array = np.array(tissue)[::subsampling[0],::subsampling[1],::subsampling[2]]
                voxelsize = np.array(tissue.voxelsize)*subsampling

                image_grid = pv.ImageData(dimensions=img_array.shape[::-1], spacing=voxelsize[::-1])

                label_polydatas = {}
                all_points = []
                all_point_cells = []
                for l in tqdm(tissue.labels(), unit="label"):
                    image_grid.point_data['mask'] = (img_array == l).flatten().astype(float)
                    label_polydatas[l] = image_grid.contour(isosurfaces=[0.5], scalars='mask', method='marching_cubes')
                    label_polydatas[l].point_data['cell'] = l
                    label_polydatas[l].point_data['label'] = l%256
                    all_points += [tuple(p) for p in label_polydatas[l].points]
                    all_point_cells += [l for p in label_polydatas[l].points]
                point_cells = {}
                for p, c in zip(all_points, all_point_cells):
                    if not p in point_cells:
                        point_cells[p] = []
                    point_cells[p] += [c]

                labels_to_remove = []
                for l in tqdm(label_polydatas.keys(), unit="label"):
                    triangles = label_polydatas[l].faces.reshape((-1, 4))[:, 1:]
                    triangle_points = label_polydatas[l].points[triangles]
                    triangle_walls = [np.unique([point_cells[tuple(p)] for p in t_p]) for t_p in triangle_points]

                    triangle_keep = np.array([len(w)==2 and w[0]==l for i, w in enumerate(triangle_walls)])
                    if triangle_keep.sum() > 0:
                        label_polydatas[l].faces = np.concatenate([3*np.ones(int(np.sum(triangle_keep)), int)[:, np.newaxis], triangles[triangle_keep]], axis=1).flatten()
                        label_polydatas[l].cell_data['wall'] = [tuple(w) for w in np.array(triangle_walls, dtype=object)[triangle_keep]]
                    else:
                        labels_to_remove += [l]
                label_polydatas = {l: p for l, p in label_polydatas.items() if not l in labels_to_remove}

                wall_polydatas = {}
                for l in tqdm(label_polydatas.keys(), unit="label"):
                    label_wall_ids = np.unique(label_polydatas[l].cell_data['wall'], axis=0)
                    triangles = label_polydatas[l].faces.reshape((-1, 4))[:, 1:]
                    for w in label_wall_ids:
                        wall_triangles = triangles[np.all(label_polydatas[l].cell_data['wall'] == tuple(w), axis=-1)]
                        wall_faces = np.concatenate([3*np.ones(len(wall_triangles), int)[:, np.newaxis], wall_triangles], axis=1).flatten()
                        wall_polydatas[tuple(w)] = pv.PolyData(label_polydatas[l].points, faces=wall_faces)
                        wall_polydatas[tuple(w)].cell_data['wall'] = [tuple(w) for _ in wall_triangles]

                smoothing = 5
                target_edge_length = 1.
                decimation = np.power(target_edge_length / np.mean(voxelsize), 2)/2
                print(decimation)

                smooth_wall_polydatas = {}
                for i, w in enumerate(tqdm(wall_polydatas.keys(), unit="wall")):
                    smooth_polydata = _vtk_smooth_polydata(wall_polydatas[w], smoothing, decimation)
                    smooth_wall_polydatas[w] = pv.PolyData(smooth_polydata)
                    smooth_wall_polydatas[w].cell_data['wall'] = [w for _ in range(smooth_wall_polydatas[w].n_faces)]
                    smooth_wall_polydatas[w].cell_data['wall_label'] = [i%256 for _ in range(smooth_wall_polydatas[w].n_faces)]

                self.all_polydatas = smooth_wall_polydatas
                self._current_resolution = self["resolution"]

            wall_labels = tissue.wall_ids()
            wall_labels = [w for w in wall_labels if w in self.all_polydatas]
            if not np.all([l in tissue.walls.feature('geometric_median') for l in wall_labels]):
                wall_centers = tissue.walls.geometric_median()
                tissue.walls.set_feature('center', wall_centers)
                for k, dim in enumerate(['x','y','z']):
                    tissue.walls.set_feature('center_'+dim, {w: p[k] for w, p in wall_centers.items()})
            
            if not self['outer_walls']:
                wall_labels = [w for w in wall_labels if not 1 in w]
            wall_labels = [w for w in wall_labels if tissue.walls.feature('geometric_median')[w][0] > self["x_range"][0]]
            wall_labels = [w for w in wall_labels if tissue.walls.feature('geometric_median')[w][0] < self["x_range"][1]]
            wall_labels = [w for w in wall_labels if tissue.walls.feature('geometric_median')[w][1] > self["y_range"][0]]
            wall_labels = [w for w in wall_labels if tissue.walls.feature('geometric_median')[w][1] < self["y_range"][1]]
            wall_labels = [w for w in wall_labels if tissue.walls.feature('geometric_median')[w][2] > self["z_range"][0]]
            wall_labels = [w for w in wall_labels if tissue.walls.feature('geometric_median')[w][2] < self["z_range"][1]]


            if self._current_filter_name != self['filter_name']:
                self._update_filter_range()
                self._current_filter_name = self['filter_name']

            if self['filter_name'] in tissue.cells.feature_names():
                cell_filter = tissue.cells.feature(self['filter_name'])
                if 1 not in cell_filter:
                    cell_filter[1] = 0
                cell_labels = [l for l in cell_filter]
                cell_labels = [l for l in cell_labels if cell_filter[l] >= self["filter_range"][0]]
                cell_labels = [l for l in cell_labels if cell_filter[l] <= self["filter_range"][1]]
                if self['filter_type'] == 'all':
                    wall_labels = [w for w in wall_labels if all([c in cell_labels for c in w])]
                elif self['filter_type'] == 'any':
                    wall_labels = [w for w in wall_labels if any([c in cell_labels for c in w])]

            property_name = self['property_name']
            if property_name in tissue.walls.feature_names():
                wall_labels = [l for l in wall_labels if l in tissue.walls.feature(self['property_name'])]
                wall_property = tissue.walls.feature(self['property_name'])
                if np.array(list(wall_property.values())).ndim > 1:
                    # TODO: create an appropriate glyph polydata in case of 3D vectors / tensors
                    wall_property = {w: np.linalg.norm(p) for w, p in wall_property.items()}
                if property_name in ['label', 'ancestor']:
                    wall_property = {w: p%256 for w, p in wall_property.items()}
            else:
                wall_property = {w: w[0]%256 for w in wall_labels}
                property_name = 'label'

            if self._current_property_name != property_name:
                self._update_value_range()
                self._current_property_name = property_name

            display_polydatas = {}
            for w in wall_labels:
                display_polydata = vtk_expanded_polydata(self.all_polydatas[w], factor=self["size_factor"])
                display_polydatas[w] = pv.PolyData(display_polydata)
                if wall_property:
                    display_polydatas[w].cell_data[property_name] = [wall_property[w] for _ in range(display_polydatas[w].n_faces)]

            self.wall_polydata3D = vtk_combine_polydatas([display_polydatas[w] for w in wall_labels])
            self.wall_polydata3D.GetCellData().SetActiveScalars(property_name)

            if self.wall_actor3D is None:
                self.wall_actor3D = vtk_actor(self.wall_polydata3D,
                                                colormap=self._parameters['colormap'].name(),
                                                value_range=self["value_range"],
                                                opacity=self["alpha"])
                self.renderer3D.AddActor(self.wall_actor3D)
            else:
                self.wall_actor3D.GetMapper().SetInputData(self.wall_polydata3D)

            bounds = self.wall_polydata3D.GetBounds()
            self.vtkView().setBounds(*bounds)

            if self.wall_polydata2D is None:
                self.wall_polydata2D = vtk.vtkPolyData()

            if self.wall_actor2D is None:
                self.wall_actor2D = vtk_actor(self.wall_polydata2D,
                                              colormap=self._parameters['colormap'].name(),
                                              value_range=self["value_range"],
                                              opacity=self["alpha"])
                self.renderer2D.AddActor(self.wall_actor2D)
            else:
                self.wall_actor2D.GetMapper().SetInputData(self.wall_polydata2D)

            self._update_lookuptable()

            self.onSliceChanged(self.slice_position)
            self.render()

    def render(self):
        self.view().render()

    def onTimeChanged(self, value):
        if value in self.tissue_image.keys():
            time_changed =  value != self.current_time
            self.current_time = value
            self._update_xyz_ranges()

            if time_changed:
                self.all_polydatas = None
                self.update()
        self.render()

    def on3D(self):
        self.is2D = False
        self.interactor = self.vtkView().interactor()
        self.render()

    def on2D(self):
        self.is2D = True
        self.interactor = self.vtkView().interactor()
        self.render()

    def onXY(self):
        self.render()

    def onYZ(self):
        self.render()

    def onXZ(self):
        self.render()

    def onSliceOrientationChanged(self, value):
        self.slice_orientation = value

    def onSliceChanged(self, value):
        if value != self.slice_position:
            self.slice_position = value
            if self.wall_polydata3D is not None:
                self.wall_polydata2D = vtk_slice_polydata(self.wall_polydata3D,
                                                          axis=orientation_axes[self.slice_orientation],
                                                          position=self.slice_position,
                                                          width=self['slice_thickness'])
                if isinstance(self.wall_polydata2D, vtk.vtkPolyData) and (self.wall_actor2D is not None):
                    self.wall_actor2D.GetMapper().SetInputData(self.wall_polydata2D)

        self.render()
