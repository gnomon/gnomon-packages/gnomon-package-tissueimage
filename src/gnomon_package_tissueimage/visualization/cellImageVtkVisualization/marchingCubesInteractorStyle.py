import vtk
import pyvista as pv

from PySide6.QtCore import Qt, Signal, QObject
from gnomon.visualization import gnomonInteractorStyle, gnomonVtkView

from gnomon.utils import DEBUG


class marchingCubesInteractorStyle(gnomonInteractorStyle, QObject):
    pickedCell = Signal(int)

    def __init__(self):
        super().__init__()
        QObject.__init__(self)
        
        self.visu = None
        self.renderer = None

    def __del__(self):
        self.disable()

    def disable(self):
        if self.visu:
            self.visu._update_rendering(force=True)
        super().disable() # disconnect view

    def setView(self, view):
        super().setView(view)
        if self.visu:
            if self.visu.cell_actor3D is not None:
                self.visu.cell_actor3D.mapper.lookup_table = pv.LookupTable(cmap=['w', 'w'])
            if self.visu.cell_actor2D is not None:
                self.visu.cell_actor2D.mapper.lookup_table = pv.LookupTable(cmap=['w', 'w'])
        view.render()

    def setVisualization(self, visu):
        self.visu = visu

    def description(self):
        return "Marching Cubes Picker"

    def on3D(self):
        self.renderer = self.visu.renderer3D

    def on2D(self):
        self.renderer = self.visu.renderer2D

    def OnLeftButtonDown(self):
        self.picked_cell_down = -1

        if self.visu is not None:
            picker = vtk.vtkCellPicker()
            picker.SetTolerance(0.005)
            picker.PickFromListOn()
            picker.InitializePickList()
            if self.visu.cell_actor3D is not None:
                picker.AddPickList(self.visu.cell_actor3D)
            if self.visu.cell_actor2D is not None:
                picker.AddPickList(self.visu.cell_actor2D)

            interactor = self.visu.vtkView().interactor()
            clickPos = interactor.GetEventPosition()
            if DEBUG:
                print("click on ", clickPos)
                print("pickposition ", picker.GetPickPosition())
            picker.Pick(clickPos[0], clickPos[1], 0, self.renderer)
            self.picked_cell_down = picker.GetCellId()

        super().OnLeftButtonDown()

    def OnMouseMove(self):
        self.picked_cell_down = -1
        super().OnMouseMove()

    def OnLeftButtonUp(self):
        if self.visu is not None:
            picker = vtk.vtkCellPicker()
            picker.SetTolerance(0.005)
            picker.PickFromListOn()
            picker.InitializePickList()
            if self.visu.cell_actor3D is not None:
                picker.AddPickList(self.visu.cell_actor3D)
            if self.visu.cell_actor2D is not None:
                picker.AddPickList(self.visu.cell_actor2D)

            interactor = self.visu.vtkView().interactor()
            clickPos = interactor.GetEventPosition()
            picker.Pick(clickPos[0], clickPos[1], 0, self.renderer)
            picked_cell_up = picker.GetCellId()

            if self.picked_cell_down == picked_cell_up and picked_cell_up >=0:
                self.pickedCell.emit(picked_cell_up)

        super().OnLeftButtonUp()
