# import gnomonwidgets
import gnomon.visualization
import numpy as np
import vtk

from dtkcore import array_real_2
from dtkcore import d_inliststring
from dtkcore import d_int
from dtkcore import d_range_real
from dtkcore import d_real

from gnomon.utils import visualizationPlugin
from gnomon.utils.decorators import imageInput
from gnomon.visualization import gnomonAbstractImageVtkVisualization

from visu_core.vtk.utils.image_tools import image_to_surface_intensity_polydata
from visu_core.vtk.utils.matplotlib_tools import vtk_lookuptable_from_mpl_cmap
from visu_core.vtk.utils.polydata_tools import vtk_slice_polydata

orientation_axes = {0:'x',1:'y',2:'z'}


@visualizationPlugin(version="1.0.0", coreversion="1.0.0", name="Surface Visualization")
@imageInput("img", data_plugin="gnomonImageDataMultiChannelImage")
class gnomonImageVtkVisualizationSurface(gnomonAbstractImageVtkVisualization):
    def __init__(self):
        super().__init__()

        self._parameters = {}
        self._parameters['channel'] = d_inliststring("Channel", "", [""], "Channel to display")
        self._parameters['cell_radius'] = d_real("Cell radius", 3., 0., 5., 1, "Radius for the projection")
        self._parameters['gaussian_sigma'] = d_real("Gaussian sigma", 0.5, 0., 50., 2, "Standard deviation of the Gaussian kernel used for surface extraction")
        self._parameters['threshold'] = d_int("Threshold", 10, 1, 255, "Intensity threshold used for surface extraction")
        #self._parameters['orientation'] = d_inliststring("Orientation",, "upright", ["upright", "inverted"], "Whether the image was acquired with an upright or inverted microscope")
        self._parameters['colormap'] = gnomon.visualization.ParameterColorMap('colormap', 'gray', "Colormap for the image signal intensity")
        self._parameters['intensity_range'] = d_range_real("Intensity range", array_real_2([0., 255.]), 0., 255., "Range used to map intensities on the colormap")
        self._parameters['opacity'] = d_real("Opacity", 1, 0, 1, "Opacity of the surface")

        self._parameter_groups = {}
        for parameter_name in ['colormap', 'intensity_range', 'opacity']:
            self._parameter_groups[parameter_name] = 'rendering'
        for parameter_name in ['gaussian_sigma', 'threshold']:
            self._parameter_groups[parameter_name] = 'surface_generation'
        for parameter_name in ['cell_radius']:
            self._parameter_groups[parameter_name] = 'signal_projection'

        self.img = {}
        self.current_img = None
        self.current_time = None

        self.actor3D = None
        self.actor2D = None

        self.slice_orientation = 2
        self.slice_position = 0

        self.renderer3D = None
        self.renderer2D = None
        self.interactor = None

    def __del__(self):
        if self.actor3D is not None:
            del self.polydata
            del self.actor3D
        if self.actor2D is not None:
            del self.actor2D

    def pluginName(self):
        return "gnomonImageVtkVisualizationSurface"

    def clear(self):
        if self.vtkView():
            if self.vtkView():
                self.renderer3D = self.vtkView().renderer3D()
                self.renderer2D = self.vtkView().renderer2D()
                if self.actor3D is not None:
                    self.renderer3D.RemoveActor(self.actor3D)
                if self.actor2D is not None:
                    self.renderer2D.RemoveActor(self.actor2D)

    def fill(self):
        if self.vtkView():
            self.renderer3D = self.vtkView().renderer3D()
            self.renderer2D = self.vtkView().renderer2D()
            if self.actor3D is not None:
                self.renderer3D.AddActor(self.actor3D)
            if self.actor2D is not None:
                self.renderer2D.AddActor(self.actor2D)

    def setVisible(self, visible):
        if self.actor3D is not None:
            self.actor3D.SetVisibility(visible)
        if self.actor2D is not None:
            self.actor2D.SetVisibility(visible)

    def refreshParameters(self):
        self.current_img = None

        img = list(self.img.values())[0]

        if (len(img.channel_names) == 1) and ('channel' in self._parameters.keys()):
            del self._parameters['channel']
        else:
            if 'channel' not in self._parameters.keys():
                self._parameters['channel'] = d_inliststring("Channel", "", [""], "Channel to display")
            channel_name = self['channel']
            self._parameters['channel'].setValues(img.channel_names)
            if channel_name in img.channel_names:
                self._parameters['channel'].setValue(channel_name)
            else:
                self._parameters['channel'].setValue(img.channel_names[0])

        if img.dtype == np.uint8:
            self._parameters['threshold'].setMax(255)
            self._parameters['threshold'].setValue(10)
            self._parameters['intensity_range'].setMax(255)
            self._parameters['intensity_range'].setValueMin(0)
            self._parameters['intensity_range'].setValueMax(255)
        elif img.dtype == np.uint16:
            self._parameters['threshold'].setMax(65535)
            self._parameters['threshold'].setValue(1000)
            self._parameters['intensity_range'].setMax(65535)
            self._parameters['intensity_range'].setValueMin(0)
            self._parameters['intensity_range'].setValueMax(65535)

    def imageRendering(self):
        if self.actor3D is not None:
            self.updateOffscreenRenderer(*self.polydata.GetBounds())
            self.offscreenRenderer().AddActor(self.actor3D)
        return self.offscreenImageRendering()

    def update(self):
        if self.current_time is None:
            self.current_time = list(self.img.keys())[0]
        self.current_img = self.img[self.current_time]

        self.renderer3D = self.vtkView().renderer3D()
        self.renderer2D = self.vtkView().renderer2D()

        if 'channel' in self._parameters.keys():
            img = self.current_img.get_channel(self['channel'])
        else:
            img = self.current_img.get_channel(self.current_img.channel_names[0])

        self.polydata = image_to_surface_intensity_polydata(img.transpose(),
                                                            subsampling=None,
                                                            threshold=self['threshold'],
                                                            gaussian_sigma=self['gaussian_sigma'],
                                                            cell_radius=self['cell_radius'])

        lut = vtk_lookuptable_from_mpl_cmap(self._parameters['colormap'].name(), self['intensity_range'])

        mapper3D = vtk.vtkPolyDataMapper()
        mapper3D.SetScalarModeToUsePointData()
        mapper3D.SetInputData(self.polydata)
        mapper3D.SetLookupTable(lut)

        if self.actor3D is None:
            self.actor3D = vtk.vtkActor()
            self.renderer3D.AddActor(self.actor3D)
        self.actor3D.SetMapper(mapper3D)
        self.actor3D.GetProperty().SetOpacity(self['opacity'])

        mapper2D = vtk.vtkPolyDataMapper()
        mapper2D.SetScalarModeToUsePointData()
        mapper2D.SetInputData(vtk.vtkPolyData())
        mapper2D.SetLookupTable(lut)

        if self.actor2D is None:
            self.actor2D = vtk.vtkActor()
            self.renderer2D.AddActor(self.actor2D)
        self.actor2D.SetMapper(mapper2D)
        self.actor2D.GetProperty().SetOpacity(self['opacity'])

        self.render()

    def render(self):
        self.view().render()

    def onTimeChanged(self, value):
        if value in self.img.keys():
            self.current_img = self.img[value]
            self.update()
        self.render()

    def on3D(self):
        self.render()

    def on2D(self):
        self.render()

    def onXY(self):
        self.render()

    def onYZ(self):
        self.render()

    def onXZ(self):
        self.render()

    def onSliceOrientationChanged(self, value):
        self.slice_orientation = value

    def onSliceChanged(self, value):
        self.slice_position = value
        polydata2D = vtk_slice_polydata(self.polydata,
                                        axis=orientation_axes[self.slice_orientation],
                                        position=self.slice_position,
                                        width=1)
        self.actor2D.GetMapper().SetInputData(polydata2D)
        self.render()
