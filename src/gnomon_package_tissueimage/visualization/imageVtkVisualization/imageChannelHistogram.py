import numpy as np

import matplotlib as mpl
from matplotlib.cm import ScalarMappable
from matplotlib.colors import Normalize

import vtk
from vtk.util.numpy_support import numpy_to_vtk, vtk_to_numpy, get_vtk_array_type

from skimage.exposure import histogram

from dtkcore import d_bool, d_inliststring, d_real

import gnomon.visualization
from gnomon.utils import visualizationPlugin
from gnomon.utils.decorators import imageInput
from gnomon.visualization import gnomonAbstractImageVtkVisualization, ParameterColorMap

from timagetk.algorithms.trsf import apply_trsf, create_trsf

from visu_core.vtk.utils.image_tools import vtk_image_data_from_image
from visu_core.vtk.utils.matplotlib_tools import vtk_lookuptable_from_mpl_cmap, vtk_lut_from_mpl_cmap
from visu_core.vtk.utils.polydata_tools import vtk_slice_polydata
from visu_core.matplotlib.colormap import multicolor_gradient_colormap

orientation_axes = {0:'x',1:'y',2:'z'}


@visualizationPlugin(version="1.0.0", coreversion="1.0.0", name="Channel Histogram")
@imageInput("img", data_plugin="gnomonImageDataMultiChannelImage")
class imageChannelHistogram(gnomonAbstractImageVtkVisualization):
    def __init__(self):
        super().__init__()

        self._parameters = {
            'channel' : d_inliststring("Channel", "", [""], "Channel to display"),
            'colormap' : ParameterColorMap('colormap', 'gray', "Colormap for the image signal intensity"),
            'brightness': d_real("Brightness", 0., -1, 1, 2, "Brightness"),
            'contrast': d_real("Contrast", 0., -1, 1, 2, "Contrast"),
            'opacity': d_real("Opacity", 1, 0, 1, 2, "Transparency value for the image rendering"),
            'show_histogram': d_bool("Show histogram", True, "Whether to display the image intensity histogram")
        }

        self._parameter_groups = {}
        for parameter_name in ['brightness', 'contrast']:
            self._parameter_groups[parameter_name] = 'adjustments'
        for parameter_name in ['colormap', 'opacity']:
            self._parameter_groups[parameter_name] = 'rendering'

        self.img = {}
        self.current_img = None
        self.current_time = None

        self.image_changed = True

        self.slice_orientation = 2
        self.slice_position = 0

        self.volume_mapper = None
        self.volume_property = None
        self.volume = None

        self.image_data = None

        self.image_planes = None

        self.histo_data = None
        self.histogram_representation = None
        self.histogram_widget = None

        self.renderer3D = None
        self.renderer2D = None
        self.interactor = None

        self.is2D = False
        self._visible = True
        self._current_channel = None

    def __del__(self):
        if self.volume is not None:
            del self.image_data
            del self.volume
            self.volume = None
        if self.image_planes is not None:
            for i, dim in enumerate("xyz"):
                del self.image_planes[i]
            self.image_planes = None

    def pluginName(self):
        return "imageChannelHistogram"

    def clear(self):
        if self.vtkView():
            self.renderer3D = self.vtkView().renderer3D()
            if self.volume is not None:
                self.renderer3D.RemoveActor(self.volume)
            if self.histogram_widget is not None:
                self.histogram_widget.Off()
            if self.image_planes is not None:
                for i, dim in enumerate("xyz"):
                    self.image_planes[i].Off()

    def fill(self):
        if self.vtkView():
            self.renderer3D = self.vtkView().renderer3D()
            self.interactor = self.vtkView().interactor()
            if self.volume is not None:
                self.renderer3D.AddActor(self.volume)
            if self.histogram_widget is not None:
                self.histogram_widget.SetInteractor(self.interactor)
                if self._visible and self['show_histogram']:
                    self.histogram_widget.On()
            if self.image_planes is not None:
                for i, dim in enumerate("xyz"):
                    self.image_planes[i].SetInteractor(self.interactor)
                    if self._visible and self.is2D:
                        self.image_planes[i].On()

    def setVisible(self, visible):
        self._visible = visible
        if self.volume is not None:
            self.volume.SetVisibility(visible)
        if self.image_planes is not None:
            for i, dim in enumerate("xyz"):
                if visible and self.is2D:
                    self.image_planes[i].On()
                else:
                    self.image_planes[i].Off()
        if self.histogram_widget is not None:
            if visible and self['show_histogram']:
                self.histogram_widget.On()
            else:
                self.histogram_widget.Off()

    def refreshParameters(self):
        self.current_img = None

        img = list(self.img.values())[0]

        if (len(img.channel_names) == 1) and ('channel' in self._parameters.keys()):
            del self._parameters['channel']
        else:
            if 'channel' not in self._parameters.keys():
                self._parameters['channel'] = d_inliststring("Channel", "", [""], "Channel to display")
            channel_name = self['channel']
            self._parameters['channel'].setValues(img.channel_names)
            if channel_name in img.channel_names:
                self._parameters['channel'].setValue(channel_name)
            else:
                self._parameters['channel'].setValue(img.channel_names[0])

        if 'channel' in self._parameters.keys():
            channel_name = self['channel']
        else:
            channel_name = img.channel_names[0]
        self.update_brightness_contrast(img.get_channel(channel_name))

        self._current_channel = None

    def update_brightness_contrast(self, img):
        vmin = 0
        vmax = 255 if img.dtype==np.uint8 else 65535
        img_min = float(np.min(img.get_array()))
        img_max = float(np.max(img.get_array()))
        img_midpoint = (img_max + img_min)/2
        img_radius = (img_max - img_min)/2
        self._parameters['brightness'].setValue(1 - 2*(img_midpoint-vmin)/(vmax - vmin))
        self._parameters['contrast'].setValue(1 - 2*img_radius/(vmax - vmin))

    def imageRendering(self):
        if self.volume is not None:
            self.updateOffscreenRenderer(*self.image_data.GetBounds())
            self.volume.Update()
            self.offscreenRenderer().AddActor(self.volume)
        q_image = self.offscreenImageRendering()
        if self.volume is not None:
            self.offscreenRenderer().RemoveActor(self.volume)
            self.volume.Update()
        return q_image

    def update(self):
        self.renderer3D = self.vtkView().renderer3D()
        self.renderer2D = self.vtkView().renderer2D()

        self.current_time = self.view().currentTime()
        if self.current_time in self.img:
            self.current_img = self.img[self.current_time]

            if 'channel' in self._parameters.keys():
                channel_name = self['channel']
            else:
                channel_name = self.current_img.channel_names[0]
            self.image_changed = self.image_changed or (self._current_channel != channel_name)
            self._current_channel = channel_name
            img = self.current_img.get_channel(channel_name)

            if self.image_changed:
                self.update_brightness_contrast(img)

                if self.image_data is None:
                    self.image_data = vtk.vtkImageData()
                image_array = numpy_to_vtk(img.get_array().ravel(),
                                           array_type=get_vtk_array_type(img.dtype),
                                           deep=True)
                self.image_data.SetDimensions(img.shape[::-1])
                self.image_data.SetSpacing(img.voxelsize[::-1])
                self.image_data.SetOrigin(img.origin[::-1])
                self.image_data.GetPointData().SetScalars(image_array)

            colormap = self._parameters['colormap'].name()

            vmin = 0
            vmax = 255 if img.dtype==np.uint8 else 65535
            range_mid_point = vmin + (1-self['brightness'])*(vmax - vmin)/2
            range_radius = (1-self['contrast'])*(vmax - vmin)/2
            value_range = (range_mid_point - range_radius, range_mid_point + range_radius)

            alpha = vtk.vtkPiecewiseFunction()
            alpha.RemoveAllPoints()
            alpha.ClampingOn()
            alpha.AddPoint(value_range[0], 0.)
            alpha.AddPoint(value_range[1], self['opacity'])

            lut = vtk_lookuptable_from_mpl_cmap(colormap, value_range)

            if self.volume_property is None:
                self.volume_property = vtk.vtkVolumeProperty()
            self.volume_property.SetColor(lut)
            self.volume_property.SetScalarOpacity(alpha)
            self.volume_property.ShadeOff()
            self.volume_property.SetInterpolationTypeToNearest()

            if self.volume_mapper is None:
                self.volume_mapper = vtk.vtkSmartVolumeMapper()
            self.volume_mapper.SetInputData(self.image_data)
            self.volume_mapper.Modified()
            self.volume_mapper.SetRequestedRenderModeToDefault()

            if self.volume is None:
                self.volume = vtk.vtkVolume()
                self.renderer3D.AddActor(self.volume)
            self.volume.SetMapper(self.volume_mapper)
            self.volume.SetProperty(self.volume_property)

            if self.image_changed:
                self.histo_data, _ = histogram(img, source_range='dtype')
                if img.dtype==np.uint16:
                    self.histo_data = self.histo_data.reshape(256,-1).sum(axis=1)
                self.histo_data = np.sqrt(self.histo_data)

            histo_image = np.zeros((len(self.histo_data), len(self.histo_data), 4), np.uint8)
            histo_height = np.ceil((self.histo_data*histo_image.shape[1]) / np.max(self.histo_data)).astype(int)

            norm = Normalize(vmin=value_range[0], vmax=value_range[1])
            bins = np.arange(len(histo_image))
            if img.dtype==np.uint16:
                bins *= 256
            histo_colors = ScalarMappable(cmap=colormap, norm=norm).to_rgba(bins)
            histo_colors = (histo_colors*255).astype(np.uint8)
            for i, (j, col) in enumerate(zip(histo_height, histo_colors)):
                histo_image[:j, i] = col[:4]
            i_min = int(np.round((histo_image.shape[1]*value_range[0])/vmax))
            if i_min >= 0:
                histo_image[:, i_min] = [255, 0, 0, 255]
            i_max = int(np.round((histo_image.shape[1]*value_range[1])/vmax))
            if i_max < histo_image.shape[1]:
                histo_image[:, i_max] = [255, 0, 0, 255]

            histogram_image_data = vtk.vtkImageData()
            histogram_image_array = numpy_to_vtk(histo_image.reshape(-1, 4),
                                                 array_type=get_vtk_array_type(histo_image.dtype),
                                                 deep=True)
            histogram_image_data.SetDimensions(histo_image.shape[0], histo_image.shape[1], 1)
            histogram_image_data.SetOrigin(0, 0, 0)
            histogram_image_data.SetSpacing(1, 1, 1)
            histogram_image_data.SetExtent(0, histo_image.shape[0]-1, 0, histo_image.shape[1]-1, 0, 0)
            histogram_image_data.GetPointData().SetScalars(histogram_image_array)

            if self.histogram_representation is None:
                self.histogram_representation = vtk.vtkLogoRepresentation()
                self.histogram_representation.SetShowBorderToOn()
                self.histogram_representation.SetPosition(0.05, 0.05)
                self.histogram_representation.SetPosition2(0.2, 0.2)
                self.histogram_representation.GetImageProperty().SetOpacity(1.)
            self.histogram_representation.SetImage(histogram_image_data)
            self.histogram_representation.SetMinimumSize(histo_image.shape[0], histo_image.shape[1])

            if self.histogram_widget is None:
                self.histogram_widget = vtk.vtkLogoWidget()
                self.histogram_widget.SetInteractor(self.vtkView().interactor())
                self.histogram_widget.On()
                self.histogram_widget.SelectableOff()
            self.histogram_widget.SetRepresentation(self.histogram_representation)
            if not self['show_histogram']:
                self.histogram_widget.Off()

            self.vtkView().resetCamera()

            _lut = vtk_lut_from_mpl_cmap(colormap, value_range, n_values=256, alpha_mode='constant')

            if self.image_planes is None:
                self.image_planes = {}

                for i, dim in enumerate("xyz"):
                    plane = vtk.vtkImagePlaneWidget()
                    plane.SetInputData(self.image_data)
                    plane.SetPlaneOrientation(i)
                    #plane.RestrictPlaneToVolumeOn()
                    plane.SetSliceIndex(0)
                    plane.SetInteractor(self.vtkView().interactor())
                    self.image_planes[i] = plane

            for i, dim in enumerate("xyz"):
                if self.image_changed:
                    slice_index = self.image_planes[i].GetSliceIndex()
                    self.image_planes[i].SetInputData(self.image_data)
                    self.image_planes[i].SetSliceIndex(slice_index)
                self.image_planes[i].UserControlledLookupTableOn()
                self.image_planes[i].SetLookupTable(_lut)
                self.image_planes[i].TextureInterpolateOff()

            self.image_changed = False

    def render(self):
        self.view().render()

    def onTimeChanged(self, value):
        if value in self.img.keys():
            if self.current_time != value:
                self.current_time = value
                self.image_changed = True
            self.update()
        self.render()

    def on3D(self):
        self.is2D = False
        if self.histogram_widget is not None:
            self.histogram_widget.Off()
            self.histogram_widget.SetInteractor(self.vtkView().interactor())
            if self._visible and self['show_histogram']:
                self.histogram_widget.On()
        if self.image_planes is not None:
            for i, dim in enumerate("xyz"):
                self.image_planes[i].Off()
        self.render()

    def on2D(self):
        self.is2D = True
        if self.histogram_widget is not None:
            self.histogram_widget.Off()
            self.histogram_widget.SetInteractor(self.vtkView().interactor())
            if self._visible and self['show_histogram']:
                self.histogram_widget.On()
        if self.image_planes is not None:
            if self._visible:
                for i, dim in enumerate("xyz"):
                    self.image_planes[i].SetInteractor(self.vtkView().interactor())
                    self.image_planes[i].On()
        self.vtkView().resetCamera()
        self.render()

    def onXY(self):
        self.render()

    def onYZ(self):
        self.render()

    def onXZ(self):
        self.render()

    def onSliceOrientationChanged(self, value):
        self.slice_orientation = value
        self.vtkView().resetCamera()

    def onSliceChanged(self, value):
        self.slice_position = value
        if self.image_planes is not None:
            i = self.slice_orientation
            pos = int(np.round(self.slice_position / self.image_data.GetSpacing()[i]))
            self.image_planes[i].SetInteractor(self.vtkView().interactor())
            self.image_planes[i].SetSliceIndex(pos)
        self.render()
