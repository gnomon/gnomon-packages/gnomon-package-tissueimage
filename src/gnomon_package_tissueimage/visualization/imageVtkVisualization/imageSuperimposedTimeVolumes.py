import numpy as np
from matplotlib.colors import _colors_full_map

import vtk
from vtk.util.numpy_support import numpy_to_vtk, vtk_to_numpy, get_vtk_array_type

from dtkcore import array_real_2
from dtkcore import d_inliststring
from dtkcore import d_int
from dtkcore import d_range_real
from dtkcore import d_real

import gnomon.visualization
from gnomon.utils import visualizationPlugin
from gnomon.utils.decorators import imageInput
from gnomon.visualization import gnomonAbstractImageVtkVisualization

from timagetk.algorithms.trsf import apply_trsf, create_trsf

from visu_core.vtk.utils.image_tools import vtk_image_data_from_image
from visu_core.vtk.utils.matplotlib_tools import vtk_lookuptable_from_mpl_cmap
from visu_core.vtk.utils.polydata_tools import vtk_slice_polydata
from visu_core.matplotlib.colormap import multicolor_gradient_colormap

orientation_axes = {0:'x',1:'y',2:'z'}
named_colors = [c for c in _colors_full_map.keys() if
                len(c) > 1 and not (("xkcd:" in c) or ("tab:" in c) or ("glasbey" in c))]


@visualizationPlugin(version="1.0.0", coreversion="1.0.0", name="Superimposed Times")
@imageInput("img", data_plugin="gnomonImageDataMultiChannelImage")
class imageSuperimposedTimeVolumes(gnomonAbstractImageVtkVisualization):
    def __init__(self):
        super().__init__()


        self._parameters = {
            'channel' : d_inliststring("Channel", "", [""], "Channel to display"),
            'color': d_inliststring("Color", "lawngreen", named_colors, "Color to apply to the current image frame"),
            'next_color': d_inliststring("Next Color", "magenta", named_colors, "Color to apply to the next image frame"),
            'opacity': d_real("Opacity", 1, 0, 1, 2, "Transparency value for the image rendering"),
            'intensity_range' : d_range_real("Intensity range", array_real_2([0., 255.]), 0., 255., "Range used to map intensities on the colormap")
        }

        self._parameter_groups = {}
        for parameter_name in ['color', 'next_color', 'intensity_range', 'opacity']:
            self._parameter_groups[parameter_name] = 'rendering'

        self.img = {}
        self.current_img = None
        self.current_time = None

        self.next_img = None
        self.next_time = None

        self.slice_orientation = 2
        self.slice_position = 0

        self.volume_mapper = None
        self.volume_property = None

        self.image_data = None
        self.next_image_data = None

        self.volume = None
        self.image_colors = None
        self.next_image_colors = None

        self.image_planes = None

        self.renderer3D = None
        self.renderer2D = None
        self.interactor = None

    def __del__(self):
        if self.volume is not None:
            del self.image_data
            del self.volume
            self.volume = None
        if self.image_planes is not None:
            for i, dim in enumerate("xyz"):
                del self.image_planes[i]
            self.image_planes = None

    def pluginName(self):
        return "imageSuperimposedTimeVolumes"

    def clear(self):
        if self.vtkView():
            self.renderer3D = self.vtkView().renderer3D()
            self.renderer2D = self.vtkView().renderer2D()
            if self.volume is not None:
                self.renderer3D.RemoveActor(self.volume)
            if self.image_planes is not None:
                for i, dim in enumerate("xyz"):
                    self.renderer2D.RemoveActor(self.image_planes[i])

    def fill(self):
        if self.vtkView():
            self.renderer3D = self.vtkView().renderer3D()
            self.renderer2D = self.vtkView().renderer2D()
            if self.volume is not None:
                self.renderer3D.AddActor(self.volume)
            if self.image_planes is not None:
                for i, dim in enumerate("xyz"):
                    self.renderer2D.AddActor(self.image_planes[i])


    def setVisible(self, visible):
        if self.volume is not None:
            self.volume.SetVisibility(visible)
        for i, dim in enumerate("xyz"):
            self.image_planes[i].SetVisibility(visible)

    def refreshParameters(self):
        self.current_img = None

        img = list(self.img.values())[0]

        if (len(img.channel_names) == 1) and ('channel' in self._parameters.keys()):
            del self._parameters['channel']
        else:
            if 'channel' not in self._parameters.keys():
                self._parameters['channel'] = d_inliststring("Channel", "", [""], "Channel to display")
            channel_name = self['channel']
            self._parameters['channel'].setValues(img.channel_names)
            if channel_name in img.channel_names:
                self._parameters['channel'].setValue(channel_name)
            else:
                self._parameters['channel'].setValue(img.channel_names[0])

        if img.dtype == np.uint8:
            self._parameters['intensity_range'].setMin(0)
            self._parameters['intensity_range'].setMax(255)
        elif img.dtype == np.uint16:
            self._parameters['intensity_range'].setMin(0)
            self._parameters['intensity_range'].setMax(65535)

        if 'channel' in self._parameters.keys():
            channel_name = self['channel']
        else:
            channel_name = img.channel_names[0]
        self._parameters['intensity_range'].setValueMin(int(np.min(img.get_channel(channel_name))))
        self._parameters['intensity_range'].setValueMax(int(np.max(img.get_channel(channel_name))))

    def imageRendering(self):
        if self.volume is not None:
            self.updateOffscreenRenderer(*self.image_data.GetBounds())
            # self.volume_mapper.SetRequestedRenderModeToRayCast()
            # self.volume_property.ShadeOff()
            self.volume.Update()
            self.offscreenRenderer().AddActor(self.volume)
        q_image = self.offscreenImageRendering()
        if self.volume is not None:
            self.offscreenRenderer().RemoveActor(self.volume)
            # self.volume_mapper.SetRequestedRenderModeToGPU()
            # self.volume_property.ShadeOn()
            self.volume.Update()
        return q_image

    def update(self):
        if self.current_time is None:
            self.current_time = list(self.img.keys())[0]
        self.current_img = self.img[self.current_time]

        current_index = list(self.img.keys()).index(self.current_time)
        next_index = np.minimum(current_index+1, len(self.img)-1)
        self.next_time = list(self.img.keys())[next_index]
        self.next_img = self.img[self.next_time]

        self.renderer3D = self.vtkView().renderer3D()
        self.renderer2D = self.vtkView().renderer2D()

        if 'channel' in self._parameters.keys():
            img = self.current_img.get_channel(self['channel'])
            next_img = self.next_img.get_channel(self['channel'])
        else:
            img = self.current_img.get_channel(self.current_img.channel_names[0])
            next_img = self.next_img.get_channel(self.next_img.channel_names[0])

        trsf = create_trsf(template_img=img)
        next_img = apply_trsf(next_img, trsf, template_img=img)
        print(img.shape, next_img.shape)

        if self.image_data is None:
            self.image_data = vtk.vtkImageData()
        image_array = numpy_to_vtk(img.ravel(),
                                   array_type=get_vtk_array_type(img.dtype),
                                   deep=False)
        self.image_data.SetDimensions(img.shape[::-1])
        self.image_data.SetSpacing(img.voxelsize[::-1])
        self.image_data.SetOrigin(img.origin[::-1])
        self.image_data.GetPointData().SetScalars(image_array)

        if self.next_image_data is None:
            self.next_image_data = vtk.vtkImageData()
        next_image_array = numpy_to_vtk(next_img.get_array().ravel(),
                                   array_type=get_vtk_array_type(next_img.dtype),
                                   deep=True)
        self.next_image_data.SetDimensions(next_img.shape[::-1])
        self.next_image_data.SetSpacing(next_img.voxelsize[::-1])
        self.next_image_data.SetOrigin(next_img.origin[::-1])
        self.next_image_data.GetPointData().SetScalars(next_image_array)

        alpha = vtk.vtkPiecewiseFunction()
        alpha.RemoveAllPoints()
        alpha.ClampingOn()
        alpha.AddPoint(self['intensity_range'][0], 0.)
        alpha.AddPoint(self['intensity_range'][1], self['opacity'])

        n_values = 255

        lut = vtk_lookuptable_from_mpl_cmap(multicolor_gradient_colormap(['k', self['color']]), self['intensity_range'])
        lookup_table = vtk.vtkLookupTable()
        lookup_table.SetNumberOfTableValues(n_values)

        next_lut = vtk_lookuptable_from_mpl_cmap(multicolor_gradient_colormap(['k', self['next_color']]), self['intensity_range'])
        next_lookup_table = vtk.vtkLookupTable()
        next_lookup_table.SetNumberOfTableValues(n_values)

        for i in range(n_values):
            val = (i*self['intensity_range'][1] + (n_values-i)*self['intensity_range'][0])/(n_values-1.)
            a = alpha.GetValue(val)
            rgb = lut.GetColor(val)
            lookup_table.SetTableValue(i, rgb[0], rgb[1], rgb[2], a)
            rgb = next_lut.GetColor(val)
            next_lookup_table.SetTableValue(i, rgb[0], rgb[1], rgb[2], a)
        lookup_table.SetTableRange(self['intensity_range'])
        next_lookup_table.SetTableRange(self['intensity_range'])

        if self.image_colors is None:
            self.image_colors = vtk.vtkImageMapToColors()
        self.image_colors.SetInputData(self.image_data)
        self.image_colors.SetLookupTable(lookup_table)
        self.image_colors.SetOutputFormatToRGBA()
        self.image_colors.Update()

        if self.next_image_colors is None:
            self.next_image_colors = vtk.vtkImageMapToColors()
        self.next_image_colors.SetInputData(self.next_image_data)
        self.next_image_colors.SetLookupTable(next_lookup_table)
        self.next_image_colors.SetOutputFormatToRGBA()
        self.next_image_colors.Update()

        colors = vtk_to_numpy(self.image_colors.GetOutput().GetPointData().GetScalars()).reshape(img.shape+(4,))
        next_colors = vtk_to_numpy(self.next_image_colors.GetOutput().GetPointData().GetScalars()).reshape(next_img.shape+(4,))
        blended_colors = np.max([colors, next_colors], axis=0)

        blended_data = vtk.vtkImageData()
        blended_array = numpy_to_vtk(blended_colors.reshape(np.prod(img.shape),4),
                                        array_type=get_vtk_array_type(blended_colors.dtype),
                                        deep=True)
        blended_data.SetDimensions(img.shape[::-1])
        blended_data.SetSpacing(img.voxelsize[::-1])
        blended_data.SetOrigin(img.origin[::-1])
        blended_data.GetPointData().SetScalars(blended_array)

        if self.volume_property is None:
            self.volume_property = vtk.vtkVolumeProperty()
        self.volume_property.SetColor(lut)
        self.volume_property.SetScalarOpacity(alpha)
        self.volume_property.IndependentComponentsOff()
        self.volume_property.ShadeOff()
        self.volume_property.SetInterpolationTypeToNearest()

        if self.volume_mapper is None:
            self.volume_mapper = vtk.vtkSmartVolumeMapper()
        self.volume_mapper.SetInputData(blended_data)
        self.volume_mapper.SetBlendModeToComposite()
        self.volume_mapper.SetRequestedRenderModeToDefault()
        self.volume_mapper.Modified()
        #self.volume_mapper.SetInputData(self.image_data)
        #self.volume_mapper.SetRequestedRenderModeToRayCast()

        if self.volume is None:
            self.volume = vtk.vtkVolume()
            self.renderer3D.AddActor(self.volume)
        self.volume.SetMapper(self.volume_mapper)
        self.volume.SetProperty(self.volume_property)

        self.vtkView().resetCamera()

        if self.image_planes is None:
            self.image_planes = {}
            extent = self.image_data.GetExtent()
            for i, dim in enumerate("xyz"):
                plane = vtk.vtkImageActor()
                plane.SetInputData(blended_data)
                if i == 0:
                    pos = (extent[0] + extent[1])//2
                    plane.SetDisplayExtent(pos, pos, extent[2], extent[3], extent[4], extent[5])
                elif i == 1:
                    pos = (extent[2] + extent[3])//2
                    plane.SetDisplayExtent(extent[0], extent[1], pos, pos, extent[4], extent[5])
                elif i == 2:
                    pos = (extent[4] + extent[5])//2
                    plane.SetDisplayExtent(extent[0], extent[1], extent[2], extent[3], pos, pos)
                self.image_planes[i] = plane
                self.renderer2D.AddActor(plane)

        for i, dim in enumerate("xyz"):
            self.image_planes[i].SetInputData(blended_data)
            self.image_planes[i].GetProperty().UseLookupTableScalarRangeOn()
            self.image_planes[i].InterpolateOff()
        self.vtkView().resetCamera()

        self.render()

    def render(self):
        self.view().render()

    def onTimeChanged(self, value):
        if value in self.img.keys():
            self.current_time = value
            self.current_img = self.img[value]
            self.update()
        self.render()

    def on3D(self):
        self.render()

    def on2D(self):
        self.render()

    def onXY(self):
        self.render()

    def onYZ(self):
        self.render()

    def onXZ(self):
        self.render()

    def onSliceOrientationChanged(self, value):
        self.slice_orientation = value

    def onSliceChanged(self, value):

        self.slice_position = value
        if self.image_planes is not None:
            i = self.slice_orientation
            pos = int(np.round(self.slice_position / self.image_data.GetSpacing()[i]))
            extent = self.image_data.GetExtent()
            if i == 0:
                self.image_planes[i].SetDisplayExtent(pos, pos, extent[2], extent[3], extent[4], extent[5])
            elif i == 1:
                self.image_planes[i].SetDisplayExtent(extent[0], extent[1], pos, pos, extent[4], extent[5])
            elif i == 2:
                self.image_planes[i].SetDisplayExtent(extent[0], extent[1], extent[2], extent[3], pos, pos)
        self.render()
