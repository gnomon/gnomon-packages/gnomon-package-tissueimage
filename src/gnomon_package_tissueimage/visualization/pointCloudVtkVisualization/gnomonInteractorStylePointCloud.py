import numpy as np
import vtk
from gnomon.visualization import gnomonInteractorStyle
from vtk import vtkTextActor


# from PyQt5.QtCore import Qt


class gnomonInteractorStylePointCloud(gnomonInteractorStyle):
    def __init__(self):

        super().__init__()

        self.renderer = None
        self.textActor2D = None
        self.textActor3D = None

        self.visu = None

        self.vtkId = -1
        self.wispId = 0
        self.click = 0

        self.mode = "3D"
        self.has_selection = False
        self.grab_mode = False
        self.add_new_point = False

        self.picker = vtk.vtkPointPicker()
        self.picker.SetTolerance(0.005)

        self.cell_sphere = vtk.vtkSphereSource()
        self.cell_actor = vtk.vtkActor()

        self.grab_plane = vtk.vtkPlaneSource()
        self.grab_actor = vtk.vtkActor()

        # self.AddObserver("LeftButtonPressEvent", self.leftButtonDownEvent)
        # self.AddObserver("LeftButtonReleaseEvent", self.leftButtonUpEvent)
        # self.AddObserver("MouseMoveEvent", self.mouseMouveEvent)

    def __del__(self):
        self.disable()

    def OnMouseMove(self):
        super().OnMouseMove()
        if self.has_selection and self.grab_mode:
            interactor = super().GetDefaultRenderer().GetRenderWindow().GetInteractor()
            mousePos = interactor.GetEventPosition()
            interactor.GetPicker().Pick(mousePos[0], mousePos[1], 0, super().GetDefaultRenderer())
            mouse_position = np.array(interactor.GetPicker().GetPickPosition())
            sphere_center = np.array(self.cell_sphere.GetCenter())
            mouse_position[self.visu.slice_orientation] = sphere_center[self.visu.slice_orientation]

            self.cell_sphere.SetCenter(*mouse_position)
            self.grab_plane.SetCenter(*mouse_position)
            self.visu.render()
        else:
            self.vtkId = -1
            self.click = 0
            self.updateTextActor()

    def OnLeftButtonDown(self):
        super().OnLeftButtonDown()
        self.click += 1

    def OnLeftButtonUp(self):
        super().OnLeftButtonUp()

        if not self.grab_mode:
            if self.click == 1:
                interactor = super().GetDefaultRenderer().GetRenderWindow().GetInteractor()
                clickPos = interactor.GetEventPosition()
                # picker.UseCellsOn()
                self.picker.Pick(clickPos[0], clickPos[1], 0, super().GetDefaultRenderer())
                self.vtkId = self.picker.GetPointId()
                self.deselectPoint()
                self.updateTextActor()
                # self.updatePane()

            if self.click == 2:
                print("Double click", self.vtkId)
                if self.vtkId > -1:
                    self.updateTextActor()
                    self.selectPoint()
                else:
                    self.deselectPoint()
                self.click = 0
            elif self.vtkId == -1:
                # self.click = 0
                self.deselectPoint()
                self.updateTextActor()

    def OnKeyPress(self):
        super().OnKeyPress()
        interactor = super().GetDefaultRenderer().GetRenderWindow().GetInteractor()
        key = interactor.GetKeySym()
        if self.has_selection:
            pointCloud = self.visu.point_cloud

            if key == 'd':
                if self.wispId in pointCloud.pointIds():
                    pointCloud.removePoint(self.wispId)

                    self.deselectPoint()
                    self.vtkId = -1
                    self.visu.update()
                    self.updateTextActor()

            if self.mode == '2D':
                if self.wispId in pointCloud.pointIds():
                    if key == 'g' and not self.grab_mode:
                        self.grabPoint()
                    elif key == 'a' and not self.grab_mode:
                        self.add_new_point = True
                        self.grabPoint()
                    elif key == 'q' and self.grab_mode:
                        self.releasePoint(save=False)
                    elif key == 's' and self.grab_mode:
                        self.releasePoint(save=True)

    def updateTextActor(self):
        text = self.visu['property']+' : '
        if self.visu.is2D:
            self.renderer = self.visu.renderer2D

            if self.textActor2D is None:
                self.textActor2D = vtkTextActor()
                self.textActor2D.SetPosition2(40,40)
                self.textActor2D.GetTextProperty().SetFontSize(24)
                self.textActor2D.GetTextProperty().SetColor(1,1,1)
                self.renderer.AddActor2D(self.textActor2D)
                self.textActor2D.SetInput(text)

            if self.vtkId > -1:
                value_property = self.cellId()
                # value_property = self.vtkId
                if value_property != "":
                    self.textActor2D.SetInput('        Point : '+str(self.wispId)+', '+text+str(value_property))
                else:
                    self.textActor2D.SetInput('        Point : '+str(self.wispId))
            elif self.vtkId == -1:
                self.textActor2D.SetInput('')
        else:
            self.renderer = self.visu.renderer3D

            if self.textActor3D is None:
                self.textActor3D = vtkTextActor()
                self.textActor3D.SetPosition2(40,40)
                self.textActor3D.GetTextProperty().SetFontSize(24)
                self.textActor3D.GetTextProperty().SetColor(1,1,1)
                self.renderer.AddActor2D(self.textActor3D)
                self.textActor3D.SetInput(text)

            if self.vtkId > -1:
                value_property = self.cellId()
                # value_property = self.vtkId
                if value_property != "":
                    self.textActor3D.SetInput('        Point : '+str(self.wispId)+', '+text+str(value_property))
                else:
                    self.textActor3D.SetInput('        Point : '+str(self.wispId))
            elif self.vtkId == -1:
                self.textActor3D.SetInput('')

        self.visu.render()

    def cellId(self):
        if self.visu.is2D:
            polydata = self.visu.point_actor2D.actor.GetMapper().GetInput()
        else:
            polydata = self.visu.point_actor3D.actor.GetMapper().GetInput()
        wispIds = polydata.GetPointData().GetArray("WispId")
        if self.vtkId < wispIds.GetNumberOfValues():
            self.wispId = wispIds.GetValue(self.vtkId)
            property_name = self.visu['property']
            topomesh = self.visu.topomesh
            if topomesh.has_wisp_property(property_name, 2):
                return topomesh.wisp_property(property_name, 0)[self.wispId]
            else:
                return ""
        else:
            self.wispId = -1
            return ""

    def selectPoint(self):
        topomesh = self.visu.topomesh
        if self.wispId in topomesh.wisps(0):
            center = topomesh.wisp_property('barycenter',0)[self.wispId]

            self.cell_sphere.SetRadius(2.0*self.visu['scale_factor'])
            self.cell_sphere.SetThetaResolution(16)
            self.cell_sphere.SetPhiResolution(16)
            self.cell_sphere.SetCenter(center)

            cell_mapper = vtk.vtkPolyDataMapper()
            cell_mapper.SetInputConnection(self.cell_sphere.GetOutputPort())

            self.cell_actor.SetMapper(cell_mapper)
            self.cell_actor.GetProperty().SetColor(1.0, 1.0, 1.0)
            self.cell_actor.GetProperty().SetOpacity(0.3)
            self.renderer.AddActor(self.cell_actor)

            self.has_selection = True
        self.visu.view().updateShortcutKeys()
        self.visu.render()

    def deselectPoint(self):
        if self.has_selection:
            if self.grab_mode:
                self.releasePoint()
            self.renderer.RemoveActor(self.cell_actor)
            self.has_selection = False
        self.visu.view().updateShortcutKeys()
        self.visu.render()

    def grabPoint(self):
        self.grab_mode = True
        polydata = self.visu.point_actor2D.actor.GetMapper().GetInput()

        e = np.diff(np.array(polydata.GetBounds()).reshape(3, 2), axis=1)
        c = self.cell_sphere.GetCenter()
        if self.visu.slice_orientation == 0:
            self.grab_plane.SetPoint1(c[0], c[1] + e[1], c[2])
            self.grab_plane.SetPoint2(c[0], c[1], c[2] + e[2])
            self.grab_plane.SetNormal(1, 0, 0)
        elif self.visu.slice_orientation == 1:
            self.grab_plane.SetPoint1(c[0] + e[0], c[1], c[2])
            self.grab_plane.SetPoint2(c[0], c[1], c[2] + e[2])
            self.grab_plane.SetNormal(0, 1, 0)
        elif self.visu.slice_orientation == 2:
            self.grab_plane.SetPoint1(c[0] + e[0], c[1], c[2])
            self.grab_plane.SetPoint2(c[0], c[1] + e[1], c[2])
            self.grab_plane.SetNormal(0, 0, 1)
        self.grab_plane.SetCenter(*c)

        grab_mapper = vtk.vtkPolyDataMapper()
        grab_mapper.SetInputConnection(self.grab_plane.GetOutputPort())

        self.grab_actor.SetMapper(grab_mapper)
        self.grab_actor.GetProperty().SetOpacity(0.0001)
        self.renderer.AddActor(self.grab_actor)

        self.visu.view().updateShortcutKeys()
        self.visu.render()

    def releasePoint(self, save=False):
        if self.grab_mode:
            if save:
                pointCloud = self.visu.point_cloud
                p = np.array(self.cell_sphere.GetCenter())
                if self.add_new_point:
                    pointCloud.addPoint(*p)
                else:
                    pointCloud.setPointPosition(self.wispId,*p)

            self.grab_mode = False
            self.add_new_point = False
            self.renderer.RemoveActor(self.grab_actor)

        self.deselectPoint()
        self.vtkId = -1
        self.visu.update()
        self.updateTextActor()

        self.visu.view().updateShortcutKeys()
        self.visu.render()

    def updatePane(self):
        # self.view.infoPane().setTitle(self.property_name)
        # properties = self.topomesh
        # self.visu.view().infoPane().clear()
        # topomesh = self.visu.topomesh
        # degree = 0
        # properties = topomesh.wisp_property_names(degree)
        # info = {}
        # for key in properties:
        #     info[key+':'] = str(topomesh.wisp_property(key,degree)[self.wispId])
        # if self.vtkId > -1:
        #     self.visu.view().infoPane().addInfoPaneItem('Point :'+str(self.wispId), info)
        # elif self.vtkId == -1:
        #     if self.visu.view().infoPane().isToggled():
        #         self.visu.view().infoPane().toggle()
        #         self.visu.view().infoPane().clear()
        pass

    # def setMode(self, mode):
    #     super().setMode(mode)

    def setVisualization(self, visu):
        self.visu = visu

    def disable(self):
        super().disable()
        if self.has_selection:
            self.renderer.RemoveActor(self.cell_actor)

    def icon(self):
        return 0xf05b #fa::crosshairs

    def description(self):
        return "Point Picker"

    def keyMap(self):
        keymap = {}

        # keymap[Qt.Key_R] = "Reset camera focus"
        # if self.has_selection:
        #     if not self.grab_mode:
        #         keymap[Qt.Key_D] = "Delete selected point"
        #         if self.mode == "2D":
        #             keymap[Qt.Key_G] = "Grab and move selected point"
        #             keymap[Qt.Key_A] = "Add a new point and move it"
        #     else:
        #         if self.mode == "2D":
        #             keymap[Qt.Key_S] = "Save point at the current position"
        #             keymap[Qt.Key_Q] = "Cancel motion and drop selection"


        return keymap
