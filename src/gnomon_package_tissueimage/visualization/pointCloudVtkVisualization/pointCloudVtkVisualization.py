import os
import logging

import numpy as np

import pyvista as pv

from dtkcore import d_bool, d_inliststring, d_int, d_real, d_range_real, array_real_2

import gnomon.visualization
from gnomon.visualization import gnomonAbstractPointCloudVtkVisualization

from gnomon.utils.decorators import pointCloudInput
from gnomon.utils import visualizationPlugin


@visualizationPlugin(version="1.0.0", coreversion="1.0.0", name="Point Cloud Visualization")
@pointCloudInput('df', data_plugin="gnomonPointCloudDataPandas")
class pointCloudVtkVisualization(gnomonAbstractPointCloudVtkVisualization):

    def __init__(self):
        super().__init__()

        self._parameters = {}

        self._parameters['position'] = d_inliststring("Position", "", [""], "Property to use as spatial reference")
        self._parameters['property'] = d_inliststring("Property", "", [""], "Point property to display")
        self._parameters['scale_factor'] = d_real("Scale factor", 1, 0, 100, 2, "Scale of scale property to display")

        self._parameters['colormap'] = gnomon.visualization.ParameterColorMap("Colormap", 'gray', "Colormap to apply to the point cloud")
        self._parameters['value_range'] = d_range_real("Value range", array_real_2([0., 1.]), 0., 1., "Value range for color adjustment")
        self._parameters['opacity'] = d_real("Opacity", 1, 0, 1, 2, "Opacity of the points")

        self._parameters['point_glyph'] = d_inliststring( "Point glyph", "sphere", ["sphere", "point"], "Glyph type to display points with scalar properties")
        # self._parameters['vector_glyph'] = d_inliststring("Vector glyph", "arrow", ["arrow", "line"], "Glyph type to display vector properties")
        # self._parameters['tensor_glyph'] = d_inliststring("Tensor glyph", "crosshair", ["crosshair", "ellipsoid"], "Glyph type to display tensor properties")
        
        self._parameters['slice_thickness'] = d_real("Slice thickness", 2, 0, 5, 2, "Thickness of the 2D slice to display")
        self._parameters['show_labels'] = d_bool("Show labels", False, "Whether to display point labels next to each point")
        self._parameters['font_size'] = d_int("Label size", 12, 0, 64, "Font size for the displayed cell labels")

        self._parameters["filter_name"] = d_inliststring("Filter", "", [""], "Cell property used to filter out cells")
        self._parameters["filter_range"] = d_range_real("Filter range", array_real_2([0., 1.]), 0., 1., "Value range for cell filtering")

        self._parameter_groups = {}
        for parameter_name in ['colormap', 'value_range', 'opacity']:
            self._parameter_groups[parameter_name] = 'rendering'
        for parameter_name in ['slice_thickness', 'show_labels', 'font_size']:
            self._parameter_groups[parameter_name] = 'display'
        for parameter_name in ['point_glyph', 'vector_glyph', 'tensor_glyph']:
            self._parameter_groups[parameter_name] = 'glyphs'
        for parameter_name in ['filter_name', 'filter_range']:
            self._parameter_groups[parameter_name] = 'filter'

        self.connectParameter("property")

        self.df = {}
        self.current_time = 0
        
        self._current_value = None

        self.point_polydata3D = None
        self.glyph_polydata3D = None
        self.point_actor3D = None

        self.point_polydata2D = None
        self.glyph_polydata2D = None
        self.point_actor2D = None

        self.label_polydata3D = None
        self.label_actor3D = None
        self.label_polydata2D = None
        self.label_actor2D = None

        self.slice_orientation = 2
        self.slice_position = 0

        self.is2D = False

        self.renderer3D = None
        self.renderer2D = None
        self.scalar_bar_widget = None
        self.axes = None

        self.flag_axes = False
        self.flag_scalar_bar = False

    def __del__(self):
        if self.point_actor3D is not None:
            del self.point_actor3D
        if self.point_actor2D is not None:
            del self.point_actor2D
        if self.label_actor3D is not None:
            del self.label_actor3D
        if self.label_actor2D is not None:
            del self.label_actor2D

    def clear(self):
        if self.vtkView():
            self.renderer3D = self.vtkView().renderer3D()
            self.renderer2D = self.vtkView().renderer2D()
            if self.point_actor3D is not None:
                self.renderer3D.RemoveActor(self.point_actor3D)
            if self.label_actor3D is not None:
                self.renderer3D.RemoveActor(self.label_actor3D)
            if self.point_actor2D is not None:
                self.renderer2D.RemoveActor(self.point_actor2D)
            if self.label_actor2D is not None:
                self.renderer2D.RemoveActor(self.label_actor2D)

    def fill(self):
        if self.vtkView():
            self.renderer3D = self.vtkView().renderer3D()
            self.renderer2D = self.vtkView().renderer2D()
            if self.point_actor3D is not None:
                self.renderer3D.AddActor(self.point_actor3D)
            if self.label_actor3D is not None:
                self.renderer3D.AddActor(self.label_actor3D)
            if self.point_actor2D is not None:
                self.renderer2D.AddActor(self.point_actor2D)
            if self.label_actor2D is not None:
                self.renderer2D.AddActor(self.label_actor2D)

    def setVisible(self, visible):
        if self.point_actor3D is not None:
            self.point_actor3D.visibility = visible
        if self.label_actor3D is not None:
            self.label_actor3D.visibility = visible and self['show_labels']
        if self.point_actor2D is not None:
            self.point_actor2D.visibility = visible
        if self.label_actor2D is not None:
            self.label_actor2D.visibility = visible and self['show_labels']

    def refreshParameters(self):
        self._current_value = {
            p: None for p in self._parameters.keys()
        }
        
        if len(self.df) > 0:
            self.current_time = list(self.df.keys())[0]

            df = self.df[self.current_time]

            property_name = self['property']
            prop = [""]+list(df.columns)
            self._parameters['property'].setValues(prop)
            self._parameters['property'].setValue(property_name if property_name in prop else "")

            self._update_value_range()

            filter_name = self["filter_name"]
            self._parameters["filter_name"].setValues(prop)
            self._parameters["filter_name"].setValue(filter_name if filter_name in prop else "")

            self._update_value_range(filter=True)

            if len(df) > 10000:
                self._parameters['point_glyph'].setValue('point')
            else:
                self._parameters['point_glyph'].setValue('sphere')

            properties = [v for v in df.columns if not 'Unnamed:' in v]
            properties = [v for v in properties if not np.array(df[v]).dtype == np.dtype('O')]
            properties = [v for v in properties if np.array(df[v]).ndim == 1]
            property_prefixes = np.unique([v[:-2] for v in properties])
            position_properties = [v for v in property_prefixes if all([f"{v}_{dim}" in properties for dim in "xyz"])]

            self._parameters['position'].setValues(position_properties)
            if len(position_properties) > 0:
                self._parameters['position'].setValue(position_properties[0])

    def onParameterChanged(self, parameter_name):
        if parameter_name == 'property':
            self._update_value_range()
        if parameter_name == 'filter_name':
            self._update_value_range(filter=True)

    def _update_value_range(self, filter=False):
        if len(self.df) > 0:
            self.current_time = list(self.df.keys())[0]
            name_param = "filter_name" if filter else "property"
            range_param = "filter_range" if filter else "value_range"

            df = self.df[self.current_time]

            property_name = self[name_param]
            if property_name in df.columns:
                point_properties = df[property_name].values
            else:
                point_properties = np.ones_like(df.index)

            min_p = float(np.around(np.nanmin(point_properties), decimals=3))
            max_p = float(np.around(np.nanmax(point_properties), decimals=3))

            self._parameters[range_param].setMin(np.around(min_p - (max_p - min_p)/2, decimals=3))
            self._parameters[range_param].setMax(np.around(max_p + (max_p - min_p)/2, decimals=3))
            self._parameters[range_param].setValueMin(min_p)
            self._parameters[range_param].setValueMax(max_p)

    def imageRendering(self):
        if self.point_actor3D is not None:
            self.updateOffscreenRenderer(*self.point_actor3D.GetMapper().GetInput().GetBounds())
            self.offscreenRenderer().AddActor(self.point_actor3D)
        return self.offscreenImageRendering()

    def _update_points(self):
        df = self.df[self.current_time]

        filter_changed = self._current_value['filter_name'] != self['filter_name']
        if filter_changed:
            self._current_value['filter_name'] = self['filter_name']
            self._update_value_range(filter=True)

        points_changed = self._current_value['position'] != self['position']
        points_changed |= self._current_value['filter_name'] != self['filter_name']
        points_changed |= (self._current_value['filter_range'] is None) or any([
            self._current_value['filter_range'][k] != self['filter_range'][k]
            for k in range(2)
        ])

        if self.point_polydata3D is None or points_changed:

            if self['filter_name'] in df.columns:
                df = df.query(f"{self['filter_name']} >= {self['filter_range'][0]}")
                df = df.query(f"{self['filter_name']} <= {self['filter_range'][1]}")

            point_positions = df[[f"{self['position']}_{dim}" for dim in 'xyz']].values

            self.point_polydata3D = pv.PolyData(point_positions)
            for property_name in df.columns:
                property_values = np.array(df[property_name])
                if (not property_values.dtype == np.dtype('O')) and (property_values.ndim == 1):
                    self.point_polydata3D.point_data[property_name] = property_values
            self.point_polydata3D.point_data['display_label'] = np.ones(self.point_polydata3D.n_points)
            self.glyph_polydata3D = None

            self._current_value['position'] = self['position']
            self._current_value['filter_name'] = self['filter_name']
            self._current_value['filter_range'] = [self['filter_range'][k] for k in range(2)]

    def _update_glyphs(self):
        glyphs_changed = any([
            self._current_value[p] != self[p]
            for p in ['point_glyph', 'scale_factor']
        ])
        if self.glyph_polydata3D is None or glyphs_changed:
            if self['point_glyph'] == 'sphere':
                self.glyph_polydata3D = self.point_polydata3D.glyph(
                    geom=pv.Sphere(radius=self['scale_factor']),
                    scale=False, orient=False
                )
            else:
                self.glyph_polydata3D = self.point_polydata3D
            for p in ['point_glyph', 'scale_factor']:
                self._current_value[p] = self[p]

    def _update_rendering(self):
        property_changed = self._current_value['property'] != self['property']
        if property_changed:
            self._current_value['property'] = self['property']
            self._update_value_range()

        rendering_changed = any([
            property_changed,
            self._current_value['opacity'] != self['opacity'],
            (self._current_value['value_range'] is None) or any([
                self._current_value['value_range'][k] != self['value_range'][k]
                for k in range(2)
            ]),
            self._current_value['colormap'] != self._parameters['colormap'].name(),
        ])
        if self.point_actor3D is None or rendering_changed:
            plotter_args = {
                "scalars": 'display_label' if self['property'] == "" else self['property'],
                "cmap": self._parameters['colormap'].name(),
                "clim": (0, 1) if self['property'] == "" else self["value_range"],
                "opacity": self['opacity']
            }

            for p in ['property', 'opacity']:
                self._current_value[p] = self[p]
            self._current_value['value_range'] = [self['value_range'][k]  for k in range(2)]
            self._current_value['colormap'] = self._parameters['colormap'].name()

            plotter = pv.Plotter(off_screen=True)

            if self.point_actor3D is not None:
                self.renderer3D.RemoveActor(self.point_actor3D)
            self.glyph_polydata3D.point_data.active_scalars_name = plotter_args["scalars"]
            self.point_actor3D = plotter.add_mesh(
                self.glyph_polydata3D, **plotter_args
            )
            self.renderer3D.AddActor(self.point_actor3D)

            if self.point_actor2D is not None:
                self.renderer2D.RemoveActor(self.point_actor2D)
            self.point_actor2D = plotter.add_mesh(
                self.glyph_polydata3D,  **plotter_args
            )
            self.renderer2D.AddActor(self.point_actor2D)

        else:
            scalars = 'display_label' if self['property'] == "" else self['property']
            self.point_actor3D.GetMapper().SetInputData(self.glyph_polydata3D)
            self.glyph_polydata3D.point_data.active_scalars_name = scalars
            self.point_actor2D.mapper.SetInputData(self.glyph_polydata3D)

    def _update_labels(self):
        if self['show_labels']:
            if self.label_polydata3D is None:
                label_polydata = pv.PolyData(self.point_polydata3D.points)
                label_polydata.point_data['label'] = self.point_polydata3D.point_data['label']
                self.label_polydata3D = label_polydata

            labels_changed = self._current_value['font_size'] != self['font_size']
            if self.label_actor3D is None or labels_changed:
                plotter = pv.Plotter(off_screen=True)

                if self.label_actor3D is not None:
                    self.renderer3D.RemoveActor(self.label_actor3D)
                self.label_actor3D = plotter.add_point_labels(
                    self.label_polydata3D, labels='label',
                    show_points=False, shape=None, background_opacity=0,
                    italic=False, bold=True, font_size=self['font_size'], text_color='w', shadow=True,
                    justification_horizontal='center', justification_vertical='center',
                    always_visible=True
                )
                self.renderer3D.AddActor(self.label_actor3D)

                if self.label_actor2D is not None:
                    self.renderer2D.RemoveActor(self.label_actor2D)
                self.label_actor2D = plotter.add_point_labels(
                    self.label_polydata3D, labels='label',
                    show_points=False, shape=None, background_opacity=0,
                    italic=False, bold=True, font_size=self['font_size'], text_color='w', shadow=True,
                    justification_horizontal='center', justification_vertical='center',
                    always_visible=True
                )
                self.renderer2D.AddActor(self.label_actor2D)
            else:
                mapper = self.label_actor3D.GetMapper()
                hier = mapper.GetInputConnection(0, 0).GetProducer()
                hier.SetInputData(self.label_polydata3D)

                mapper = self.label_actor2D.GetMapper()
                hier = mapper.GetInputConnection(0, 0).GetProducer()
                hier.SetInputData(self.label_polydata3D)

        if self.label_actor3D is not None:
            self.label_actor3D.SetVisibility(self['show_labels'])
            self.label_actor2D.SetVisibility(self['show_labels'])

    def _update_slice(self):
        if self.glyph_polydata3D is not None:

            slice_mask = self.glyph_polydata3D.points[:, self.slice_orientation] >  self.slice_position - self['slice_thickness']/2
            slice_mask &= self.glyph_polydata3D.points[:, self.slice_orientation] <  self.slice_position + self['slice_thickness']/2
            slice_point_ids = np.arange(self.glyph_polydata3D.n_points)[slice_mask]

            self.glyph_polydata2D = self.glyph_polydata3D.extract_points(slice_point_ids, include_cells=True)

            self.point_actor2D.mapper.SetInputData(self.glyph_polydata2D)

            if self['show_labels']:
                if self.glyph_polydata2D.n_points > 0:

                    df = self.df[self.current_time]
                    points = dict(zip(
                        df['label'].values,
                        df[[f"{self['position']}_{dim}" for dim in 'xyz']].values
                    ))

                    display_labels = np.unique(self.glyph_polydata2D.point_data['label'])

                    label_points = np.array([points[l] for l in display_labels])
                    label_points[:, self.slice_orientation] = self.slice_position

                    self.label_polydata_2D = pv.PointSet(label_points)
                    self.label_polydata_2D.point_data['label'] = display_labels
                else:
                    self.label_polydata_2D = pv.PointSet()

                mapper = self.label_actor2D.GetMapper()
                hier = mapper.GetInputConnection(0, 0).GetProducer()
                hier.SetInputData(self.label_polydata_2D)

    def update(self):
        if self.current_time in self.df.keys():
            self.renderer3D = self.vtkView().renderer3D()
            self.renderer2D = self.vtkView().renderer2D()

            self._update_points()
            self._update_glyphs()

            self._update_rendering()
            self._update_labels()

            bounds = self.point_polydata3D.GetBounds()
            self.vtkView().setBounds(*bounds)

            self._update_slice()

            self.render()

    def render(self):
        self.view().render()

    def onTimeChanged(self, value):
        if value in self.df.keys():
            time_changed =  value != self.current_time
            self.current_time = value
            if time_changed:
                self.point_polydata3D = None
                self.update()
        self.render()

    def on3D(self):
        self.render()

    def on2D(self):
        self.render()

    def onXY(self):
        self.render()

    def onYZ(self):
        self.render()

    def onXZ(self):
        self.render()

    def onSliceOrientationChanged(self, value):
        self.slice_orientation = value

    def onSliceChanged(self, value):
        if value != self.slice_position:
            self.slice_position = value
            self._update_slice()
        self.render()