import unittest

import numpy as np

import gnomon.core
from gnomon.core import gnomonCellImage
from gnomon.utils import load_plugin_group

from timagetk.synthetic_data.labelled_image import example_layered_sphere_labelled_image
from timagetk import TissueImage3D, LabelledImage


class TestGnomonCellImage(unittest.TestCase):
    """Tests the gnomonCellImage class.

    """

    @classmethod
    def setUpClass(cls):
        load_plugin_group("cellImageData")
        np.random.seed(42)

    def setUp(self):
        self.img = example_layered_sphere_labelled_image(extent=20, voxelsize=(1., 0.5, 0.5), n_points=9)
        self.tissue = TissueImage3D(self.img, not_a_label=0, background=1)
        self.tissue.cells.volume()
        self.tissue.walls.area()

        # -- Creating a gnomonCellImage from a PropertySpatialImage
        self.cell_img = gnomonCellImage()
        self.cell_img_data = gnomon.core.cellImageData_pluginFactory().create("gnomonCellImageDataTissueImage")
        self.cell_img_data.set_tissue_image(self.tissue)
        self.cell_img.setData(self.cell_img_data)

        self.filename = "test/resources/TestGnomonCellImageSerialization.txt"
        with open(self.filename, "w") as file:  # prepare serialization resource
            file.write(self.cell_img_data.serialize())

    def tearDown(self):
        self.cell_img.this.disown()
        self.cell_img_data.this.disown()

    def test_gnomonCellImage_clone(self):
        clone = self.cell_img.clone().asCellImage()
        assert clone.cellCount() == self.cell_img.cellCount()

    def test_gnomonCellImage_cells(self):
        assert self.cell_img.cellCount() == 10

    def test_gnomonCellImage_image(self):
        vtk_img = self.cell_img.image()
        dims = vtk_img.GetDimensions()
        assert dims[0] == self.img.shape[2]
        assert dims[1] == self.img.shape[1]
        assert dims[2] == self.img.shape[0]

    def test_gnomonCellImage_propertyNames(self):
        assert "volume" in self.cell_img.cellPropertyNames()
        cell_volumes = self.cell_img.cellProperty("volume")
        assert np.all([l in cell_volumes.keys() for l in self.cell_img.cellIds()])

    def test_gnomonCellImage_computeProperty(self):
        self.cell_img.computeCellProperty("area")
        assert "area" in self.cell_img.cellPropertyNames()
        cell_areas = self.cell_img.cellProperty("area")
        assert np.all([l in cell_areas.keys() for l in self.cell_img.cellIds()])

    def test_gnomonCellImage_updateProperty(self):
        layer_property = {l:1. for l in self.cell_img.cellIds()}
        # TODO: Fix SWIG typemap for QMap<long, QVariant>
        # self.cell_img.updateCellProperty("layer", layer_property, True)
        self.cell_img_data.updateCellProperty("layer", layer_property, True)
        assert "layer" in self.cell_img.cellPropertyNames()
        cell_layers = self.cell_img.cellProperty("layer")
        assert np.all([l in cell_layers.keys() for l in self.cell_img.cellIds()])
        assert np.all([cell_layers[l] == layer_property[l] for l in self.cell_img.cellIds()])

    # def test_gnomonCellImage_fromGnomonForm(self):
    #     cell_img_data = gnomon.core.cellImageData_pluginFactory().create("gnomonCellImageDataTissueImage")
    #     cell_img_data.fromGnomonForm(self.cell_img)
    #     assert cell_img_data.cellCount() == 10
    #     assert "volume" in cell_img_data.cellPropertyNames()
    #     assert "area" in cell_img_data.wallPropertyNames()

    def test_gnomonCellImage_wallPropertyNames(self):
        assert "area" in self.cell_img.wallPropertyNames()
        wall_areas = self.cell_img.wallProperty("area")
        wall_ids = self.cell_img.wallIds()
        assert all([w in wall_areas.keys() for w in wall_ids])

    def test_gnomonCellImage_updateWallProperty(self):
        wall_ids = self.cell_img.wallIds()
        layer_property = {w: 1. for w in wall_ids}
        # TODO: Fix SWIG typemap for QMap<long, QVariant>
        # self.cell_img.updateWallProperty("layer", layer_property, True)
        self.cell_img_data.updateWallProperty("layer", layer_property, True)
        assert "layer" in self.cell_img.wallPropertyNames()
        wall_layers = self.cell_img.wallProperty("layer")
        assert all([w in wall_layers.keys() for w in wall_ids])
        assert all([wall_layers[w] == layer_property[w] for w in wall_ids])

    def test_serialization(self):
        serialization = self.cell_img.data().serialize()
        with open(self.filename, "r") as file:
            control = file.read()
        assert control == serialization

    def test_deserialization(self):
        with open(self.filename, "r") as file:
            control = file.read()
        new_form_data = gnomon.core.cellImageData_pluginFactory().create("gnomonCellImageDataTissueImage")
        new_form_data.deserialize(control)
        assert np.all(new_form_data._tissue == self.cell_img.data()._tissue)

#
# test_gnomonCellImage.py ends here.
