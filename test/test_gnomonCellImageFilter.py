import unittest

import gnomon.core
from gnomon.utils import load_plugin_group


class TestGnomonMorphoCellFilterTimagetk(unittest.TestCase):
    """Tests the morphoCellFilter class.

    """

    @classmethod
    def setUpClass(cls):
        load_plugin_group("cellImageReader")
        load_plugin_group("cellImageFilter")

    def setUp(self):
        self.filename = "test/resources/p58-t0_imgSeg_down_interp_2x.inr.gz"

        self.reader = gnomon.core.cellImageReader_pluginFactory().create("cellImageReaderTimagetk")
        self.reader.setPath(self.filename)
        self.reader.run()

        self.filter = gnomon.core.cellImageFilter_pluginFactory().create("morphoCellFilter")
        self.filter.setInput(self.reader.cellImage())
        self.filter.refreshParameters()

        self.cellimage = None

    def tearDown(self):
        self.reader.this.disown()
        self.filter.this.disown()
        if self.cellimage is not None:
            self.cellimage.this.disown()

    def test_gnomonCellImageFilter_parameters(self):
        assert self.filter.parameters() is not None
        assert 'iterations' in self.filter.parameters()
        self.filter.setParameter('iterations', 1)
        assert self.filter['iterations'] == 1

    def test_gnomonCellImageFilter_filter(self):
        self.filter.run()
        self.cellimage = self.filter.output()[0]
        assert self.cellimage is not None

#
# test_gnomonCellImageFilter.py ends here.
