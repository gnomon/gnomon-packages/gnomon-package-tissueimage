import unittest

import gnomon.core
from gnomon.utils import load_plugin_group


class TestGnomonSeededWatershedSegmentationTimagetk(unittest.TestCase):
    """Tests the seededWatershedSegmentation class.

    """

    @classmethod
    def setUpClass(cls):
        load_plugin_group("imageReader")
        load_plugin_group("cellImageFromImage")

    def setUp(self):
        self.filename = "test/resources/qDII-CLV3-PIN1-PI-E35-LD-SAM1-T0-Subset.czi"

        self.reader = gnomon.core.imageReader_pluginFactory().create("imageReaderTimagetk")
        self.reader.setPath(self.filename)
        self.reader.run()

        self.segmentation = gnomon.core.cellImageFromImage_pluginFactory().create("seededWatershedSegmentationTimagetk")
        self.segmentation.setInput(self.reader.image())
        self.segmentation.refreshParameters()

        self.cellimage = None

    def tearDown(self):
        self.reader.this.disown()
        self.segmentation.this.disown()
        if self.cellimage is not None:
            self.cellimage.this.disown()

    def test_gnomonCellImageFromImage_parameters(self):
        assert self.segmentation.parameters() is not None
        assert 'h_min' in self.segmentation.parameters()
        self.segmentation.setParameter('h_min',1500)
        assert self.segmentation['h_min'] == 1500

    def test_gnomonCellImageFromImage_segmentation(self):
        self.segmentation.run()
        self.cellimage = self.segmentation.output()[0]
        assert self.cellimage is not None

#
# test_gnomonCellImageFromImage.py ends here.
