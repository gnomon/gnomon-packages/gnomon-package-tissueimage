import unittest

import numpy as np

import gnomon.core
from gnomon.utils import load_plugin_group


class TestGnomonCellImageQuantification(unittest.TestCase):
    '''
    Tests the cellPropertyTissueImage and signalQuantificationImageSignal class.
    '''

    @classmethod
    def setUpClass(cls):
        load_plugin_group("imageReader")
        load_plugin_group("cellImageReader")
        load_plugin_group("cellImageQuantification")

    def setUp(self):
        self.filename = "test/resources/p58-t0_imgFus_down_interp_2x.inr.gz"
        self.seg_filename = "test/resources/p58-t0_imgSeg_down_interp_2x.inr.gz"

        self.reader = gnomon.core.imageReader_pluginFactory().create("imageReaderTimagetk")
        self.reader.setPath(self.filename)
        self.reader.run()

        self.seg_reader = gnomon.core.cellImageReader_pluginFactory().create("cellImageReaderTimagetk")
        self.seg_reader.setPath(self.seg_filename)
        self.seg_reader.run()

        self.property_quantification = gnomon.core.cellImageQuantification_pluginFactory().create("cellFeaturesTissueImage")
        self.property_quantification.setCellImage(self.seg_reader.cellImage())
        self.property_quantification.refreshParameters()

        self.signal_quantification = gnomon.core.cellImageQuantification_pluginFactory().create("signalQuantificationImageSignal")
        self.signal_quantification.setCellImage(self.seg_reader.cellImage())
        self.signal_quantification.setImage(self.reader.image())
        self.signal_quantification.refreshParameters()

        self.wall_property_quantification = gnomon.core.cellImageQuantification_pluginFactory().create("wallFeaturesTissueImage")
        self.wall_property_quantification.setCellImage(self.seg_reader.cellImage())
        self.wall_property_quantification.refreshParameters()

        self.wall_signal_quantification = gnomon.core.cellImageQuantification_pluginFactory().create("wallSignalQuantificationImage")
        self.wall_signal_quantification.setCellImage(self.seg_reader.cellImage())
        self.wall_signal_quantification.setImage(self.reader.image())
        self.wall_signal_quantification.refreshParameters()

        self.cellimage = None
        self.dataframe = None

    def tearDown(self):
        self.reader.this.disown()
        self.seg_reader.this.disown()
        self.signal_quantification.this.disown()
        if self.dataframe is not None:
            self.dataframe.this.disown()

    def test_propertyQuantification_layer(self):
        self.property_quantification.setParameter('properties', ['volume', 'layer'])
        self.property_quantification.run()
        self.dataframe = self.property_quantification.outputDataFrame()[0]
        assert self.dataframe is not None
        self.cellimage = self.property_quantification.outputCellImage()[0]
        assert self.cellimage is not None
        assert "layer" in self.cellimage.cellPropertyNames()
        cell_layers = self.cellimage.cellProperty("layer")
        assert np.all([l in cell_layers.keys() for l in self.cellimage.cellIds()])

    def test_signalQuantification_parameters(self):
        assert self.signal_quantification.parameters() is not None
        assert 'erosion_radius' in self.signal_quantification.parameters()
        self.signal_quantification.setParameter('erosion_radius',0)
        assert self.signal_quantification['erosion_radius'] == 0

    def test_signalQuantification_quantification(self):
        self.signal_quantification.run()
        self.dataframe = self.signal_quantification.outputDataFrame()[0]
        assert self.dataframe is not None
        self.cellimage = self.signal_quantification.outputCellImage()[0]
        assert self.cellimage is not None
        assert "image_signal" in self.cellimage.cellPropertyNames()

    def test_wallPropertyQuantification_area(self):
        self.wall_property_quantification.run()
        self.dataframe = self.wall_property_quantification.outputDataFrame()[0]
        assert self.dataframe is not None
        self.cellimage = self.wall_property_quantification.outputCellImage()[0]
        assert self.cellimage is not None
        assert "area" in self.cellimage.wallPropertyNames()

    def test_wallSignalQuantification_quantification(self):
        self.wall_signal_quantification.run()
        self.dataframe = self.wall_signal_quantification.outputDataFrame()[0]
        assert self.dataframe is not None
        self.cellimage = self.wall_signal_quantification.outputCellImage()[0]
        assert self.cellimage is not None
        assert "image_signal" in self.cellimage.wallPropertyNames()

#
# test_gnomonCellImageQuantification.py ends here.
