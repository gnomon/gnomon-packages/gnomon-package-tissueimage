# Version: $Id$
#
#

# Commentary:
#
#

# Change Log:
#
#

# Code:

import unittest

import gnomon.core
from gnomon.utils import load_plugin_group


class TestGnomonCellImageReader(unittest.TestCase):
    '''
    Tests the gnomonCellImageReader class.
    '''

    @classmethod
    def setUpClass(cls):
        load_plugin_group("cellImageReader")

    def setUp(self):
        self.filename = "test/resources/p58-t0_imgSeg_down_interp_2x.inr.gz"

        self.reader = gnomon.core.cellImageReader_pluginFactory().create("cellImageReaderTimagetk")
        self.reader.setPath(self.filename)

    def tearDown(self):
        self.reader.this.disown()

    def test_gnomonCellImageReader_read(self):
        self.reader.run()
        cellImage = self.reader.cellImage()[0]
        assert cellImage is not None

    def test_gnomonCellImageReader_cellImage(self):
        self.reader.run()
        cellImage = self.reader.cellImage()[0]
        assert cellImage.cellCount() == 1039

#
# test_gnomonCellImageReader.py ends here.
