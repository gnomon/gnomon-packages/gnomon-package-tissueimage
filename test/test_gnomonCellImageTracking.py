import unittest

import numpy as np

import gnomon.core
from gnomon.utils import load_plugin_group


class TestGnomonCellImageTracking(unittest.TestCase):
    '''
    Tests the cellPropertyTissueImage and cellSuperpositionTracking class.
    '''

    @classmethod
    def setUpClass(cls):
        load_plugin_group("imageReader")
        load_plugin_group("cellImageReader")
        load_plugin_group("cellImageTracking")

    def setUp(self):
        self.filenames = "test/resources/p58-t0_imgFus_down_interp_2x.inr.gz,test/resources/p58-t1_imgFus_down_interp_2x.inr.gz"
        self.seg_filenames = "test/resources/p58-t0_imgSeg_down_interp_2x.inr.gz,test/resources/p58-t1_imgSeg_down_interp_2x.inr.gz"

        self.reader = gnomon.core.imageReader_pluginFactory().create("imageReaderTimagetk")
        self.reader.setPath(self.filenames)
        self.reader.run()

        self.seg_reader = gnomon.core.cellImageReader_pluginFactory().create("cellImageReaderTimagetk")
        self.seg_reader.setPath(self.seg_filenames)
        self.seg_reader.run()

        from gnomon_package_tissueimage.algorithm.cellImageTracking.cellSuperpositionTracking import cellSuperpositionTracking
        self.image_tracking = cellSuperpositionTracking()
        #self.image_tracking = gnomon.core.cellImageTracking_pluginFactory().create("cellSuperpositionTracking")
        self.image_tracking.setCellImage(self.seg_reader.cellImage())
        self.image_tracking.setImage(self.reader.image())
        self.image_tracking.refreshParameters()

        self.cellimage = None

    def tearDown(self):
        self.reader.this.disown()
        self.seg_reader.this.disown()
        self.image_tracking.this.disown()

    def test_cellSuperpositionTracking(self):
        self.image_tracking.run()


#
# test_gnomonCellImageQuantification.py ends here.
