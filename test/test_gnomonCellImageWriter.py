import unittest
import os

import numpy as np

import gnomon.core
from gnomon.utils import load_plugin_group


class TestGnomonCellImageWriter(unittest.TestCase):
    '''
    Tests the gnomonCellImageWriter class.
    '''

    @classmethod
    def setUpClass(cls):
        load_plugin_group("cellImageReader")
        load_plugin_group("cellImageWriter")

    def setUp(self):
        self.filename = "test/resources/p58-t0_imgSeg_down_interp_2x.inr.gz"

        self.reader = gnomon.core.cellImageReader_pluginFactory().create("cellImageReaderTimagetk")
        self.reader.setPath(self.filename)

        self.reader.run()
        self.cellImage = self.reader.cellImage()
        self.cellImage[0].computeCellProperty("volume")

        self.saved_filename = "test/resources/cellImage_writer_tmp.tif"
        self.saved_data_filename = "test/resources/cellImage_writer_tmp.csv"

        self.writer = gnomon.core.cellImageWriter_pluginFactory().create("cellImageWriterTissueImage")
        self.writer.setPath(self.saved_filename)

    def tearDown(self):
        if os.path.exists(self.saved_filename):
            os.remove(self.saved_filename)
        if os.path.exists(self.saved_data_filename):
            os.remove(self.saved_data_filename)
        self.reader.this.disown()
        self.writer.this.disown()

    def test_gnomonCellImageWriter_write(self):
        self.writer.setCellImage(self.cellImage)
        self.writer.run()
        assert os.path.exists(self.saved_filename)

        self.reader.setPath(self.saved_filename)
        self.reader.run()
        read_cellImage = self.reader.cellImage()
        assert read_cellImage[0] is not None

        assert read_cellImage[0].image().GetDimensions() == self.cellImage[0].image().GetDimensions()
        assert read_cellImage[0].cellCount() == self.cellImage[0].cellCount()

        assert all([read_cellImage[0].hasCellProperty(p) for p in self.cellImage[0].cellPropertyNames()])
        for p in self.cellImage[0].cellPropertyNames():
            assert all([l in self.cellImage[0].cellProperty(p).keys() for l in self.cellImage[0].cellProperty(p).keys()])
            for l in self.cellImage[0].cellProperty(p).keys():
                    assert np.isclose(read_cellImage[0].cellProperty(p)[l], self.cellImage[0].cellProperty(p)[l], atol=1e-6)

#
# test_gnomonCellImageWriter.py ends here.
