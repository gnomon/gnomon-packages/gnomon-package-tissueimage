import unittest
import numpy as np

import gnomon.core
from gnomon.core import gnomonImage
from gnomon.utils import load_plugin_group

from timagetk.synthetic_data.nuclei_image import example_nuclei_signal_images
from timagetk import MultiChannelImage


class TestGnomonImage(unittest.TestCase):
    """Tests the gnomonImage class.

    """

    @classmethod
    def setUpClass(cls) -> None:
        load_plugin_group("imageData")
        np.random.seed(42)

    def setUp(self):

        reference_img, signal_img, _, _ = example_nuclei_signal_images(n_points=13, extent=20, voxelsize=(1, 0.5, 0.5))
        self.img = MultiChannelImage([reference_img, signal_img], channel_names=["reference","signal"])

        # -- Creating a gnomonImage from a SpatialImage dict
        self.image = gnomonImage()
        self.image_data = gnomon.core.imageData_pluginFactory().create("gnomonImageDataMultiChannelImage")
        self.image_data.set_image(self.img)
        self.image.setData(self.image_data)
        self.filename = "test/resources/TestGnomonImageSerialization.txt"
        with open(self.filename, "w") as file:  # prepare serialization resource
            file.write(self.image_data.serialize())

    def tearDown(self):
        self.image.this.disown()
        self.image_data.this.disown()

    def test_gnomonImage_channels(self):
        channels = self.image.channels()
        assert 'reference' in channels
        assert 'signal' in channels

    def test_gnomonImage_channelName(self):
        self.image.setChannelName('reference','nuclei')
        channels = self.image.channels()
        assert 'reference' not in channels
        assert 'nuclei' in channels

    def test_gnomonImage_clone(self):
        cloned_image = self.image.clone().asImage()
        assert all([ch in cloned_image.channels() for ch in self.image.channels()])
        assert not np.may_share_memory(cloned_image, self.image)
        for ch in self.image.channels():
            assert not np.may_share_memory(cloned_image.image(ch), self.image.image(ch))
            assert cloned_image.image(ch).GetDimensions() == self.image.image(ch).GetDimensions()

    # def test_gnomonImage_fromGnomonForm(self):
    #     image_data = gnomon.core.imageData_pluginFactory().create("gnomonImageDataMultiChannelImage")
    #     image_data.fromGnomonForm(self.image)
    #     assert 'reference' in image_data.channels()
    #     assert image_data.image("reference").xDim() == self.image.image("reference").xDim()

    def test_serialization(self):
        serialization = self.image.data().serialize()
        with open(self.filename, "r") as file:
            control = file.read()
        assert control == serialization

    def test_deserialization(self):
        with open(self.filename, "r") as file:
            control = file.read()
        new_form_data = gnomon.core.imageData_pluginFactory().create("gnomonImageDataMultiChannelImage")
        new_form_data.deserialize(control)
        for key, img in self.image.data()._image.items():
            assert key in new_form_data._image
            np.testing.assert_equal(new_form_data._image[key], img)

#
# test_gnomonImage.py ends here.
