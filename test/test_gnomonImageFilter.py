import unittest

import gnomon.core
from gnomon.utils import load_plugin_group

load_plugin_group("imageReader")
load_plugin_group("imageFilter")


class TestGnomonImageFilter(unittest.TestCase):
    '''
    Tests the gnomonImageFilter class.
    '''

    def setUp(self):
        self.filename = "test/resources/time_0_cut.inr"

        self.reader = gnomon.core.imageReader_pluginFactory().create("imageReaderTimagetk")
        self.reader.setPath(self.filename)
        self.reader.run()
        self.image = self.reader.image()

        self.filter = gnomon.core.imageFilter_pluginFactory().create("linearFilterTimagetk")

        self.morpho_contrast = gnomon.core.imageFilter_pluginFactory().create("morphoContrastTimagetk")
        self.global_contrast = gnomon.core.imageFilter_pluginFactory().create("globalContrastStretch")
        self.equalize_hist = gnomon.core.imageFilter_pluginFactory().create("equalizeAdaptHist")

        self.filtered_image = None

    def tearDown(self):
        self.reader.this.disown()
        self.filter.this.disown()
        self.morpho_contrast.this.disown()
        self.global_contrast.this.disown()
        self.equalize_hist.this.disown()

    def test_gnomonImageFilter_filter(self):
        self.filter.setInput(self.image)
        self.filter.refreshParameters()
        self.filter.run()
        self.filtered_image = self.filter.output()

        assert self.filtered_image is not None
        image = self.image[0]
        filtered_image = self.filtered_image[0]

        vtk_img = image.image()
        dims = vtk_img.GetDimensions()
        filtered_dims = filtered_image.image().GetDimensions()

        assert dims[0] == filtered_dims[0]
        assert dims[1] == filtered_dims[1]
        assert dims[2] == filtered_dims[2]

    def test_gnomonImageFilter_morpho_contrast(self):
        self.morpho_contrast.setInput(self.image)
        self.morpho_contrast.refreshParameters()
        self.morpho_contrast.run()
        self.filtered_image = self.morpho_contrast.output()

        assert self.filtered_image is not None
        image = self.image[0]
        filtered_image = self.filtered_image[0]
        vtk_img = image.image()
        dims = vtk_img.GetDimensions()
        filtered_dims = filtered_image.image().GetDimensions()

        assert dims[0] == filtered_dims[0]
        assert dims[1] == filtered_dims[1]
        assert dims[2] == filtered_dims[2]

    def test_gnomonImageFilter_global_contrast(self):
        self.global_contrast.setInput(self.image)
        self.global_contrast.refreshParameters()
        self.global_contrast.run()
        self.filtered_image = self.global_contrast.output()

        assert self.filtered_image is not None
        image = self.image[0]
        filtered_image = self.filtered_image[0]
        vtk_img = image.image()
        dims = vtk_img.GetDimensions()
        filtered_dims = filtered_image.image().GetDimensions()

        assert dims[0] == filtered_dims[0]
        assert dims[1] == filtered_dims[1]
        assert dims[2] == filtered_dims[2]

    def test_gnomonImageFilter_equalize_hist(self):
        self.equalize_hist.setInput(self.image)
        self.equalize_hist.refreshParameters()
        self.equalize_hist.run()
        self.filtered_image = self.equalize_hist.output()

        assert self.filtered_image is not None
        image = self.image[0]
        filtered_image = self.filtered_image[0]
        vtk_img = image.image()
        dims = vtk_img.GetDimensions()
        filtered_dims = filtered_image.image().GetDimensions()
        assert dims[0] == filtered_dims[0]
        assert dims[1] == filtered_dims[1]
        assert dims[2] == filtered_dims[2]



#
# test_gnomonImageFilter.py ends here.
