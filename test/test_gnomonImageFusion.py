# Version: $Id$
#
#

# Commentary:
#
#

# Change Log:
#
#

# Code:

import unittest

from dtkcore import d_int
import gnomon.core

# from timagetk.algorithms.resample import resample

from gnomon.utils import load_plugin_group
load_plugin_group("imageWriter")
load_plugin_group("imageReader")
load_plugin_group("imageFusion")

downsampling = 3.

# TODO: update the fusion algorithm
# class TestGnomonImageFusion(unittest.TestCase):
#     '''
#     Tests the gnomonImageFusion class.
#     '''
#
#     def setUp(self):
#         WRITE = False
#
#         if WRITE:
#             self.filename1 = "test/resources/time_0_cut.inr"
#             self.filename2 = "test/resources/time_0_cut_rotated1.mha"
#             self.filename3 = "test/resources/time_0_cut_rotated2.mha"
#         else:
#             self.filename1 = "test/resources/time_0_cut_resampled.inr"
#             self.filename2 = "test/resources/time_0_cut_rotated1_resampled.inr"
#             self.filename3 = "test/resources/time_0_cut_rotated2_resampled.inr"
#
#         if WRITE:
#             writer = gnomon.core.imageWriter_pluginFactory().create("gnomonImageWriter")
#         self.imgs = []
#
#         self.reader1 = gnomon.core.imageReader_pluginFactory().create("imageReaderTimagetk")
#         self.reader1.setPath(self.filename1)
#         self.reader1.run()
#         image1 = self.reader1.image()
#         #if WRITE:
#         #    ds_vxs1 = [vox * downsampling for vox in image1.voxelsize]
#         #    resampled_image1 = resample(image1, ds_vxs1, option='grey')
#         #    writer.setImage(sp_img_to_dtk_img(resampled_image1))
#         #    writer.setPath("resources/time_0_cut_resampled.inr")
#         #    writer.run()
#         #    self.imgs.append(sp_img_to_dtk_img(resampled_image1))
#         #else:
#         self.imgs.append(image1)
#
#         self.reader2 = gnomon.core.imageReader_pluginFactory().create("imageReaderTimagetk")
#         self.reader2.setPath(self.filename2)
#         self.reader2.run()
#         image2 = self.reader2.image()
#         #if WRITE:
#         #    ds_vxs2 = [vox * downsampling for vox in image2.voxelsize]
#         #    resampled_image2 = resample(image2, ds_vxs2, option='grey')
#         #    writer.setImage(sp_img_to_dtk_img(resampled_image2))
#         #    writer.setPath("resources/time_0_cut_rotated1_resampled.inr")
#         #    writer.run()
#         #    self.imgs.append(sp_img_to_dtk_img(resampled_image2))
#         #else:
#         self.imgs.append(image2)
#
#         self.reader3 = gnomon.core.imageReader_pluginFactory().create("imageReaderTimagetk")
#         self.reader3.setPath(self.filename3)
#         self.reader3.run()
#         image3 = self.reader3.image()
#         #if WRITE:
#         #    ds_vxs3 = [vox * downsampling for vox in image3.voxelsize]
#         #    resampled_image3 = resample(image3, ds_vxs3, option='grey')
#         #    writer.setImage(sp_img_to_dtk_img(resampled_image3))
#         #    writer.setPath("resources/time_0_cut_rotated2_resampled.inr")
#         #    writer.run()
#         #    self.imgs.append(sp_img_to_dtk_img(resampled_image3))
#         #else:
#         self.imgs.append(image3)
#
#         self.landmarks = [[[7.41806, 55.608, 5.4522], [62.0434, 47.8437, 5.4522]], [[7.41806, 55.608, 5.4522], [72.2863, 38.2216, 5.4522]], [[7.41806, 55.608, 5.4522], [62.0434, 47.8437, 5.4522]]]
#
#         self.resultimage = None
#
#     def tearDown(self):
#         self.reader1.this.disown()
#         self.reader2.this.disown()
#         self.reader3.this.disown()
#
#     def test_gnomonImageFusion_fusion(self):
#         self.fusion = gnomon.core.imageFusion_pluginFactory().create("gnomonImageFusion")
#         params = {'nb_iterations': d_int("", 0, 0, 5)}
#
#         self.fusion.setParameters(params)
#
#         for img in self.imgs:
#             self.fusion.addImage(img)
#
#         self.fusion.run()
#
#         self.resultimage = self.fusion.output()
#
#         assert self.resultimage is not None
#
#         self.fusion.this.disown()
#
#     def test_gnomonImageFusion_fusionWithLandmarks(self):
#         self.fusion = gnomon.core.imageFusion_pluginFactory().create("gnomonImageFusion")
#         params = {'nb_iterations': d_int("", 0, 0, 5)}
#
#         self.fusion.setParameters(params)
#
#         for img in self.imgs:
#             self.fusion.addImage(img)
#
#         for lndmks in self.landmarks:
#             self.fusion.addLandmarks(lndmks)
#
#         self.fusion.run()
#
#         self.resultimage = self.fusion.output()
#
#         assert self.resultimage is not None
#
#         self.fusion.this.disown()


#
# test_gnomonImageFusion.py ends here.
