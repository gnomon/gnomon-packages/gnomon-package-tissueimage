import unittest

import gnomon.core
from gnomon.utils import load_plugin_group


class TestGnomonImageReader(unittest.TestCase):
    """Tests the gnomonImageReader class.

    """

    @classmethod
    def setUpClass(cls):
        load_plugin_group("imageReader")

    def setUp(self):
        self.filename = "test/resources/p58-t0_imgFus_down_interp_2x.inr.gz"
        self.filename_czi = "test/resources/qDII-CLV3-PIN1-PI-E35-LD-SAM1-T0-Subset.czi"
        self.filename_tif = "test/resources/0hrs_plant1_trim-acylYFP_small.tif"
        self.reader = gnomon.core.imageReader_pluginFactory().create("imageReaderTimagetk")

        self.image = None

    def tearDown(self):
        self.reader.this.disown()

    def test_gnomonImageReader_read(self):
        self.reader.setPath(self.filename)
        self.reader.run()
        self.image = self.reader.image()[0]
        assert self.image is not None
        vtk_img = self.image.image("")
        dims = vtk_img.GetDimensions()
        assert dims[0] == 230
        assert dims[1] == 230
        assert dims[2] == 160

    def test_gnomonImageReader_readCzi(self):
            self.reader.setPath(self.filename_czi)
            self.reader.run()
            self.image = self.reader.image()[0]
            assert self.image is not None
            print(self.image.channels())
            vtk_img = self.image.image('Ch1-T3')
            dims = vtk_img.GetDimensions()
            assert dims[0] == 101
            assert dims[1] == 101
            assert dims[2] == 20

    def test_gnomonImageReader_readTif(self):
        self.reader.setPath(self.filename_tif)
        self.reader.run()
        self.image = self.reader.image()[0]
        assert self.image is not None
        vtk_img = self.image.image("")
        dims = vtk_img.GetDimensions()
        assert dims[0] == 137
        assert dims[1] == 137
        assert dims[2] == 116

#
# test_gnomonMultiChannelImage.py ends here.
