# Version: $Id$
#
#

# Commentary:
#
#

# Change Log:
#
#

# Code:

import unittest

import gnomon.core

# from timagetk.algorithms.resample import resample

from gnomon.utils import load_plugin_group
load_plugin_group("imageReader")
load_plugin_group("imageRegistration")

downsampling = 3.

class TestGnomonImageRegistration(unittest.TestCase):
    '''
    Tests the gnomonImageRegistration class.
    '''

    def setUp(self):

        self.filename1 = "test/resources/time_0_cut_resampled.inr"
        self.filename2 = "test/resources/time_0_cut_rotated1_resampled.inr"
        # self.filename3 = "test/resources/time_0_cut_rotated2_resampled.inr"

        # self.registration = gnomon.core.imageRegistration_pluginFactory().create("backwardRegistrationTimagetk")
        self.registration = gnomon.core.imageRegistration_pluginFactory().create("registrationTimagetk")

        self.reader1 = gnomon.core.imageReader_pluginFactory().create("imageReaderTimagetk")
        self.reader1.setPath(self.filename1)
        self.reader1.run()
        self.image = self.reader1.image()

        self.reader2 = gnomon.core.imageReader_pluginFactory().create("imageReaderTimagetk")
        self.reader2.setPath(self.filename2)
        self.reader2.run()
        self.image[1] = self.reader2.image()[0]

        self.registration.setImage(self.image)
        self.registration.refreshParameters()

        # self.reader3 = gnomon.core.imageReader_pluginFactory().create("imageReaderTimagetk")
        # self.reader3.setPath(self.filename3)
        # self.reader3.run()
        # self.registration.setInput(self.reader3.image())

        self.result_images = []

    def tearDown(self):
        self.reader1.this.disown()
        self.reader2.this.disown()
        # self.reader3.this.disown()
        self.registration.this.disown()

    def test_gnomonImageRegistration_rigid_registration(self):
        self.registration.setParameter("method", "rigid")
        self.registration.run()
        out = self.registration.output()
        assert(len(out) == len(self.image))

        for t in out.keys():
            assert t in self.image
            assert out[t] is not None
            
    def test_gnomonImageRegistration_rigid_transformation(self):
        self.registration.setParameter("method", "rigid")
        self.registration.run()
        out_trsf = self.registration.outputTransformation()
        assert(len(out_trsf) == len(self.image))

        rigid_trsf = out_trsf[0]
        assert "transform" in rigid_trsf.keys()

    def test_gnomonImageRegistration_affine_registration(self):
        self.registration.setParameter("method", "affine")
        self.registration.run()
        out = self.registration.output()
        assert(len(out) == len(self.image))

        for t in out.keys():
            assert t in self.image
            assert out[t] is not None

    def test_gnomonImageRegistration_deformable_registration(self):
        self.registration.setParameter("method","deformable")
        self.registration.run()
        out = self.registration.output()
        assert(len(out) == len(self.image))

        for t in out.keys():
            assert t in self.image
            assert out[t] is not None

#
# test_gnomonImageRegistration.py ends here.
