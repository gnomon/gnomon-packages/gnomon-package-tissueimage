import unittest
import os

import numpy as np

import gnomon.core
from gnomon.utils import load_plugin_group


class TestGnomonImageWriter(unittest.TestCase):
    """Tests the gnomonImageWriter class.

    """

    @classmethod
    def setUpClass(cls):
        load_plugin_group("imageReader")
        load_plugin_group("imageWriter")

    def setUp(self):
        self.filename = "test/resources/p58-t0_imgFus_down_interp_2x.inr.gz"

        self.reader = gnomon.core.imageReader_pluginFactory().create("imageReaderTimagetk")
        self.reader.setPath(self.filename)

        self.reader.run()
        self.image = self.reader.image()

        self.saved_filename = "test/resources/image_writer_tmp.tif"

        self.writer = gnomon.core.imageWriter_pluginFactory().create("gnomonImageWriter")
        self.writer.setPath(self.saved_filename)

    def tearDown(self):
        if os.path.exists(self.saved_filename):
            os.remove(self.saved_filename)
        self.reader.this.disown()
        self.writer.this.disown()

    def test_gnomonImageWriter_write(self):
        self.writer.setImage(self.image)
        self.writer.run()
        assert os.path.exists(self.saved_filename)

        self.reader.setPath(self.saved_filename)
        self.reader.run()
        read_image = self.reader.image()[0]
        assert read_image is not None

        # assert np.all(read_image.image().array() == self.image[0].image().array())

#
# test_gnomonImageWriter.py ends here.
