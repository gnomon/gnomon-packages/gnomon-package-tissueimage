# Version: $Id$
#
#

# Commentary:
#
#

# Change Log:
#
#

# Code:

import unittest

import numpy as np
import pandas as pd

import gnomon.core
from gnomon.core import gnomonPointCloud
from gnomon.utils import load_plugin_group


class TestGnomonPointCloud(unittest.TestCase):
    """Test the gnomonPointCloudDataPandas class.

    """

    @classmethod
    def setUpClass(cls):
        load_plugin_group("pointCloudData")

    def setUp(self):
        # -- Creating a PropertyTopomesh to build up our gnomonPointCloud
        self.n_points = 10
        self.df = pd.DataFrame()
        for dim in 'xyz':
            self.df['center_'+dim ] = np.random.rand(self.n_points)
        self.df['signal'] = np.random.rand(self.n_points)

        # -- Creating a gnomonPointCloud from a PropertyTopomesh
        self.point_cloud = gnomonPointCloud()
        self.point_cloud_data = gnomon.core.pointCloudData_pluginFactory().create("gnomonPointCloudDataPandas")
        self.point_cloud_data.set_data(self.df)
        self.point_cloud.setData(self.point_cloud_data)

        self.filename = "test/resources/TestGnomonPointCloudSerialization.txt"
        with open(self.filename, "w") as file:  # prepare serialization resource
            file.write(self.point_cloud_data.serialize())

    def tearDown(self):
        self.point_cloud_data.this.disown()

    def test_gnomonPointCloud_elements(self):
        assert self.point_cloud.pointCount() == self.n_points
        assert self.point_cloud.pointIds() == list(range(self.n_points))

    def test_gnomonPointCloud_properties(self):
        assert self.point_cloud.hasPointProperty('signal')
        for property_name in self.point_cloud.pointPropertyNames():
            assert property_name in self.df.columns

    def test_gnomonPointCloud_edition(self):
        pid = self.point_cloud.addPoint(0., 0., 0.)
        assert self.point_cloud.pointX()[pid] == 0
        assert self.point_cloud.pointCount() == self.n_points + 1
        self.point_cloud.setPointPosition(pid, 1., 1., 1.)
        assert self.point_cloud.pointX()[pid] == 1
        self.point_cloud.removePoint(pid)
        assert self.point_cloud.pointCount() == self.n_points

    def test_gnomonCellImage_fromGnomonForm(self):
        point_cloud_data = gnomon.core.pointCloudData_pluginFactory().create("gnomonPointCloudDataPandas")
        point_cloud_data.fromGnomonForm(self.point_cloud)
        assert point_cloud_data.pointCount() == self.n_points
        assert "signal" in point_cloud_data.pointPropertyNames()

    def test_serialization(self):
        serialization = self.point_cloud.data().serialize()
        with open(self.filename, "r") as file:
            control = file.read()
        assert control == serialization

    def test_deserialization(self):
        with open(self.filename, "r") as file:
            control = file.read()
        new_form_data = gnomon.core.pointCloudData_pluginFactory().create("gnomonPointCloudDataPandas")
        new_form_data.deserialize(control)
        assert control == new_form_data.serialize()


#
# test_gnomonPointCloud.py ends here.
