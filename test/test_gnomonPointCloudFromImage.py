import unittest

import numpy as np

import gnomon.core
from gnomon.core import gnomonImage
from gnomon.utils import load_plugin_group
from gnomon.utils.decorators.form_series import buildFormSeries

from timagetk.synthetic_data.nuclei_image import example_nuclei_signal_images
from timagetk.synthetic_data.wall_image import example_layered_sphere_wall_image
from timagetk import MultiChannelImage


class TestGnomonNucleiDetection(unittest.TestCase):
    """Tests the nucleiDetectionTimagetk class.

    """

    @classmethod
    def setUpClass(cls):
        load_plugin_group("imageData")
        load_plugin_group("pointCloudFromImage")
        np.random.seed(42)

    def setUp(self):
        self.n_points = 12
        reference_img, signal_img, _, _ = example_nuclei_signal_images(n_points=self.n_points,
                                                                       nuclei_radius=1.5,
                                                                       extent=20,
                                                                       voxelsize=(1, 0.5, 0.5))
        self.img = MultiChannelImage({'reference': reference_img, 'signal': signal_img})

        self.image = {0: gnomonImage()}
        self.image_data = gnomon.core.imageData_pluginFactory().create("gnomonImageDataMultiChannelImage")
        self.image_data.set_image(self.img)
        self.image[0].setData(self.image_data)

        self.detection = gnomon.core.pointCloudFromImage_pluginFactory().create("nucleiDetectionTimagetk")
        self.detection.setInput(self.image)
        self.detection.refreshParameters()

        self.pointcloud = None

    def tearDown(self):
        self.detection.this.disown()
        if self.pointcloud is not None:
            self.pointcloud.this.disown()

    def test_gnomonPointCloudFromImage_parameters(self):
        assert self.detection.parameters() is not None
        assert 'nuclei_channel' in self.detection.parameters()
        self.detection.setParameter('nuclei_channel','reference')
        assert self.detection['nuclei_channel'] == 'reference'

    def test_gnomonPointCloudFromImage_detection(self):
        self.detection.setParameter('nuclei_channel','reference')
        self.detection.setParameter('threshold',1)
        self.detection.run()

        self.pointcloud = self.detection.output()[0]
        assert self.pointcloud is not None
        assert self.pointcloud.pointCount() == self.n_points


class TestGnomonSeedDetection(unittest.TestCase):
    """Tests the seedDetectionTimagetk class.

    """

    @classmethod
    def setUpClass(cls):
        load_plugin_group("imageData")
        load_plugin_group("pointCloudFromImage")
        np.random.seed(42)

    def setUp(self):
        self.n_points = 9

        img = example_layered_sphere_wall_image(extent=20,
                                                voxelsize=(1., 0.5, 0.5),
                                                n_points=self.n_points)
        self.img = MultiChannelImage({'reference': img})

        self.image = {0: gnomonImage()}
        self.image_data = gnomon.core.imageData_pluginFactory().create("gnomonImageDataMultiChannelImage")
        self.image_data.set_image(self.img)
        self.image[0].setData(self.image_data)

        self.detection = gnomon.core.pointCloudFromImage_pluginFactory().create("seedDetectionTimagetk")
        self.detection.setInput(self.image)
        self.detection.refreshParameters()

        self.pointcloud = None

    def tearDown(self):
        self.detection.this.disown()
        if self.pointcloud is not None:
            self.pointcloud.this.disown()

    def test_gnomonPointCloudFromImage_detection(self):
        self.detection.setParameter('membrane_channel', 'reference')
        self.detection.setParameter('h_min', 2)
        self.detection.run()

        self.pointcloud = self.detection.output()[0]
        assert self.pointcloud is not None
        assert self.pointcloud.pointCount() == self.n_points+1

#
# test_gnomonPointCloudFromImage.py ends here.
