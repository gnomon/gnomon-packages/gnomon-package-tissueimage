import unittest

import numpy as np

import gnomon.core
from gnomon.core import gnomonImage
from gnomon.utils import load_plugin_group
from gnomon.utils.decorators.form_series import buildFormSeries

from timagetk import MultiChannelImage
from timagetk.synthetic_data.nuclei_image import example_nuclei_signal_images


class TestGnomonPointCloudQuantification(unittest.TestCase):
    """Tests the gnomonPointCloudQuantification class.
    
    """
    
    @classmethod
    def setUpClass(cls):
        load_plugin_group("imageData")
        load_plugin_group("pointCloudFromImage")
        load_plugin_group("pointCloudQuantification")
        np.random.seed(42)

    def setUp(self):
        self.n_points = 12
        reference_img, signal_img, _, _ = example_nuclei_signal_images(n_points=self.n_points,
                                                                       nuclei_radius=1.5,
                                                                       extent=20,
                                                                       voxelsize=(1, 0.5, 0.5))

        self.img = MultiChannelImage({'reference': reference_img, 'signal': signal_img})

        self.image = {0: gnomonImage()}
        self.image_data = gnomon.core.imageData_pluginFactory().create("gnomonImageDataMultiChannelImage")
        self.image_data.set_image(self.img)
        self.image[0].setData(self.image_data)

        self.detection = gnomon.core.pointCloudFromImage_pluginFactory().create("nucleiDetectionTimagetk")
        self.detection.setInput(self.image)
        self.detection.refreshParameters()
        self.detection.run()
        self.pointcloud = self.detection.output()

        self.dataframe_series = None
        self.analysis = gnomon.core.pointCloudQuantification_pluginFactory().create("nucleiSignalQuantificationTimagetk")
        self.analysis.setImage(self.image)
        self.analysis.setPointCloud(self.pointcloud)
        self.analysis.refreshParameters()

    def tearDown(self):
        self.detection.this.disown()
        self.analysis.this.disown()

    def test_gnomonPointCloudQuantification_signals(self):
        self.analysis.setParameter('gaussian_sigma',0.5)
        self.analysis.run()
        self.dataframe_series = self.analysis.outputDataFrame()
        dataframe = self.dataframe_series[0]
        assert 'signal' in dataframe.columnNames()

#
# test_gnomonPointCloudQuantification.py ends here.
