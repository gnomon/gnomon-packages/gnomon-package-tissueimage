import unittest

import gnomon.core
from gnomon.utils import load_plugin_group

class TestGnomonPointCloudReader(unittest.TestCase):
    """Tests the gnomonPointCloudReader class.

    """

    @classmethod
    def setUpClass(cls):
        load_plugin_group("pointCloudReader")

    def setUp(self):
        self.filename = "test/resources/hexagon.csv"

        self.reader = gnomon.core.pointCloudReader_pluginFactory().create("gnomonPointCloudReaderDataFrame")
        self.reader.setPath(self.filename)

    def tearDown(self):
        self.reader.this.disown()

    def test_gnomonPointCloudReader_read(self):
        self.reader.run()
        pointCloud = self.reader.pointCloud()[0]
        assert pointCloud is not None

    def test_gnomonPointCloudReader_pointCloud(self):
        self.reader.run()
        pointCloud = self.reader.pointCloud()[0]
        assert pointCloud.pointCount() == 7

    def test_gnomonPointCloudReader_type(self):
        self.reader.run()
        type = self.reader.extensions()
        assert type == ['csv']

#
# test_gnomonPointCloudReader.py ends here.
