import unittest
import os

import numpy as np

import gnomon.core
from gnomon.utils import load_plugin_group


class TestGnomonPointCloudWriter(unittest.TestCase):
    """Tests the gnomonPointCloudWriter class.

    """

    @classmethod
    def setUpClass(cls):
        load_plugin_group("pointCloudReader")
        load_plugin_group("pointCloudWriter")

    def setUp(self):
        self.filename = "test/resources/hexagon.csv"

        self.reader = gnomon.core.pointCloudReader_pluginFactory().create("gnomonPointCloudReaderDataFrame")
        self.reader.setPath(self.filename)

        self.reader.run()
        self.pointCloud = self.reader.pointCloud()

        self.saved_filename = "test/resources/pointCloud_writer_tmp.csv"

        self.writer = gnomon.core.pointCloudWriter_pluginFactory().create("pointCloudWriterDataFrame")
        self.writer.setPath(self.saved_filename)

    def tearDown(self):
        if os.path.exists(self.saved_filename):
            os.remove(self.saved_filename)
        self.reader.this.disown()
        self.writer.this.disown()

    def test_gnomonPointCloudWriter_write(self):
        self.writer.setPointCloud(self.pointCloud)
        self.writer.run()
        assert os.path.exists(self.saved_filename)

        self.reader.setPath(self.saved_filename)
        self.reader.run()
        read_pointCloud = self.reader.pointCloud()
        assert read_pointCloud is not None

        assert read_pointCloud[0].pointCount() == self.pointCloud[0].pointCount()

        assert np.all([p in read_pointCloud[0].pointPropertyNames() for p in self.pointCloud[0].pointPropertyNames()])
        assert np.all([pid in read_pointCloud[0].pointIds() for pid in self.pointCloud[0].pointIds()])
        
        assert np.all([read_pointCloud[0].pointX()[pid] == self.pointCloud[0].pointX()[pid] for pid in self.pointCloud[0].pointIds()])
        assert np.all([read_pointCloud[0].pointY()[pid] == self.pointCloud[0].pointY()[pid] for pid in self.pointCloud[0].pointIds()])
        assert np.all([read_pointCloud[0].pointZ()[pid] == self.pointCloud[0].pointZ()[pid] for pid in self.pointCloud[0].pointIds()])


#
# test_gnomonPointCloudWriter.py ends here.
