import unittest
import numpy as np

import gnomon.core
from gnomon.core import gnomonImage
from gnomon.utils import load_plugin_group

import vtk

from gnomon_package_tissueimage.utils import sp_img_to_vtk_img, vtk_img_to_sp_img

from timagetk.synthetic_data.nuclei_image import example_nuclei_signal_images
from timagetk import SpatialImage


class TestVTKTimagetkConverter(unittest.TestCase):
    """Tests the VTK Timagetk converter class.

    """

    @classmethod
    def setUpClass(cls) -> None:
        load_plugin_group("imageData")
        np.random.seed(42)

    def setUp(self):

        reference_img, signal_img, _, _ = example_nuclei_signal_images(n_points=13, extent=20, voxelsize=(1, 0.5, 0.5))
        np_arr = np.array([[[1,2,3,4],[5,6,7,8],[8,8,8,8]],[[9,10,11,12],[13,14,15,16],[1,1,1,1]]])
        self.img = SpatialImage(np_arr)

    def tearDown(self):
        pass
        #self.image.this.disown()
        #self.image_data.this.disown()

    def test_toVtk(self):
        #print("before")
        #print(self.img)

        vtk_img = sp_img_to_vtk_img(self.img)
        #print("vtk img")
        #print(vtk_img)

        new_sp_img = vtk_img_to_sp_img(vtk_img)

        #print("after")
        #print(new_sp_img)
        assert np.array_equal(self.img.get_array(), new_sp_img.get_array())
